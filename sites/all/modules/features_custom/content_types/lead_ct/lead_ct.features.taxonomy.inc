<?php
/**
 * @file
 * lead_ct.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function lead_ct_taxonomy_default_vocabularies() {
  return array(
    'lead_account_win_agency_type' => array(
      'name' => 'Company Type',
      'machine_name' => 'lead_account_win_agency_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'lead_account_win_industry' => array(
      'name' => 'Lead/Account Win Industry',
      'machine_name' => 'lead_account_win_industry',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'lead_category' => array(
      'name' => 'Lead Category',
      'machine_name' => 'lead_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'leads_position' => array(
      'name' => 'Leads position',
      'machine_name' => 'leads_position',
      'description' => 'Tabs',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'newsletter' => array(
      'name' => 'Newsletter',
      'machine_name' => 'newsletter',
      'description' => 'Simplenews newsletter categories.',
      'hierarchy' => 0,
      'module' => 'simplenews',
      'weight' => 0,
    ),
    'our_positioning_differentiation' => array(
      'name' => 'Lead/Account Win Topic',
      'machine_name' => 'our_positioning_differentiation',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
