<?php
/**
 * @file
 * lead_ct.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function lead_ct_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_info_conferencevid|node|conference_video|form';
  $field_group->group_name = 'group_basic_info_conferencevid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'conference_video';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_conference_video';
  $field_group->data = array(
    'label' => 'Basic Information',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_conference_speaker',
      2 => 'field_duration',
      3 => 'field_file_download_lead',
      4 => 'field_subtitle_training',
      5 => 'field_video_id',
      6 => 'field_non_member_teaser_video',
      7 => 'field_new_video',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basic-info-conferencevid field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basic_info_conferencevid|node|conference_video|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_company_info_contact|node|lead|form';
  $field_group->group_name = 'group_company_info_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_company_info_vert_group';
  $field_group->data = array(
    'label' => 'Contact Information',
    'weight' => '15',
    'children' => array(
      0 => 'field_contact_name',
      1 => 'field_contact_title',
      2 => 'field_phone_contact',
      3 => 'field_email_contact',
      4 => 'field_additional_information',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_company_info_contact|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_company_info_general|node|lead|form';
  $field_group->group_name = 'group_company_info_general';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_company_info_vert_group';
  $field_group->data = array(
    'label' => 'General',
    'weight' => '14',
    'children' => array(
      0 => 'field_company_summary',
      1 => 'field_company',
      2 => 'field_website',
      3 => 'field_phone_company',
      4 => 'field_fax_company',
      5 => 'field_parent_company',
      6 => 'field_address',
      7 => 'field_legacy',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_company_info_general|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_company_info_vert_group|node|lead|form';
  $field_group->group_name = 'group_company_info_vert_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_company_info';
  $field_group->data = array(
    'label' => 'Company Information',
    'weight' => '13',
    'children' => array(
      0 => 'group_company_info_contact',
      1 => 'group_company_info_general',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-company-info-vert-group field-group-tabs',
      ),
    ),
  );
  $export['group_company_info_vert_group|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_company_info|node|lead|form';
  $field_group->group_name = 'group_company_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Company Information',
    'weight' => '1',
    'children' => array(
      0 => 'group_company_info_vert_group',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-company-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_company_info|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_conference_video_overview|node|conference_video|default';
  $field_group->group_name = 'group_conference_video_overview';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'conference_video';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Video Overview',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_non_member_teaser_video',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'Video Overview',
      'instance_settings' => array(
        'classes' => 'group-conference-video-overview field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_conference_video_overview|node|conference_video|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_conference_video|node|conference_video|form';
  $field_group->group_name = 'group_conference_video';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'conference_video';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Conference Video',
    'weight' => '1',
    'children' => array(
      0 => 'field_link',
      1 => 'group_basic_info_conferencevid',
      2 => 'group_featured_conference_vid',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-conference-video field-group-tabs',
      ),
    ),
  );
  $export['group_conference_video|node|conference_video|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_featured_conference_vid|node|conference_video|form';
  $field_group->group_name = 'group_featured_conference_vid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'conference_video';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_conference_video';
  $field_group->data = array(
    'label' => 'Featured Item',
    'weight' => '3',
    'children' => array(
      0 => 'field_featured_video',
      1 => 'field_image_conference_video',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-featured-conference-vid field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_featured_conference_vid|node|conference_video|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_info_fieldset|node|lead|form';
  $field_group->group_name = 'group_general_info_fieldset';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '0',
    'children' => array(
      0 => 'group_general_info',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general-info-fieldset field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_general_info_fieldset|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_info_leads|node|lead|form';
  $field_group->group_name = 'group_general_info_leads';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_general_info';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '23',
    'children' => array(
      0 => 'body',
      1 => 'field_title_non_member',
      2 => 'field_file_download_lead',
      3 => 'field_feature_intro',
      4 => 'field_lead_category',
      5 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general-info-leads field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_general_info_leads|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_info|node|lead|form';
  $field_group->group_name = 'group_general_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_general_info_fieldset';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '1',
    'children' => array(
      0 => 'group_general_info_leads',
      1 => 'group_header_area_lead',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_general_info|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header_area_lead|node|lead|form';
  $field_group->group_name = 'group_header_area_lead';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_general_info';
  $field_group->data = array(
    'label' => 'Header Area',
    'weight' => '24',
    'children' => array(
      0 => 'field_enable_colorbox',
      1 => 'field_banner_image',
      2 => 'field_feature_image_link',
      3 => 'field_video_id',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Header Area',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_header_area_lead|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_cropnon|node|lead|form';
  $field_group->group_name = 'group_image_cropnon';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_landing_imagenon';
  $field_group->data = array(
    'label' => 'Image crop',
    'weight' => '17',
    'children' => array(
      0 => 'field_lead_landing_image_crop_nm',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-image-cropnon field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_image_cropnon|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_crop|node|lead|form';
  $field_group->group_name = 'group_image_crop';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_landing_image';
  $field_group->data = array(
    'label' => 'Image crop',
    'weight' => '33',
    'children' => array(
      0 => 'field_lead_landing_image_crop',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-image-crop field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_image_crop|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_librarynon|node|lead|form';
  $field_group->group_name = 'group_image_librarynon';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_landing_imagenon';
  $field_group->data = array(
    'label' => 'Image from library',
    'weight' => '18',
    'children' => array(
      0 => 'field_lead_landing_image_lib_nm',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-image-librarynon field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_image_librarynon|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_library|node|lead|form';
  $field_group->group_name = 'group_image_library';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_landing_image';
  $field_group->data = array(
    'label' => 'Image from library',
    'weight' => '34',
    'children' => array(
      0 => 'field_lead_landing_image_lib',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-image-library field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_image_library|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_landing_image_non|node|lead|form';
  $field_group->group_name = 'group_landing_image_non';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Landing Image for non-memebr',
    'weight' => '10',
    'children' => array(
      0 => 'group_landing_imagenon',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-landing-image-non field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_landing_image_non|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_landing_image_tabs|node|lead|form';
  $field_group->group_name = 'group_landing_image_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Landing Image',
    'weight' => '8',
    'children' => array(
      0 => 'group_landing_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-landing-image-tabs field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_landing_image_tabs|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_landing_imagenon|node|lead|form';
  $field_group->group_name = 'group_landing_imagenon';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_landing_image_non';
  $field_group->data = array(
    'label' => 'Landing Image',
    'weight' => '16',
    'children' => array(
      0 => 'group_image_cropnon',
      1 => 'group_image_librarynon',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-landing-imagenon field-group-tabs',
      ),
    ),
  );
  $export['group_landing_imagenon|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_landing_image|node|lead|form';
  $field_group->group_name = 'group_landing_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_landing_image_tabs';
  $field_group->data = array(
    'label' => 'Landing Image',
    'weight' => '5',
    'children' => array(
      0 => 'group_image_crop',
      1 => 'group_image_library',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-landing-image field-group-tabs',
      ),
    ),
  );
  $export['group_landing_image|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lead_company_info|node|lead|default';
  $field_group->group_name = 'group_lead_company_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'The Company',
    'weight' => '9',
    'children' => array(
      0 => 'field_company_summary',
      1 => 'field_company',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'The Company',
      'instance_settings' => array(
        'classes' => 'group-lead-company-info field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_lead_company_info|node|lead|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lead_contact_info|node|lead|default';
  $field_group->group_name = 'group_lead_contact_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact Info',
    'weight' => '2',
    'children' => array(
      0 => 'field_website',
      1 => 'field_contact_name',
      2 => 'field_contact_title',
      3 => 'field_phone_company',
      4 => 'field_fax_company',
      5 => 'field_phone_contact',
      6 => 'field_email_contact',
      7 => 'field_additional_information',
      8 => 'field_parent_company',
      9 => 'field_address',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Contact Info',
      'instance_settings' => array(
        'classes' => 'group-lead-contact-info field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_lead_contact_info|node|lead|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lead_information|node|lead|form';
  $field_group->group_name = 'group_lead_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Lead Information',
    'weight' => '2',
    'children' => array(
      0 => 'field_mirren_lead_source',
      1 => 'field_related_info',
      2 => 'field_mirren_leads_alert',
      3 => 'field_twitter',
      4 => 'field_topic_account_win',
      5 => 'field_agency_type',
      6 => 'field_category_industry',
      7 => 'field_client_executive_movement',
      8 => 'field_region',
      9 => 'field_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-lead-information field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_lead_information|node|lead|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lead_related_info|node|lead|default';
  $field_group->group_name = 'group_lead_related_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Related Info',
    'weight' => '6',
    'children' => array(
      0 => 'field_related_info',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Related Info',
      'instance_settings' => array(
        'classes' => 'group-lead-related-info field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_lead_related_info|node|lead|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lead_scoop|node|lead|default';
  $field_group->group_name = 'group_lead_scoop';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'The Scoop',
    'weight' => '8',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'The Scoop',
      'instance_settings' => array(
        'classes' => 'group-lead-scoop field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_lead_scoop|node|lead|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_legacy_fields|node|lead|form';
  $field_group->group_name = 'group_legacy_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'lead';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Legacy fields',
    'weight' => '3',
    'children' => array(
      0 => 'field_contact_person',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-legacy-fields field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_legacy_fields|node|lead|form'] = $field_group;

  return $export;
}
