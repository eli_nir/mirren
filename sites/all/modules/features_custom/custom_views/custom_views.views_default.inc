<?php
/**
 * @file
 * custom_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function custom_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'training_chapters';
  $view->description = 'On-demand learning, view creates the outline for topic/chapter/video content';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Training Chapters';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Chapters and Items';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'title_1',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['row_class'] = 'nid-[nid] [field_optional]';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = '[node title]';
  $handler->display->display_options['header']['area']['content'] = '<h2>%1</h2>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['header']['area']['tokenize'] = TRUE;
  /* Relationship: Content: Training Chapters (field_chapters) */
  $handler->display->display_options['relationships']['field_chapters_nid']['id'] = 'field_chapters_nid';
  $handler->display->display_options['relationships']['field_chapters_nid']['table'] = 'field_data_field_chapters';
  $handler->display->display_options['relationships']['field_chapters_nid']['field'] = 'field_chapters_nid';
  $handler->display->display_options['relationships']['field_chapters_nid']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_chapters_nid']['delta'] = '-1';
  /* Relationship: Content: Training Item (field_training_item) */
  $handler->display->display_options['relationships']['field_training_item_nid']['id'] = 'field_training_item_nid';
  $handler->display->display_options['relationships']['field_training_item_nid']['table'] = 'field_data_field_training_item';
  $handler->display->display_options['relationships']['field_training_item_nid']['field'] = 'field_training_item_nid';
  $handler->display->display_options['relationships']['field_training_item_nid']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['relationships']['field_training_item_nid']['delta'] = '-1';
  /* Field: Content: Optional */
  $handler->display->display_options['fields']['field_optional']['id'] = 'field_optional';
  $handler->display->display_options['fields']['field_optional']['table'] = 'field_data_field_optional';
  $handler->display->display_options['fields']['field_optional']['field'] = 'field_optional';
  $handler->display->display_options['fields']['field_optional']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['fields']['field_optional']['label'] = '';
  $handler->display->display_options['fields']['field_optional']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['alter']['text'] = 'optional';
  $handler->display->display_options['fields']['field_optional']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_optional']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['type'] = 'list_key';
  /* Field: Chapter Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Chapter Title';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_1']['link_to_node'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'field_training_item_nid';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'span';
  $handler->display->display_options['fields']['title']['element_class'] = '[field_optional]';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['nid']['title'] = '%1';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type_1']['id'] = 'type_1';
  $handler->display->display_options['filters']['type_1']['table'] = 'node';
  $handler->display->display_options['filters']['type_1']['field'] = 'type';
  $handler->display->display_options['filters']['type_1']['value'] = array(
    'training_program' => 'training_program',
  );
  $handler->display->display_options['filters']['type_1']['group'] = 1;

  /* Display: Training */
  $handler = $view->new_display('block', 'Training', 'block');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Optional */
  $handler->display->display_options['fields']['field_optional']['id'] = 'field_optional';
  $handler->display->display_options['fields']['field_optional']['table'] = 'field_data_field_optional';
  $handler->display->display_options['fields']['field_optional']['field'] = 'field_optional';
  $handler->display->display_options['fields']['field_optional']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['fields']['field_optional']['label'] = '';
  $handler->display->display_options['fields']['field_optional']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['alter']['text'] = 'optional';
  $handler->display->display_options['fields']['field_optional']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_optional']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_optional']['type'] = 'list_key';
  /* Field: Chapter Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Chapter Title';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_1']['link_to_node'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'field_training_item_nid';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_type'] = 'span';
  $handler->display->display_options['fields']['title']['element_class'] = '[field_optional]';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['relationship'] = 'field_chapters_nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: New for 2014 */
  $handler->display->display_options['fields']['field_new_video_chapters']['id'] = 'field_new_video_chapters';
  $handler->display->display_options['fields']['field_new_video_chapters']['table'] = 'field_data_field_new_video_chapters';
  $handler->display->display_options['fields']['field_new_video_chapters']['field'] = 'field_new_video_chapters';
  $handler->display->display_options['fields']['field_new_video_chapters']['relationship'] = 'field_training_item_nid';
  $handler->display->display_options['fields']['field_new_video_chapters']['label'] = '';
  $handler->display->display_options['fields']['field_new_video_chapters']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_new_video_chapters']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_new_video_chapters']['alter']['text'] = '<span class="new-video">New for 2014</span>';
  $handler->display->display_options['fields']['field_new_video_chapters']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_new_video_chapters']['type'] = 'list_key';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="training-item-container">
<div class="video-title">[title][field_new_video_chapters]</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $translatables['training_chapters'] = array(
    t('Master'),
    t('Chapters and Items'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('[node title]'),
    t('<h2>%1</h2>'),
    t('field_chapters'),
    t('field_training_item'),
    t('optional'),
    t('All'),
    t('%1'),
    t('Training'),
    t('<span class="new-video">New for 2014</span>'),
    t('<div class="training-item-container">
<div class="video-title">[title][field_new_video_chapters]</div>
</div>'),
  );
  $export['training_chapters'] = $view;

  return $export;
}
