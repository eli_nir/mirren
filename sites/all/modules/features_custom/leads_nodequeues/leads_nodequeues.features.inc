<?php
/**
 * @file
 * leads_nodequeues.features.inc
 */

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function leads_nodequeues_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: build_your_pipeline_of_leads
  $nodequeues['build_your_pipeline_of_leads'] = array(
    'name' => 'build_your_pipeline_of_leads',
    'title' => 'Build Your Pipeline of Leads - proactive subtab',
    'subqueue_title' => '',
    'size' => 5,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'lead',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: build_your_pipeline_of_leads_acc
  $nodequeues['build_your_pipeline_of_leads_acc'] = array(
    'name' => 'build_your_pipeline_of_leads_acc',
    'title' => 'Build Your Pipeline of Leads - accounts subtab',
    'subqueue_title' => '',
    'size' => 5,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'lead',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: convert_your_leads
  $nodequeues['convert_your_leads'] = array(
    'name' => 'convert_your_leads',
    'title' => 'Convert Your Leads - proactive subtab',
    'subqueue_title' => '',
    'size' => 5,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'training_item',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: convert_your_leads_accounts_subt
  $nodequeues['convert_your_leads_accounts_subt'] = array(
    'name' => 'convert_your_leads_accounts_subt',
    'title' => 'Convert Your Leads - accounts subtab',
    'subqueue_title' => '',
    'size' => 5,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'training_item',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}
