<div id="fee-table">
  <a id="hide-fee" href="#fee-table-inner" data-toggle="collapse" data-parent="#fee-table" class="collapsed">Show Fee Table <span class="arrow-down"></span></a>

  <div id="fee-table-inner" class="collapse" style="height: 0px;">

    <style type="text/css">.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
      .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#848484 !important; background-color:#e8edff;}
      .tg th{font-family:Arial, sans-serif;font-size:14px;border-bottom: #ccc solid 1px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: #f2f2f2;color:#039 !important;background-color:#b9c9fe;}
      .tg .tg-hjma{background-color:#ffffff;text-align: center;}
      .tg .tg-cxkv{background-color:#ffffff;text-align: center;}
      .tg .tg-pxqp{background-color:#a73d16;color:#ffffff !important; text-align:center;}
      .tg-cxkv span.monthly-fee {font-size: 10px;color: #848484 !important;width: 83px;float: left;margin-top: -2px;line-height: 12px;margin-bottom: -4px;text-align: center;}
      span.quarterly {display: block;width: 93px;float: left;margin-right: 6px;}
      td.tg-hjma.orange {color: #a73d16 !important;text-align: left;}
      .tg .tg-zlxb{border-bottom: #ccc solid 1px; border-left: #ffffff solid 1px; border-top:#ffffff solid 1px; background-color:#ffffff ;text-align:center;}
      .tg-hjma.orange span {color: #848484 !important;font-size: 12px;}
      span.monthly {float: left;}
    </style>
    <table class="tg" style="margin-top:20px;">
      <tbody>
      <tr>
        <th class="tg-zlxb">&nbsp;</th>
        <th class="tg-pxqp" colspan="6">MONTHLY MEMBERSHIP FEE</th>
      </tr>
      <tr>
        <td class="tg-hjma orange">Office Staff Size <span data-toggle="tooltip" id="fee-table-tooltip-1" title="Why is membership only for one office location...and not for all our office locations? We are set up this way because we do not believe that smaller offices should have to carry the heavy burden of paying for their larger office brethren. This way each office is paying a membership fee that better correlates to actual size. That said, contact Member Services to set up a special &quot; National Membership&quot;.">[?]</span></td>
        <td class="tg-hjma orange">&lt; 25</td>
        <td class="tg-hjma orange">26-75</td>
        <td class="tg-hjma orange">76-250</td>
        <td class="tg-hjma orange">251-500</td>
        <td class="tg-hjma orange">501-1500</td>
        <td class="tg-hjma orange">1501+</td>
      </tr>
      <tr>
        <td class="tg-cxkv"><span class="quarterly">Paid Quarterly</span> <span class="monthly-fee">Monthly Fee with<br />
			10% Savings <span data-toggle="tooltip" id="fee-table-tooltip-2" title="If you select Quarterly Membership, you will receive a 10% fee discount and be billed every quarter for your next 3 months of membership.">[?]</span></span></td>
        <td class="tg-cxkv">$125</td>
        <td class="tg-cxkv">$195</td>
        <td class="tg-cxkv">$245</td>
        <td class="tg-cxkv">$290</td>
        <td class="tg-cxkv">$335</td>
        <td class="tg-cxkv">$350</td>
      </tr>
      <tr>
        <td class="tg-hjma"><span class="monthly">Paid Monthly</span></td>
        <td class="tg-hjma">$140</td>
        <td class="tg-hjma">$220</td>
        <td class="tg-hjma">$270</td>
        <td class="tg-hjma">$320</td>
        <td class="tg-hjma">$370</td>
        <td class="tg-hjma">$390</td>
      </tr>
      </tbody>
    </table>

    <p>To keep our fees as low as possible, membership is for each individual office location.<a class="tooltip fee_box" rel="office-size-explination/?showtemplate=false"></a></p>
    <p>For information about National Membership for all of your office locations, please contact <a class="colorbox-inline" href="?width=670&amp;height=340&amp;inline=true#member-services-modal">Member.Service</a></p>

  </div>

</div>
</div>
