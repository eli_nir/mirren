<?php switch($type) :
  case 'agency': ?>
  <?php break; ?>

  <?php case 'freelancer': ?>
  <div style="display: none;" class="Freelancer_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services,</a> who can help with your application process.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'sc': ?>
  <div style="display: none;" class="Consultant_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a> to set up your profile.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'hc': ?>
  <div style="display: none;" class="Holding_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a> for your custom application process.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'anac': ?>
  <div style="display: none;" class="AdClub_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a> to set up your profile.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'nbc': ?>
  <div style="display: none;" class="BizConsultant_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        As you would imagine, those that compete with Mirren's training, recruiting or on-line services are not eligible for membership. If you have any questions please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a>.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'other': ?>
  <div style="display: none;" class="Other_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services,</a> who can help with your application process.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>
<?php endswitch; ?>