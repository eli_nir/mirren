<?php switch($type) :
  case 'agency': ?>
  <div style="display: none;" class="Agency_Group second_level indent_40">
    <div class="checkbox">
      <input type="radio" value="Master" onclick="jQuery('#fee_section').show();jQuery('.master_fields_2').show();jQuery('.member_fields_1').show();jQuery('.member_fields_2').hide();" name="agency_type"> <span class="text_label">Master Membership Application</span>
    </div>
    <div class="agency_type text_input  indent_40" id="Master">
      <div class="master_fields_1">
        <p class="font_12">I am applying for membership for my agency office (and for me).</p>
      </div>
    </div>
    <div class="checkbox">
      <input type="radio" value="Personal" onclick="jQuery('#fee_section').hide();jQuery('.member_fields_2').show();jQuery('.master_fields_1').show();jQuery('.master_fields_2').hide();jQuery('#membership_table').hide();" name="agency_type"> <span class="text_label">Personal Membership</span>
    </div>
    <div class="agency_type text_input indent_40" id="Personal">
      <div class="member_fields_1">
        <p class="font_12">I am adding my personal account to my agency's membership, which is already set up.</p>
      </div>

      <div style="display: none" class="member_fields_2">
        <div id="code_check">
          <div>
            <div class="sub_text_label float_l">Your Company Code</div>
            <div class="clearb"></div>
            <input type="text" style="width:200px;margin-right: 5px;" id="agency_code" name="agency_code" class="text_input float_l">
            <div style="margin-top:5px;" class="color_btn std_btn  float_l"><a onclick="verify_ccode();return false;" class="font_15">Verify Code ›</a></div><div class="clearb"></div>
            <div id="code_results"></div>
          </div>
        </div>
        <div class="clearb"></div>
        <div class="small_label">Note that your Company Code is required to add your personal membership. You can get this from the person that set up your agency's membership. If you have any questions, contact <a class="fancybox_application_help" onclick="return false" href="contactmemberservice?showtemplate=false">Member Services,</a>.</div>
      </div>
    </div>
    <div class="clearb"></div>

    <div id="fee_section" style="display: none;">
      <a onclick="jQuery('#membership_table').toggle();jQuery(this).hide();jQuery('#collapse_fee_table').show();" class="color_link font_15" id="expand_fee_table">See Membership Fees v</a>

      <a onclick="jQuery('#membership_table').toggle();jQuery(this).hide();jQuery('#expand_fee_table').show();" style="display:none;" class="color_link font_15" id="collapse_fee_table">Hide Membership Fees ^</a>
      <div class="clearb"></div>
    </div>

    <div style="display: none;" class="second_level Agency_Group" id="member_fees">
      <div style="display:none;" id="membership_table"><p class="font_15">When you get to the billing page you will select from one of three options for membership fees:</p>
        <div class="fee_btns"><a class="selected round">1. Monthly</a></div>
        <div class="fee_btns round"><a onclick="$('#fees_quarterly').toggle();$(this).toggleClass('selected');$('#fees_monthly').removeClass('round');$('#fees_quarterly').addClass('round');$('#monthly_fee').toggle();$('#paid_monthly').toggle();$('#down1').toggle();">2. Quarterly <span>Save 10%</span><span class="more_arrow" id="down1"></span></a></div>
        <div id="table_wrapper">


          <style type="text/css">.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
            .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#848484 !important; background-color:#e8edff;}
            .tg th{font-family:Arial, sans-serif;font-size:14px;border-bottom: #ccc solid 1px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color: #f2f2f2;color:#039 !important;background-color:#b9c9fe;}
            .tg .tg-hjma{background-color:#ffffff;text-align: center;}
            .tg .tg-cxkv{background-color:#ffffff;text-align: center;}
            .tg .tg-pxqp{background-color:#a73d16;color:#ffffff !important; text-align:center;}
            .tg-cxkv span.monthly-fee {font-size: 10px;color: #848484 !important;width: 83px;float: left;margin-top: -2px;line-height: 12px;margin-bottom: -4px;text-align: center;}
            span.quarterly {display: block;width: 93px;float: left;margin-right: 6px;}
            td.tg-hjma.orange {color: #a73d16 !important;text-align: left;}
            .tg .tg-zlxb{border-bottom: #ccc solid 1px;  border-left: #ffffff solid 1px; border-top:#ffffff solid 1px;background-color:#ffffff ;text-align:center;}
            .tg-hjma.orange span {color: #848484 !important;font-size: 12px;}
            span.monthly {float: left;}
          </style>
          <table class="tg" style="margin-top:20px;">
            <tbody>
            <tr>
              <th class="tg-zlxb">&nbsp;</th>
              <th class="tg-pxqp" colspan="6">MONTHLY MEMBERSHIP FEE</th>
            </tr>
            <tr>
              <td class="tg-hjma orange">Office Staff Size <span data-toggle="tooltip" id="fee-table-tooltip-1" title="Why is membership only for one office location...and not for all our office locations? We are set up this way because we do not believe that smaller offices should have to carry the heavy burden of paying for their larger office brethren. This way each office is paying a membership fee that better correlates to actual size. That said, contact Member Services to set up a special &quot; National Membership&quot;.">[?]</span></td>
              <td class="tg-hjma orange">&lt; 25</td>
              <td class="tg-hjma orange">26-75</td>
              <td class="tg-hjma orange">76-250</td>
              <td class="tg-hjma orange">251-500</td>
              <td class="tg-hjma orange">501-1500</td>
              <td class="tg-hjma orange">1501+</td>
            </tr>
            <tr>
              <td class="tg-cxkv"><span class="quarterly">Paid Quarterly</span> <span class="monthly-fee">Monthly Fee with<br />
        10% Savings <span data-toggle="tooltip" id="fee-table-tooltip-2" title="If you select Quarterly Membership, you will receive a 10% fee discount and be billed every quarter for your next 3 months of membership.">[?]</span></span></td>
              <td class="tg-cxkv">$125</td>
              <td class="tg-cxkv">$195</td>
              <td class="tg-cxkv">$245</td>
              <td class="tg-cxkv">$290</td>
              <td class="tg-cxkv">$335</td>
              <td class="tg-cxkv">$350</td>
            </tr>
            <tr>
              <td class="tg-hjma"><span class="monthly">Paid Monthly</span></td>
              <td class="tg-hjma">$140</td>
              <td class="tg-hjma">$220</td>
              <td class="tg-hjma">$270</td>
              <td class="tg-hjma">$320</td>
              <td class="tg-hjma">$370</td>
              <td class="tg-hjma">$390</td>
            </tr>
            </tbody>
          </table>


        </div>

        <p>To keep our fees as low as possible, membership is for each individual office location.<a class="tooltip fee_box" rel="office-size-explination/?showtemplate=false"></a></p>
        <p>For information about National Membership for all of your office locations, please contact <a class="colorbox-inline" href="?width=670&amp;height=340&amp;inline=true#member-services-modal">Member Services</a></p>
        <div class="clearb"></div></div>
    </div>
  </div>
  <?php break; ?>

  <?php case 'freelancer': ?>
  <div style="display: none;" class="Freelancer_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services,</a> who can help with your application process.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'sc': ?>
  <div style="display: none;" class="Consultant_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a> to set up your profile.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'hc': ?>
  <div style="display: none;" class="Holding_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a> for your custom application process.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'anac': ?>
  <div style="display: none;" class="AdClub_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a> to set up your profile.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'nbc': ?>
  <div style="display: none;" class="BizConsultant_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        As you would imagine, those that compete with Mirren's training, recruiting or on-line services are not eligible for membership. If you have any questions please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services</a>.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>

  <?php case 'other': ?>
  <div style="display: none;" class="Other_Group second_level indent_40">
    <div class="comment_box_top">
      <div class="comment_box_bg">
        Please contact <a class="fancybox_contact" onclick="return false" href="contactmemberservice?showtemplate=false">member services,</a> who can help with your application process.
      </div>
    </div><div class="clearb"></div>
    <div class="comment_box_bottom"></div>
  </div>
  <?php break; ?>
<?php endswitch; ?>