<?php
	$modulePath = drupal_get_path('module', 'mirren');
?>

<div id="page" class="leads">
	<div class="lead-steps">
		<div class="container">
			<div class="row">
				<?php foreach ( $th1 as $key => $item ) : ?>
				<div class="col-xs-6" id="step-<?php print $item['tid']; ?>">
					<p <?php if ( $key == 0 ) : ?>class="current"<?php endif; ?>>
					<span><?php print $key + 1; ?></span>
					<?php
						$titleItems = explode(' ', $item['name']);
						$titleItems[0] = '<strong>' . $titleItems[0] . '</strong>';
						print implode(' ', $titleItems);
					?>
					</p>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<?php foreach ( $th1 as $key => $item ) : ?>
		<div class="view-content leads step-<?php print $item['tid']; ?> <?php if ( $key == 0 ) : ?>active<?php endif; ?>">
			<div class="lead-tabs">
				<div class="container">
					<div class="row">
						<ul>
							<?php if ( !empty($item['children']) ) : ?>
								<?php foreach ( $item['children'] as $indexTab => $elementTab ) : ?>
									<li id="secondtab-<?php print $elementTab['tid']; ?>"><a title="<?php print $elementTab['name']; ?>" <?php if ( $indexTab == 0 ) : ?>class="selected"<?php endif; ?> ><?php print $elementTab['name']; ?></a></li>
								<?php endforeach; ?>
							<?php endif; ?>
							<li id="secondtab-filter" class="filter"><a title="filter">Filter<img src="<?php print $modulePath; ?>/css/images/icn_menu.png" /></a></li>
						</ul>
					</div>
				</div>
			</div><!--END lead-tabs-->
			<?php if ( !empty($item['children']) ) : ?>
				<?php foreach ( $item['children'] as $indexTab => $elementTab ) : ?>
					<div class="container secondtab-<?php print $elementTab['tid']; ?> <?php if ( $indexTab == 0 ) : ?>active<?php endif; ?>">
						<div class="leadnote contextual-links-region">
              <?php print $elementTab['description']; ?>
              <?php if (entity_access('update', 'taxonomy_term')) : ?>
              <div class="contextual-links-wrapper">
                <ul class="contextual-links">
                  <li class="node-edit first"><a href="/taxonomy/term/<?php print $elementTab['tid']; ?>/edit?destination=<?php print $_GET['q']; ?>">Edit</a></li>
                </ul>
              </div>
              <?php endif; ?>
            </div>
						<?php if ( $key == 0 ) : ?>
							<h2><?php print t('Featured leads');?>
								<ul class="leadinfo clearfix">
									<li><span><?php print mirren_get_leadsby_periods($elementTab['tid'], 'day'); ?></span> <?php print t('New leads in last 24hrs'); ?></li>
									<li><span><?php print mirren_get_leadsby_periods($elementTab['tid'], 'week'); ?></span> <?php print t('Over last 7 days'); ?></li>
									<li><span><?php print mirren_get_leadsby_periods($elementTab['tid'], 'month'); ?></span> <?php print t('Over last 30 days'); ?></li>
									<li><span><?php print mirren_get_leadsby_periods($elementTab['tid'], 'year'); ?></span> <?php print t('Over last 365 days'); ?></li>
								</ul>
							</h2>
							<?php $nodeqItems = mirren_block_leads_tabs_get_featured_nodes($elementTab['tid']); ?>
							<?php if ( !empty($nodeqItems) ) : ?>
								<div class="row featuredleads">
									<div class="mainlead big">
										<?php print $nodeqItems[0]['image']; ?>
										<div class="leaddetails">
											<?php print $nodeqItems[0]['title']; ?>
											<p><?php print $nodeqItems[0]['company']; ?></p>
											<p class="timeposted"><?php print $nodeqItems[0]['posted']; ?></p>
										</div>
									</div>
									<ul class="clearfix list1">
										<?php foreach ( $nodeqItems as $indexNodeQ => $elementNodeQ ) : ?>
											<?php if ( $indexNodeQ == 0 ) continue;?>
											<li class="clearfix small">
												<?php print $elementNodeQ['image']; ?>
												<div class="leaddetails">
													<?php print $elementNodeQ['title']; ?>
													<p><?php print $elementNodeQ['company']; ?></p>
													<p class="timeposted"><?php print $elementNodeQ['posted']; ?></p>
												</div>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif; ?>
							<div class="row breakingleads">
								<h2><?php print t('Breaking leads'); ?></h2>
								<?php
									$view = views_get_view('breaking_leads');
									print $view->preview('block', array($elementTab['tid']));

								?>
							</div>
						<?php elseif ( $key == 1 ): ?>
							<h2><?php print t('On-Demand Learning: Proactive Prospecting'); ?></h2>
							<?php
                $view = views_get_view('convert_your_leads');
                print $view->preview('block', array($elementTab['tid']));
							?>
						<?php endif; ?>

					</div>
				<?php endforeach; ?>
			<?php endif; ?>
			<div class="container secondtab-filter">
				<?php
					$block = block_load('views', 'leds_filter-block_1');
					print render(_block_get_renderable_array( _block_render_blocks(array($block))));
				?>
			</div>

		</div>

	<?php endforeach; ?>

</div>