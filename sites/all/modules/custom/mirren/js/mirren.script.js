(function($) {

	$(document).ready(function() {

		if ( $('#page.leads').length ) {

			var tabsLevel1 = $( '#page.leads .lead-steps .row > div' );
			tabsLevel1.live('click', function() {
				tabsLevel1.find('p').removeClass('current');
				$(this).find('p').addClass('current');
				var contentClass = $( this ).attr('id');

				$('#page.leads .view-content').removeClass('active');
				$('#page.leads .view-content.' + contentClass).addClass('active');

			});

			var tabsLevel2 = $( '#page.leads .lead-tabs ul li' );
			tabsLevel2.live('click', function() {
				$(this).closest('ul').find('a').removeClass('selected');
				$(this).find('a').addClass('selected');
				var contentClass = $( this ).attr('id');

				$( this ).closest('.view-content ').find('.container').removeClass('active');
				$( this ).closest('.view-content ').find('.container.' + contentClass).addClass('active');

			});

			$('.view-convert-your-leads .views-field-php a').live('click', function() {
				var htmlVideo = $( this ).closest('.field-content').find( '.video-' + $(this).attr('id') );
				$.colorbox({html: htmlVideo.clone(), closeButton: false });
				return false;
			});

			$('.btn-mirren.colorbox-close').live('click', function() {
				$.colorbox.close();
				return false;
			});

			$.each( $('.view-convert-your-leads .views-row'), function( key, value ) {
				$('<div class="leaddetails"></div>').insertAfter( $( this ).find( '.views-field-php' ) );
				$( this ).find( '.leaddetails' ).append( $( this ).find( '.views-field-body' ) ).append( $( this ).find( '.views-field-title' ) );
			});

			var buttonResetFilter = $('.view-leds-filter .views-reset-button input');
			buttonResetFilter.live('click', function() {
				console.log('Reser form');

				var formFilter = $( this ).closest('form');
				formFilter[0].reset();

				$.each( formFilter.find('.bootstrap-select'), function( key, value ) {
					$(this).find('ul.dropdown-menu li').removeClass('selected');
					$(this).find('ul.dropdown-menu li[rel=0]').addClass('selected');
					$( this ).find('.selectpicker span.filter-option').text( $(this).find('ul.dropdown-menu li[rel=0] span.text').text() );
				});

				return false;
			});

		}

	});//document ready

})(jQuery);