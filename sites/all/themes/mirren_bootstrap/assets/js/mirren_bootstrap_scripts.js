(function ($) {

    $(document).ready(function () {
        
        //$("ul.nav-tabs a").click(function (e) {
        //    e.preventDefault();  
        //      $(this).tab('show');
        //  });

        /* Bootstrap style Admin Tabs */
        $('.tabs ul.tabs.primary').removeClass().addClass('nav nav-tabs');
        $('.action-links').appendTo('.tabs').addClass('nav nav-tabs');

        /* Dismiss System Messages -- add closing 'X' */
        $('.messages').append('<a class="close" data-dismiss="alert" href="#">&times;</a>');

        /* Search from Main Menu */
        $('.menu-499 a').click(function(e) {
            e.preventDefault();
        });

        /* Search Form Label as value */
        $('#edit-search-block-form--2').attr('value', 'Search');
        //clear search form on focus
        $('#edit-search-block-form--2')
            .focus(function(){ if (this.value == 'Search') this.value = ''; })
            .blur(function(){ if (this.value == '') this.value = 'Search'; });

        /* Colorbox Modal Window */
        if ($('.colorbox-video').length) {
            $('.colorbox-video').colorbox({
                iframe: 'true',
                innerWidth: '480',
                innerHeight: '270',
                close: '<span class="close_txt">CLOSE X</span>',
                opacity: '.7'
            });
        }

        $('.colorbox-inline').colorbox({onLoad: function() {
            $('#cboxClose').remove();
        }});

        $('.colorbox-close').click(function() {
            $.colorbox.close();
        });

        $('.mirren-gate').colorbox({
            inline: true,
            href: "#mirren-gate-modal",
            innerWidth: '450px',
            innerHeight:'120px',
            opacity: '0.5',
            onLoad: function() {
                $('#cboxClose').remove();
            }
        });


        $('.page-node-27210 .mirren-gate').colorbox({
            inline: true,
            href: "#mirren-gate-modal",
            innerWidth: '450px',
            innerHeight:'130px',
            opacity: '0.5',
            onLoad: function() {
                $('#cboxClose').remove();
            }
        });




        /* Gated Content by Role - page load triggers mirren-gate modal */
        /* IF blocked role on My Library page */
        if ( $('.page-node-25728.content-blocked').length ) {
            $('.mirren-gate').once('.mirren-gate').click();
        }
        /* IF blocked role and "users" is in the URL */
        if ( $('.content-blocked').length && (/users/.test(self.location.href)) ) {
            $('.mirren-gate').once('.mirren-gate').click();
        }
        /* IF blocked role on "My Agency Hub" */
        if ( $('.page-node-25946.content-blocked').length ) {
            $('.mirren-gate').once('.mirren-gate').click();
        }
        /* IF blocked role on "Service Providers All or Search Consultants All" */
        if ( $('.page-node-25841.content-blocked').length || $('.page-node-25840.content-blocked').length ) {
            $('.mirren-gate').once('.mirren-gate').click();
        }

        /* READ MORE LINK inline with preceding paragraph */
        /* Training Item PDF */
        $('.group-body-group p').each(function(e){
            $(this).html($(this).html()+" ").append($(this).next('.read-more-link'));
        });

        /* Training Chapters Gray Out */
        $(".panel .optional").each(function() {
            $(this).closest('.panel').addClass('optional-chapter');
        });

        /* Training Item Video -- reveal/hide trouble viewing div */
        $('.link-trouble-viewing').click(function() {
           $('#video_message').removeClass('hidden');
        });
        $('#video_message .close').click(function() {
           $('#video_message').addClass('hidden');
           return false;
        });

        /* Header Submenu Region --  links as buttons */
        $('.intro-nav ul.menu li a').addClass('btn-mirren');

        /*
         * Form Styling -- Leads Exposed Filter
         */
        if ( $('#views-exposed-form-lead-view-page').length || $('#views-exposed-form-account-win-page').length ) {
            $('input[type="text"]').addClass('form-control');
            /* Label Edit */
            $("label[for='edit-field-date-value-min']").text("From");
            $("label[for='edit-field-date-value-max']").text("To");

        }
        /* Bootstrap Select Styling */
        $('select').selectpicker();

        /* Live Events Landing Page - page class determines bkgd image */
        if ($('.page-live-events').length) {
            if ($('.header-intro.slim').length) {
                $('#page').addClass('default');
            }
            else {
               $('#page').addClass('header-true');
            }
        }
        /* Live Events Landing Page -- move view into content region of node-23 */
        $("#live-events-view-wrap").appendTo(".node-25736 .main-target");
        $("#live-events-view-wrap").appendTo(".node-26387 .main-target");
        $("#live-events-view-wrap").appendTo(".node-26388 .main-target");
        $("#live-events-view-wrap").appendTo(".node-26389 .main-target");
        /* Submenu Events - prevent 'All Live Events' from always being active */
        if ( $('body').is('[class*="page-live-events-"]') ) {
            $('.btn-mirren.active-trail.active').removeClass('active');
        }

        /* Mirren Tooltip Default to the Right */
        $('.mirren-tooltip').tooltip( {
            html: 'true',
            placement: 'right'
            /*delay: { hide:100000 } //theming hold */
        });

        //comment form (forum) text area default text
        if ( !($('.page-comment-edit').length) ) {
            $('#comment-form textarea')
            .focus(function(){ if (this.value == $(this).attr("data-default")) this.value = ''; })
            .blur(function(){ if (this.value == '') this.value = $(this).attr("data-default"); });
        }

        // Save comment if user logs in
        if (window.sessionStorage && window.sessionStorage.getItem("tmpComment")) {
          if ($.cookie("deleteTmpComment")) {
            $.cookie("deleteTmpComment", null, { path: '/' });
            window.sessionStorage.setItem("tmpComment", "");
          }
          else {
            $('#comment-form textarea').val(window.sessionStorage.getItem("tmpComment"));
          }
        }

        // Disable post button
        if ($('#comment-form .anon-comment-fields').length) {
          $('#comment-form .form-actions .form-submit').prop('readonly', true);
        }

        // Member vs Non-Member click
        $("#edit-membertoggle input").click(function() {
          if ($(this).val() == 'anon') {
            $(".anon-comment-fields").slideDown();
            $('#comment-form .form-actions .form-submit').prop('readonly', false);
          }
          // Member clicked
          else {
            window.sessionStorage.setItem("tmpComment", $("#comment-form textarea").val());

            $(".anon-comment-fields").hide();
            $('#comment-form .form-actions .form-submit').prop('readonly', true);
            $("#block-user-login h2").text("Login to post your comment");

            $.colorbox({
                inline: true,
                href: "#block-user-login",
                innerWidth: '430px',
                innerHeight:'285px',
                opacity: '0.5',
                onLoad: function() {
                    $("#block-user-login").show();
                    $('#cboxClose').remove();
                },
                onClosed: function() {
                    $("#block-user-login").hide();
                }
            });
          }
        });
        // Show modal on submit error
        if ($('#comment-form').length && $("#block-user-login input").hasClass("error")) {
          $(".messages").prependTo($("#block-user-login form"));
          $("#edit-membertoggle-user").click();
        }

        // Anon submit error
        if ($(".anon-comment-fields input").hasClass("error")) {
          $(".anon-comment-fields").show();
          $('#comment-form .form-actions .form-submit').prop('readonly', false);
        }

        $('#comment-form .form-actions .form-submit').mousedown(function() {
          if ($(this).prop("readonly")) {
            alert('Please select Member or Non-Member.');
          }
        });

        // Show Anon Thank You Modal
        if (window.location.href.indexOf("showAnonThankYou") != -1) {
          $.colorbox({
              //inline: true,
              html: '<div id="comment-anon-thanks"><h1>Thank you for your post.</h1><p>As a non-member, we will quickly review your posting before pushing it live. This is simply to eliminate SPAM and inappropriate content.</p><a href="#" class="btn-mirren" onclick="jQuery.colorbox.close();">close</a></div>',
              innerWidth: '440px',
              innerHeight:'120px',
              opacity: '0.5',
              closeButton:false
          });
        }


		$("#colorboxCloseBtn").click(function() {
			$.colorbox.close();
			});

        // Auto-expand chapter
        if ($("#block-views-training-chapters-block").length && document.location.hash) {
          var nid = document.location.hash.substr(1);
          $("#chapter-accordion .views-row-first.nid-" + nid).parent().parent().prev().find("a").click();
        }

        //-------------------------------------

        //404 page referer code----------------------
        var referer_url = document.referrer;
        //if there is a referer url
        if(document.referrer){
            //write the html on the 404 page for the link
            $('#referer-404').html('Revisit the <a href="' + document.referrer + '">previous page</a>.<br />');
        }
        //-------------------------------------------

        //Jquery Text Expander / More - Less Fun-----
        //my agency hub page
        $('.team-body').expander({
          slicePoint: 565
        });
        $('.user-bio-body').expander({
          slicePoint: 185
        });
        //------------------
        // /training-in-person page. bottom block
        $('#block-views-featured-blocks-block-26 .views-field-body-1 .field-content').expander({
          slicePoint: 165
        });
        //------------------
        // Forum Comment
        /*
$('#comments .field-name-comment-body .field-item').expander({
          slicePoint: 370
        });
*/
        //------------------
        // Content Types with 315 cutoff
        $('.node-type-lead .field-name-body, .node-type-lead .field-name-field-company-summary').expander({
          slicePoint: 550
        });
        // Account WIN no cutoff
        $('.node-type-account-win .field-name-body .field-item').expander({
          slicePoint: 10000
        });

        //------------------
        // Content Types with 565 cutoff
        $('.node-type-team .field-name-body .field-item, .node-type-search-consultant-company .field-name-body .field-item, .node-type-service-providers-company .field-name-body .field-item, .page-user .field-name-field-bio-user .field-item, .node-type-search-consultant-individual .field-name-body .field-item').expander({
          slicePoint: 10000
        });
        //------------------
        // Content Types with 365 cutoff
        $('.node-type-event .field-name-body .field-item').expander({
          slicePoint: 10000
        });
        // Content Types with 365 cutoff
        $('.view-display-id-block_1 .views-field .field-content').expander({
          slicePoint: 455
        });

        //------------------

        /* Read-More/Less Toggle */
        $('.content-access .read-more-link').click(function() {
           $(this).addClass('hidden');
           $(this).closest('.group-body-group').find('.read-less-link').removeClass('hidden').addClass('collapsed');
        });
        $('.content-access .read-less-link').click(function() {
           $(this).addClass('hidden');
           $(this).closest('.group-body-group').find('.read-more-link').removeClass('hidden').addClass('collapsed');
        });

        $(window).resize(function(){
            resizeFooter();
        });

        $("#webform-client-form-27405").appendTo("#form_destination");
        $("#webform-client-form-27719").appendTo("#form_destination");

        $("#webform-ajax-wrapper-27719").appendTo("#form_destination");



        /*$('#library-flag-block .flag-team-member-library a').each(function(e){
            $(this).html( $(this).html() + " +" );
        });*/

        // Delete library item confirmation
        var $flagLinks = $("#views-form-my-library-logged-in-user-block-1 a.flag");
        if ($flagLinks.length) {
          $flagLinks.click(function(e) {
            if (confirm("Remove from library?")) {
              setTimeout("location.reload();", 800);
            }
            else {
              e.stopImmediatePropagation(); // Prevent Ajax unflag click event
              e.preventDefault();
            }
          })
          // Reverse click event to prevent unflag Ajax from firing before this click
          .data('events').click.reverse();
        }

        if ( $('.page-search').length ) {
            $('.header-intro, .header-intro-content').addClass('slim');
        }

        /* Delete Comment Confirm Page -- add header & style button */
        if ( $('.page-comment-delete').length ) {
            $('#block-system-main .content').prepend('<div class="header-intro slim"><div class="header-intro-content slim clearfix"><div class="col-xs-7"><div class="header-intro-text"><h1>Confirm Comment Delete</h1></div></div></div></div>');
            $('#edit-submit').addClass('btn-mirren').css("margin-right", "20px");
        }
        if ( $('.page-comment-edit').length ) {
            $('#block-system-main .content').prepend('<div class="header-intro slim"><div class="header-intro-content slim clearfix"><div class="col-xs-7"><div class="header-intro-text"><h1>Edit Comment</h1></div></div></div></div>');
            $('#edit-submit').addClass('btn-mirren').css("margin-right", "20px");
        }
        if ( $('.page-comment-reply').length ) {
            $('#block-system-main .content').prepend('<div class="header-intro slim"><div class="header-intro-content slim clearfix"><div class="col-xs-7"><div class="header-intro-text"><h1>Comment Reply</h1></div></div></div></div>');
            $('#edit-submit').addClass('btn-mirren').css("margin-right", "20px");
        }

        // Remember My Email
        if ($("#user-login").length && window.localStorage) {
          if (window.localStorage.getItem("email")) {
            $("#edit-name").val(window.localStorage.getItem("email"));
          }
          $("#user-login").submit(function() {
            if ($("#edit-remember-me:checked").length) {
              window.localStorage.setItem("email", $("#edit-name").val());
            }
            else {
              window.localStorage.removeItem("email");
            }
          });
        }

    });

    $(document).ajaxComplete(function (event, xhr, settings) {
        /* Text Input and Select Element Styling */
        $('input[type="text"]').addClass('form-control');
        /* Label Edit */
        $("label[for='edit-field-date-value-min']").text("From");
        $("label[for='edit-field-date-value-max']").text("To");
        $('select').selectpicker();
    });

    //bootstrap collapse add class to extra close button - http://stackoverflow.com/a/19500278
    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".panel-close").removeClass("collapsed");
        }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".panel-close").addClass("collapsed");
    });
    //-------------------

    //membership page js
    $('#fee-table-tooltip-1, #fee-table-tooltip-2').tooltip({
        html: 'true',
        placement: 'right'
    });

    $('#conference-tooltip').tooltip({
        html: 'true',
        placement: 'left'
    });

    $('#fee-table-inner').collapse('hide');
    $('#hide-fee').click(function() {
        if ($(this).next('#fee-table-inner').hasClass('in') ) {
            $(this).html('Show Fee Table <span class="arrow-down"></span>');
        } else {
            $(this).html('Hide Fee Table <span class="arrow-up"></span>');
        }
    });

    $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
	html: true,
	trigger: 'hover',
	content: function() {
        var content = $(this).data('content-target');
        try { content = $(content).html() } catch(e) {/* Ignore */}
        return content || $(this).data('content');

	}
	});
	});


    //-----------------

    //jquery scroller---
    //http://manos.malihu.gr/jquery-custom-content-scroller/
    $(window).load(function(){
        $("#block-views-648f7a98eb2596af47232126f50c8083, #team-members-block, #block-views-team-members-block, #block-views-team-members-block-1, #block-views-team-members-block-2, #block-views-team-members-block-3, #block-views-d971e592fcfbf29ba57347d7d454191a").mCustomScrollbar({
            theme:"dark-thick"
        });

        resizeFooter();

        if ( $('.field-name-field-logo-team img').length ) {
            verticalCenterLogo();
        }

        /* Service Providers & Search Consultants
         * For anonymous users: remove link, trigger colorbox
         */
        if ( $('.content-blocked.page-node-25742').length ) {
            $(".sidebar .btn-mirren, .view-id-featured_blocks .view-content a, .intro-nav .last a")
            .removeAttr("href")
            .css("cursor", "pointer")
            .colorbox({
                inline: true,
                href: "#mirren-gate-modal",
                innerWidth: '450px',
                innerHeight:'120px',
                opacity: '0.5',
                onLoad: function() {
                    $('#cboxClose').remove();
                }
            });
        }

        $("a.read-more-link").each(function() {
            if ( $(this).height() > '18' ) {
                $(this).addClass('breaking-bad');
            }
        });

    });
    //--------------------

    /* Footer Gray Bar Height to Fill Window on Short Pages */
    function resizeFooter() {
        var theWindow        = $(window).height();
        var theHTML          = $('html').height();
        var theFooter        = $('footer').height();
        var theBody          = (theHTML - theFooter);
        var newFooterHeight  = (theWindow - theBody);

        // Conditional because <front> Masquerade Block
        if ( !($('#footer .block-masquerade').length) ) {
            $('#footer').css("height", newFooterHeight);
        }

    }

    function verticalCenterLogo() {
        var coLogoHeight = $('.field-name-field-logo-team img').height();
        var imgTopMargin = (165 - coLogoHeight) / 2;
        $('.field-name-field-logo-team img').css("margin-top", imgTopMargin).show();
    }



/* These are for the Special Access Page. When there is no content it will hide the parent DIV's */
 
     
   var $empty = jQuery(".field-name-field-survey-monkey-set > .field-items").filter(function() {
    return !$.trim(this.innerHTML);
});
if ($empty.length) {
    $empty.closest('#survey-monkey').hide();
    $('#survey-monkey-title').hide();
    
}


var $empty = jQuery(".field-name-field-youtube-video-set > .field-items").filter(function() {
    return !$.trim(this.innerHTML);
});
if ($empty.length) {
    $empty.closest('#youtube-section').hide();
    $('#youtube-title').hide();
    
}

var $empty = jQuery(".field-name-field-wistia-video-set > .field-items").filter(function() {
    return !$.trim(this.innerHTML);
});
if ($empty.length) {
    $empty.closest('#wistia-section').hide();
    $('#wistia-title').hide();
    
}

var $empty = jQuery("#call-to-action-1 > .container > .row > .col-sm-12").filter(function() {
    return !$.trim(this.innerHTML);
});
if ($empty.length) {
    $empty.closest('#call-to-action-1').hide();
        
}

var $empty = jQuery("#call-to-action-2 > .container > .row > .col-sm-12").filter(function() {
    return !$.trim(this.innerHTML);
});
if ($empty.length) {
    $empty.closest('#call-to-action-2').hide();
        
}









  /*
if ($(".view-display-id-live_sessions").html()) {
    if ($(".view-display-id-live_sessions").html().trim() == '') {
      $(".view-display-id-live_sessions").parent().parent().parent().parent().hide();
  	}

  	if ($(".view-display-id-completed_sessions").html().trim() == '') {
      $(".view-display-id-completed_sessions").parent().parent().parent().parent().hide();
  	}
  }
*/






})(jQuery);

