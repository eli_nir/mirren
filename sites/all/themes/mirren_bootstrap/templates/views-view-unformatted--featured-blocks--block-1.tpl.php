<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php 
	$total = count($rows);
	$i = 0; 
?>

<?php foreach ($rows as $id => $row): ?>
  <span<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php 
    	$i++;
    	print $row; 
    	if ($i != $total) echo '| ';
    ?>
  </span>
<?php endforeach; ?>