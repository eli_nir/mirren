<!DOCTYPE html>
<html lang="en" <?php print $html_attributes; ?>>
  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <!-- DISABLE RESPONSIVE -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <?php print $styles; ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Google Web Font -->
    <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,500,600,700,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>

  </head>

  <body class="<?php print $classes; ?> <?php if ( isset($roleClearance) ) { print $roleClearance; } ?> <?php if ( isset($training_type) ) { print $training_type; } ?>" <?php print $attributes; ?>>

	<!-- Visual Visitor -->

	<script type="text/javascript">
	var vv_account_id = 'ZQsSd2TRkp';
	var vv_BaseURL = (("https:" == document.location.protocol) ? "https://frontend.id-visitors.com/FrontEndWeb/" : "http://frontend.id-visitors.com/FrontEndWeb/");
	(function () {
	var va = document.createElement('script'); va.type = 'text/javascript'; va.async = true;
	va.src = vv_BaseURL + 'Scripts/liveVisitAsync.js';
	var sv = document.getElementsByTagName('script')[0]; sv.parentNode.insertBefore(va, sv);
	})();
	</script>


    <?php print $page_top; ?>
    <?php print $page; ?>


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php print $scripts; ?>
    <!-- <script src="<?php //print $base_path . $path_to_theme; ?>/assets/js/classie.js"></script>
    <script src="<?php //print $base_path . $path_to_theme; ?>/assets/js/uisearch.js"></script>
    <script>
      /* new UISearch( document.getElementById( 'sb-search' ) ); */
    </script>
    -->
    

    
    
    
    
    

    <?php print $page_bottom; ?>

  </body>
</html>