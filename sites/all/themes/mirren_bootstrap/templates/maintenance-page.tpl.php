<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <!-- DISABLE RESPONSIVE -->
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Le styles -->
  <?php print $styles; ?>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
  <![endif]-->
  <!-- Google Web Font -->
  <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic' rel='stylesheet' type='text/css'>
</head>
<body class="<?php print $classes; ?> <?php if ( isset($training_type) ) { print $training_type; } ?>" <?php print $attributes; ?>>
  <?php print $page_top; ?>
  <div id="page" class="<?php if ( isset($header_status) ) { print $header_status; } ?>">
    <div id="container" class="clearfix">
      <div id="main" class="clearfix container">
        <div class="row">
          <div class="col-xs-12">
            <div id="section1">
              <div class="block col-xs-10 col-xs-push-1"> 
                <a id="logo-link" href="http://www.mirren.org" title="Mirren.org">Mirren.org</a>
                <h1>Mirren’s New Business Network, Training and Resource Center.</h1>    
                <p><?php print $content; ?></p>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- /main -->
     </div> <!-- /container -->
  </div> <!-- /page -->
  <?php print $page_bottom; ?>
</body>
</html>
