<?php
// Theme Default Node Template
?>

<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
    
    <?php 
      // IF there is header content (#page.header-true)
       if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
    ?> 

    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>

      <div class="header-intro-content clearfix">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-xs-5">

          <?php // 1) IF feature_image_link is used, wrap the image
            if ( isset($node->field_feature_image_link['und'][0]['url']) ) { 
          ?>
            <a 
              href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
              role="button"
              <?php // IF the target attribute is set for "new window"
                if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) {
                  print 'target="_blank"';
                } 
              ?>
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
          <?php // 2) ELSE IF the Video ID field is used
            } elseif ( isset($node->field_video_id['und'][0]['value']) ) {
          ?>  
            <a 
              href="?width=960&amp;height=600&amp;inline=true#wistia-vid" 
              role="button"
              class="colorbox-inline"
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
            <div style="display:none;">
              <div id="wistia-vid">
                <iframe scrolling="no" width="960" height="540" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                <button class="btn-mirren colorbox-close">Close</button>
              </div>
            </div>
          <?php 
            } // 3) ELSE, not linked just an image
            else {
          ?>
            <?php print render($content['field_banner_image']); ?>
          <?php } ?>

        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->

    <?php 
      } // ELSE, no header content (#page or #page.default) 
      else { 
    ?>
      <div class="header-intro slim">  
        <?php 
          // Header Submenu Region
          if ( isset($header_submenu) ) { 
        ?>    
          <div class="intro-nav">
            <?php print render($header_submenu); ?>
          </div>
        <?php 
          }
        ?>

        <div class="header-intro-content slim clearfix">
          <div class="col-xs-7">
            <div class="header-intro-text">
              <?php print render($title_prefix); ?>
                <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
              <?php print render($title_suffix); ?>
              <?php print render($content['field_feature_intro']); ?>
            </div><!-- /.header-intro-text -->
          </div>
        </div><!-- /.header-intro-content -->
      </div><!-- /.header-intro.slim -->
    <?php 
      } 
    ?>


    <?php 
      // Full Width Banner Space
      if ( isset($full_width_banner) ) { 
    ?>    
      <div class="row">
        <div id="full_width_banner" class="col-xs-12">
          <?php print render($full_width_banner); ?>
        </div>
      </div>
    <?php 
      }
    ?>

    <div class="row">

      <div class="col-xs-8 main-target">

        <?php print render($content); ?>

        <?php if ( isset($node_body_content) ) { ?>
            <?php print render($node_body_content); ?>
        <?php } ?>

        <!-- CUSTOM FIELD COLLECTION -->    
        <div id="three_services" class="row">
          <?php $fc_count = 1; ?>
          <?php 
            foreach ($content['field_accordion_content']['#items'] as $entity_uri) : $field_collection_item = entity_load('field_collection_item', $entity_uri);
            foreach ($field_collection_item as $field_collection_object) : 

            $fc_title = $field_collection_object->field_title_accordion['und'][0]['value']; 
            $fc_body = $field_collection_object->field_body_area['und'][0]['safe_value']; 
          ?>
            <div class="col-xs-4">

              <div class="home-feature">
                <div class="hf-content"> 
                  <div class="hf-count"><?php print $fc_count; ?></div>
                  <div class="hf-text">
                    <h2><?php print $fc_title; ?></h2>
                    <?php print $fc_body; ?>
                  </div>
                </div>
              </div>

            </div>
            <?php $fc_count++; ?>
          <?php endforeach; endforeach; ?>
        </div>

      </div>

      <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php $nid = 2; print drupal_render(node_view(node_load($nid))); ?>
          <?php print render($sidebar_right_4); ?>
      </div>
        
    </div>
    
  </article>

<?php } ?>