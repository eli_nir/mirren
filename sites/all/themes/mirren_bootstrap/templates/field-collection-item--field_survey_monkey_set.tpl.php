<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

?>





<!--



<?php print $content['field_survey_title']['#items'][0]['value'];?><br>


<?php print $content['field_survey_description']['#items'][0]['value'];?><br>
-->



<div id="surveyMonkeyInfo" style="width:945px;font-size:10px;color:#666;">
<div style="border-radius: 0px; width: 945px; height:500px; overflow: hidden;">
<div class="box-content">
<div class="video-container">
<iframe allowtransparency="true" frameborder="0" id="sm_e_s" src="<?php print $content['field_survey_monkey']['#items'][0]['value']; ?>" width="945" height="500" style="border:0px" frameborder="0" allowtransparency="true"></iframe></div></div>
</div>
</div>