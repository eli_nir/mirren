<?php
// Theme Default Node Template
?>

<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php }else{ ?>
  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <div class="header-intro">  
      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>
      <div class="header-intro-content clearfix">
        <?php
        //this is the for the Forum Header Block Region / Block
        print render($forum_header_block);
        //----------------------------------------------------
        ?>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->
    <div class="row">
      <?php 
        if ( isset($sidebar_right_3) ) { 
          print '<div class="col-xs-9 main-target">';
        } 
        elseif ( isset($sidebar_left_3) ) {
          print '<div class="col-xs-9 col-xs-push-3 main-target">';
        }
        elseif ( isset($sidebar_right_4) ) {
          print '<div class="col-xs-8 main-target">';
        }
        else {
          print '<div class="col-xs-12 main-target">';
        }
      ?>

        <?php print render($content); ?>

        <?php if ( isset($node_body_content) ) { ?>
            <?php print render($node_body_content); ?>
        <?php } ?>

        <?php // Embed Views Pages
          if (($node->nid) == '25732') { 
            print views_embed_view('lead_view',"page"); 
          }
          elseif (($node->nid) == '25738') { 
            print views_embed_view('account_win',"page"); 
          }
          elseif (($node->nid) == '25764') { 
            //adding view page for the forum - /admin/structure/views/view/forum_landing/edit
            print views_embed_view('forum_landing',"page"); 
          }
          else {

          }   
        ?>

      </div>

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-3 sidebar-right-3 sidebar">
          <?php print render($sidebar_right_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>

<?php } ?>