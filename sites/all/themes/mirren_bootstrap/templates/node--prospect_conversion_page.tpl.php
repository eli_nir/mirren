
<?php
// Theme Default Node Template
hide($content['field_sa_youtube_video']);
hide($content['field_sa_youtube_titles']);
hide($content['field_sa_wistia_title']);
hide($content['field_sa_wistia_video']);
hide($content['field_sa_survey_monkey_title']);
hide($content['field_survey_monkey_set']);
hide($content['field_sa_call_to_action_1']);
hide($content['field_sa_call_to_action_2']);
hide($content['field_free_wistia_video_set']);
?>



<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
    
    <?php 
      // IF there is header content (#page.header-true)
       if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
    ?> 

    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>

      <div class="header-intro-content clearfix">
        <div class="col-sm-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-sm-5">

          <?php // 1) IF feature_image_link is used, wrap the image
            if ( isset($node->field_feature_image_link['und'][0]['url']) ) { 
          ?>
            <a 
              href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
              role="button"
              <?php // IF the target attribute is set for "new window"
                if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) {
                  print 'target="_blank"';
                } 
              ?>
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
          <?php // 2) ELSE IF the Video ID field is used
            } elseif ( isset($node->field_video_id['und'][0]['value']) ) {
          ?>  
            <a 
              href="?width=960&amp;height=600&amp;inline=true#wistia-vid" 
              role="button"
              class="colorbox-inline"
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
            <div style="display:none;">
              <div id="wistia-vid">
                <iframe scrolling="no" width="960" height="540" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                <button class="btn-mirren colorbox-close">Close</button>
              </div>
            </div>
          <?php 
            } // 3) ELSE, not linked just an image
            else {
          ?>
            <?php print render($content['field_banner_image']); ?>
          <?php } ?>

        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->

    <?php 
      } // ELSE, no header content (#page or #page.default) 
      else { 
    ?>
      <div class="header-intro slim">  
        <?php 
          // Header Submenu Region
          if ( isset($header_submenu) ) { 
        ?>    
          <div class="intro-nav">
            <?php print render($header_submenu); ?>
          </div>
        <?php 
          }
        ?>

        <div class="header-intro-content slim clearfix">
          <div class="col-xs-7">
            <div class="header-intro-text">
              <?php print render($title_prefix); ?>
                <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
              <?php print render($title_suffix); ?>
              <?php print render($content['field_feature_intro']); ?>
            </div><!-- /.header-intro-text -->
          </div>
        </div><!-- /.header-intro-content -->
      </div><!-- /.header-intro.slim -->
    <?php 
      } 
    ?>


    <?php 
      // Full Width Banner Space
      if ( isset($full_width_banner) ) { 
    ?>    
      <div class="row">
        <div id="full_width_banner" class="col-xs-12">
          <?php print render($full_width_banner); ?>
        </div>
      </div>
    <?php 
      }
    ?>

    <div class="row">

      <?php 
        if ( isset($sidebar_right_3) ) { 
          print '<div class="col-xs-9 main-target">';
        } 
        elseif ( isset($sidebar_left_3) ) {
          print '<div class="col-xs-9 col-xs-push-3 main-target">';
        }
        elseif ( isset($sidebar_right_4) ) {
          print '<div class="col-xs-8 main-target">';
        }
        else {
          print '<div class="col-xs-12 main-target">';
        }
      ?>

        <?php print render($content); ?>

        <?php if ( isset($node_body_content) ) { ?>
            <?php print render($node_body_content); ?>
        <?php } ?>
        
           
        
        

        <?php // Embed Views Pages
          if (($node->nid) == '25732') { 
            print views_embed_view('lead_view',"page"); 
          }
          elseif (($node->nid) == '25738') { 
            print views_embed_view('account_win',"page"); 
          }
          elseif (($node->nid) == '25764') { 
            //adding view page for the forum - /admin/structure/views/view/forum_landing/edit
            print views_embed_view('forum_landing',"page"); 
          }
          elseif (($node->nid) == '25840' && $contentAccess ) { 
            //adding view page for /service-providers-all - /admin/structure/views/view/all_service_search/edit/page_1
            print views_embed_view('all_service_search',"page_1"); 
          }
          elseif ( ($node->nid) == '25841' && $contentAccess ) { 
            //adding view page for /service-providers-all- /admin/structure/views/view/all_service_search/edit/page
            print views_embed_view('all_service_search',"page"); 
          }
          else {

          }   
        ?>
        

<div id="survey-monkey-title">
<div class="container">
<div class="row">
<div class="col-sm-12">

<?php print render($content['field_sa_survey_monkey_title']); ?> 

</div>
</div>
</div>
</div>


<!--
/**************************/
/* Survey Monkey          */
/**************************/-->


<div id="survey-monkey">
<div class="container">
<div class="row">
<div class="col-sm-12">

<?php print render($content['field_survey_monkey_set']); ?> 

</div>
</div>
</div>
</div>


        
    
<div id="youtube-title">
<div class="container">
<div class="row">
<div class="col-sm-12">

<?php print render($content['field_sa_youtube_titles']); ?>


</div>
</div>
</div>
</div>    
 

<!--
/**************************/
/* YouTube Video          */
/**************************/-->

<div id="youtube-section">
<div class="container">
<div class="row">
<div class="col-sm-12">

<?php print render($content['field_sa_youtube_video']); ?>

<?php print render($content['field_free_wistia_video_set']); ?>


</div>
</div>
</div>
</div>


<!--Call to Action 1-->

<div id="call-to-action-1">
<div class="container">
<div class="row">
<div class="col-sm-12">

<?php print render($content['field_sa_call_to_action_1']); ?></a>

</div>
</div>
</div>
</div>  

<div id="wistia-title">
<div class="container">
<div class="row">
<div class="col-sm-12">


<?php print render($content['field_sa_wistia_title']); ?></a>


</div>
</div>
</div>
</div> 


<!--
/**************************/
/* Wistia Video           */
/**************************/-->

<div id="wistia-section">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="login-more-activate">
<ul>
<li><?php print l('Log In', 'user', array('query' => drupal_get_destination() ) ); ?></li>
<li><a href="/membership">Learn more about membership</a></li>
<li><a href="/membership/nojs/trial">Activate your 3-week trial</a></li>
</ul>
</div>

<?php print render($content['field_sa_wistia_video']); ?> 


</div>
</div>
</div>
</div> 

   
    </div>


<div id="call-to-action-2">
<div class="container">
<div class="row">
<div class="col-sm-12">


<?php print render($content['field_sa_call_to_action_2']); ?></a>

</div>
</div>
</div>
</div>  



  

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-3 sidebar-right-3 sidebar">
          <?php print render($sidebar_right_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>


<script>
    
  </script>




<?php } ?>