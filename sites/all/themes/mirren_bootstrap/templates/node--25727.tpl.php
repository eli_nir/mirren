<?php //homepage - no teaser, header intro area only ?>
<article id="section1" class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="block col-xs-10 col-xs-push-1"> 
    <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
    <?php print render($content); ?>
  </div>
</article>