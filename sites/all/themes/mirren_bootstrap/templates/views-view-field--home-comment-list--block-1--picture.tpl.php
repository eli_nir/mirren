<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
//this is to add avatars to the users with no images
//if there is an image
if(trim($output)){
	//print image
	print $output; 
}else{


$max = 44;
$numbers = range(0, $max);

// Retrieve already used numbers, if any.
$already_used = unserialize(variable_get('mirren_bootstrap_numbers_already_used', array()));

// Subtract already used numbers from $numbers.
$numbers = array_values(array_diff($numbers, $already_used));

shuffle($numbers);

// add current number to $already_used.
$already_used[] = $number = array_pop($numbers);

// Take care that the stored array of numbers is emptied if contains all the numbers.
$new_already_used = (count($already_used) <= $max) ? $already_used : array();
variable_set('mirren_bootstrap_numbers_already_used', serialize($new_already_used));
      
	  
print "<img src='/sites/all/themes/mirren_bootstrap/assets/images/avatar_selection/member_default_" . $number . ".jpg' width='56' height='56' />";
      

}
?>

