<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?><!-- jQuery library (served from Google) -->

<html>
<head>
    <!--
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript">
</script>
-->
    <!--
   <script src="/sites/all/themes/mirren_bootstrap/assets/js/jquery.bxslider.min.js" type="text/javascript">
</script>
    <link href="/sites/all/libraries/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css">
-->

    <title></title>

    <title></title>
</head>

<body>
    <div id="page" class="<?php if ( isset($header_status) ) { print $header_status; } ?>">
        <div id="secondary_menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php print render($page['header']); ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="main_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php print render($page['navigation']); ?><!-- Collapsed Nav -->

                        <nav class="navbar navbar-default collapsed-nav" role="navigation">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"></button> <a class="navbar-brand" href="http://www.mirren.com/">Mirren</a>
                                </div><!-- Collect the nav links, forms, and other content for toggling -->

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a href="/leads">Daily Leads</a></li>

                                        <li><a href="/training-online">On-Demand Learning</a></li>

                                        <li><a href="/training-in-person">In Person Training</a></li>

                                        <li><a href="/live-events">Live Events</a></li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">More</a>

                                            <ul class="dropdown-menu">
                                                <li><a href="/membership">Membership</a></li>

                                                <li><a href="/search-consultants">Search Consultants</a></li>

                                                <li><a href="/service-providers">Service Providers</a></li>

                                                <li><a href="/discussion-forums">Forums</a></li>

                                                <li><a href="/about-us">About / Contact</a></li>

                                                <li><a href="/blog">Blog</a></li>

                                                <li><a href="/subscribe">Mailing List</a></li>

                                                <li>
                                                    <!-- START OF LOGIN/LOGOUT BLOCK -->
                                                    <?php global $user;
                                                                                                            if ($user->uid != 0) {
                                                                                                           // code for the logout button ?>

                                                    <ul class="login-block">
                                                        <li class="first leaf"><a href="/user" title="">My Profile</a></li>

                                                        <li class="leaf"><a href="/my-agency-hub" title="">My Agency Hub</a></li>

                                                        <li class="leaf"><a href="/my-library" title="">My Library</a></li>

                                                        <li class="last leaf"><a href="/user/logout" title="">Logout</a></li>
                                                    </ul><?php
                                                                                                                    }
                                                                                                                 else {
                                                                                                                  // code for the login button
                                                                                                               echo "<a class=\"user-link\" href=\"?q=user\">Login</a>";
                                                                                                                 }
                                                                                                              ?><!-- END OF LOGIN/LOGOUT BLOCK -->
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        
        <?php print render($page['page_header_block']); ?>

        <div id="main" class="clearfix container">
            <div class="row">
                <div class="col-md-12">
                    <?php print $messages; ?><?php if ($tabs) { ?>

                    <div class="tabs">
                        <?php print render($tabs); ?>
                    </div><?php } ?><?php print render($page['help']); ?><?php if ($action_links) { ?>

                    <ul class="action-links">
                        <?php print render($action_links); ?>
                    </ul><?php } ?><?php print render($page['content']); ?><?php print $feed_icons; ?>
                </div>
            </div><!-- /.row -->
        </div><!-- /#main -->
        <!-- SECTION 2 -->
        <!-- SECTION 3 -->
        <?php print render($page['specialaccess']);?>
    
    
    
    
    </div>

    <div id="call-to-action-two">
        <div class="container">
            <div class="row">
                <div class="flickr-container">
                    <?php print render($page['advertising_banner']);?>
                </div>
            </div>
        </div>
    </div><!--*****Call to Action - End*****-->
    
    
    
    <!--*****Broadcast Form - Start*****-->

    <div id="broadcast-form">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 style="margin:0px; font-size:26px;">Activate your membership here: takes 3 minutes</h1>

                    <p style="font-size: 14px;"><a href="/membership" target="_blank">Learn even more about membership before you activate ›</a></p>

                    <div id="name-choice-view">
                        <?php print render($page['conference_form']);?>
                    </div>
                </div>
            </div>
        </div>

        <div class="hr-rule"></div>
    </div><!--*****Broadcast Form - End*****-->
   
   
   
   
    <!--
<div id="mirren-business-training">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>ON THE HOUSE: HERE'S SOME ADDITIONAL NEW BUSINESS STRATEGY FOR YOU</h1><span>No membership required, check out these on-demand videos that will provide insight into two key areas of converting business. </span>
                    </div>
                </div>
            </div>
        </div>
    
    
    <div id="mirren-business-training-videos">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        
                    <iframe src="//fast.wistia.net/embed/iframe/c8rpkph0cq" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="400" height="250"></iframe>   
                    
                    <h4>Competitive Pitching / RFPs || Chapter 1: Pitch Discovery: The Truth About the Pitch</h4>
                    <h1>Uncover What the Client Really Wants</h1>
                    <h2>How to Inject a Discovery Process into Every Competitive Review</h2>
                    <p>The first part of this video series will provide you with a few key principles that will better help you understand whether or not it is a "real" competitive review and more about what the prospect is looking for.</p>
                                         
                        
                        
                        
                    </div>
                    
                    
                    <div class="col-sm-6">
                        
                        <iframe src="//fast.wistia.net/embed/iframe/6az6ytteiy" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="400" height="250"></iframe>                             
                        
                    <h4>Proactive ProspectingChapter 1: Prospect Targeting</h4>
                    <h1>Developing Your Optimal Target List</h1>
                    <h2>Identifying Your Ideal Target Prospects (For The Shortest Sales Cycle)</h2>
                    <p>Part one of this series discusses how to build your optimal target list - your "low hanging fruit." It is critical that you approach your prospecting in a manner that targets only those prospects that you can most quickly and easily convert into new assignments. In this session we'll also review the critical concept of "perceived credibility" and the role this plays in building your prospecting program.</p>
                                         
      </div>
                    
                    
                    
                </div>
            </div>
        </div>
-->
    <!--
<div id="mirren-membership-drive">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h1>Drive your growth all year long with mirren membership</h1>
                    </div>
                     <div class="col-sm-4">
                   <button class="btn btn-primary" type="button"><strong>Learn More</strong><br />
About Membership ›</button>
                    </div>
                    
                    
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <ul class="left">
                            <li><span>Instant Access to Daily Leads</span>No other source uncovers as many leads, gets them to you faster and helps you convert the business.</li>

                            <li><span>Live Online Classes</span>Interact live with a Mirren Instructor, follow along with a workbook.</li>
                        </ul>
                    </div>

                    <div class="col-sm-6">
                        <ul class="right">
                            <li><span>On-Demand Learning</span>On demand video-based new business training for your entire agency (130+ videos).</li>

                            <li><span>Search Consultant Center</span>Full search consultant directory, contact info &training.</li>
                        </ul>
                    </div>
                </div>
          </div>
</div>
    
    
    
<div id="mirren-three-business-services">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>We provide Three New Business Services To Agencies</h1>
                    </div>
                  
                    
                    
                </div>

                <div class="row">
                    <div class="col-sm-4">
                       
<h2>Training In-Person</h2>
<p>Bring Mirren to your agency for<br>Advanced New Business Training<br>for your entire team</p>
                       
<a class="btn-mirren" title="Training In-Person" href="training-in-person">Training In-Person › </a>                       
                       
                    </div>

                    <div class="col-sm-4">
                        
<h2>Live Events</h2>
<p>Live Online Classes / Webinars,<br>Fall Workshops, Mirren Live<br>New York (Annual Conference)</p>

<a class="btn-mirren" title="Training In-Person" href="live-events">See Live Events › </a>  
                        
                    </div>

<div class="col-sm-4">
                        
<h2>Mirren Membership</h2>
<p>Online Access: Daily Leads,<br> On-Demand Learning, Search<br>Consultants, Discussions & more</p>
                        
<a class="btn-mirren" title="Training In-Person" href="membership">Mirren Membership › </a>                          
                        
                    </div>



                </div>
          </div>
</div>

-->
    <!--*****Footer - Start*****-->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php print render($page['footer']);?>
                </div>
            </div>
        </div><!--*****Footer - End*****-->
        <!-- /.section, /#footer -->

        <div style="display:none;">
            <a class="hidden mirren-gate">Log In</a> <!-- Content Triggered by ".mirren-gate" class click -->

            <div id="mirren-gate-modal">
                <?php /* print '<h2>' . t('Join Mirren to view this content') . '</h2>'; */ ?><?php print '<p>' . t('You are now being redirected to view this live session. You will need the password provided in the email you received today (unique password for each day). When logging in, be sure to use your agency email address.<p>Please note, that some sessions may start a few minutes late as we are working hard to move 400 attendees around the venue.') . '</p>'; ?><?php /* print l('Log In', 'user', array('query' => drupal_get_destination(), 'attributes' => array('class' => array('btn-mirren', 'btn-jumbo')) ) ); */ ?><?php print l('Watch Now ›', 'membership', array('attributes' => array('class' => array('btn-mirren')) ) ); ?><!-- <button class="btn-mirren btn-jumbo colorbox-close">Cancel</button> -->
            </div>
        </div>

        <div class="hidden-popup">
            <div id="inline_content">
                <div id="conference-fee-table">
                    <img src="/sites/all/themes/mirren_bootstrap/assets/images/fee_table.png">

                    <div class="close_button">
                        <button class="btn btn-primary" onclick="jQuery.colorbox.close();" type="button">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- /#page -->
    <script type="text/javascript">
            
    (function ($) { 
    $(document).ready(function(){
    $('.slider1').bxSlider({
    slideWidth: 175,
    minSlides: 1,
    maxSlides: 5,
    slideMargin: 10,
    controls: true,
    captions: true,
    nextText: 'Next',
    prevText: 'Previous',
    onSliderLoad: function(){
    $(".slider1").css("visibility", "visible");
    }    
    });  
    });
    }(jQuery));
    </script>
</body>
</html>
