<?php
// Theme Default Node Template
?>


<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
    
    <?php 
      // IF there is header content (#page.header-true)
       if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
    ?> 

    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>

      <div class="header-intro-content clearfix">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-xs-5">

          <?php // 1) IF feature_image_link is used, wrap the image
            if ( isset($node->field_feature_image_link['und'][0]['url']) ) { 
          ?>
            <a 
              href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
              role="button"
              <?php // IF the target attribute is set for "new window"
                if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) {
                  print 'target="_blank"';
                } 
              ?>
            >
            
              <?php print render($content['field_banner_image']); ?>
              
            </a>
          <?php // 2) ELSE IF the Video ID field is used
            } elseif ( isset($node->field_video_id['und'][0]['value']) ) {
          ?>  
            <a 
              href="?width=960&amp;height=600&amp;inline=true#wistia-vid" 
              role="button"
              class="colorbox-inline"
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
            <div style="display:none;">
              <div id="wistia-vid">
                <iframe scrolling="no" width="960" height="540" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                <button class="btn-mirren colorbox-close">Close</button>
              </div>
            </div>
          <?php 
            } // 3) ELSE, not linked just an image
            else {
          ?>
            <?php print render($content['field_banner_image']); ?>
          <?php } ?>

        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->

    <?php 
      } // ELSE, no header content (#page or #page.default) 
      else { 
    ?>
      <div class="header-intro slim">  
        <?php 
          // Header Submenu Region
          if ( isset($header_submenu) ) { 
        ?>    
          <div class="intro-nav">
            <?php print render($header_submenu); ?>
          </div>
        <?php 
          }
        ?>

        <div class="header-intro-content slim clearfix">
          <div class="col-xs-7">
            <div class="header-intro-text">
              <?php print render($title_prefix); ?>
                <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
              <?php print render($title_suffix); ?>
              <?php print render($content['field_feature_intro']); ?>
            </div><!-- /.header-intro-text -->
          </div>
        </div><!-- /.header-intro-content -->
      </div><!-- /.header-intro.slim -->
    <?php 
      } 
    ?>





    <div class="row">

<?php print '<div class="col-xs-12 main-target">';?>


<!-- Check to see if the user is logged in. If not, have them log in -->

     
<?php if ($logged_in): ?>

  <?php print render($content); ?>

        <?php if ( isset($node_body_content) ) { ?>
            <?php print render($node_body_content); ?>
        <?php } ?>
      </div>
  
<?php else: ?>
  
 
  
 <div class="credit-login">
    <a class="hidden mirren-gate">Log In</a>
    <!-- Content Triggered by ".mirren-gate" class click -->
    <div id="mirren-gate-modal">
      <?php print '<h2>' . t('Please Log In') . '</h2>'; ?>
      <p>To bring your membership account to current billing status,
      please log in below and then update your billing information.<br>
      If you have any questions, please contact <a class="colorbox-inline" href="?width=670&amp;height=340&amp;inline=true#member-services-modal">member services.</a></p>
      <?php print l('Log In', 'user', array('query' => drupal_get_destination(), 'attributes' => array('class' => array('btn-mirren', 'btn-jumbo')) ) ); ?>
    </div>
  </div>

  			<div style="display:none;">
                  <div id="member-services-modal">
                  
                  
					  	<div class="content">
						  		<h1>We Are Here To Help</h1>

						  			<p>If you have any questions, please contact Member Services by emailing<br /><a href="mailto:member.services@mirren.com">member.services@mirren.com</a> or call 212-388-9544.</p>

						  				<p>We are available Monday through Friday, from 9:00am – 7:00pm<br />
							  					Eastern. You may also contact us after hours by email, as we're<br />
							  						often online outside the main hours.</p>
						</div>
						<button class="btn-mirren colorbox-close">Close</button>
				</div>				
        
         </div>
               
  
<?php endif; ?>
           
    </div>
    
  </article>

<?php } ?>