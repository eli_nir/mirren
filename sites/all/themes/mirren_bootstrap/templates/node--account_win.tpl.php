<?php
// Account Win Content Type Node Template
// Clone of node--lead.tpl.php with field customizations for teaser
?>

<?php if ($teaser) { ?>

<article class="lead-teaser node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <div class="row">
    <div class="col-xs-11">
     
      <a class="lead-category-icon-wrap" href="<?php print $node_url; ?>">
        <img src="<?php print image_style_url('account_win_teaser', $node->field_banner_image['und'][0]['uri']); ?>" alt="<?php print $node->field_banner_image['und'][0]['alt']; ?>" />
        <span class="lead-category-icon <?php if ( isset($lead_category) ) { print $lead_category; } ?>"></span>
      </a>
      <div class="lead-details">
        <?php print render($title_prefix); ?>  
        <h2<?php print $title_attributes; ?>>
          <a href="<?php print $node_url; ?>">
            <?php // IF logged in, show default title
              if ($contentAccess) { ?>
              <?php print $title; ?>
            <?php } 
              else { ?>
              <?php // Not logged in, show non-member title if it exists
               if ( !empty($content['field_title_non_member']) ) { ?>
                <?php print render($content['field_title_non_member']); ?>
              <?php } // ELSE, show default title
                else { ?>
                <?php print $title; ?>
              <?php } ?>
            <?php } ?> 
          </a>
        </h2>
        <?php print render($title_suffix); ?>
        <?php if (isset($node->field_feature_intro['und'][0]['value']) ) {
            print '<p>' . ($node->field_feature_intro['und'][0]['value']) . '</p>'; 
          } 
        ?>
      </div>
    </div>
    
    <div class="col-xs-1">
      <div class="lead-date-col">
        <span class="lead-month-day"><?php print format_date($node->field_date['und'][0]['value'], "custom", "M d"); ?></span>
        <span class="lead-time"><?php print format_date($node->field_date['und'][0]['value'], "custom", "h:i A"); ?></span>
      </div>
    </div>
  </div>

  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
    <?php 
      // IF there is header content (#page.header-true)
       if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
    ?>
    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>
      <div class="header-intro-content clearfix">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>>
                <?php // IF logged in, show default title
                  if ($contentAccess) { ?>
                  <?php print $title; ?>
                <?php } 
                  else { ?>
                  <?php // Not logged in, show non-member title if it exists
                   if ( !empty($content['field_title_non_member']) ) { ?>
                    <?php print render($content['field_title_non_member']); ?>
                  <?php } // ELSE, show default title
                    else { ?>
                    <?php print $title; ?>
                  <?php } ?>
                <?php } ?>
              </h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-xs-5">

          <?php // 1) IF feature_image_link is used, wrap the image
            if ( isset($node->field_feature_image_link['und'][0]['url']) ) { 
          ?>
            <a 
              href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
              role="button"
              <?php // IF the target attribute is set for "new window"
                if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) {
                  print 'target="_blank"';
                } 
              ?>
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
          <?php // 2) ELSE IF the Video ID field is used
            } elseif ( isset($node->field_video_id['und'][0]['value']) ) {
          ?>  
            <a 
              href="?width=960&amp;height=600&amp;inline=true#wistia-vid" 
              role="button"
              class="colorbox-inline"
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
            <div style="display:none;">
              <div id="wistia-vid">
                <iframe scrolling="no" width="960" height="540" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                <button class="btn-mirren colorbox-close">Close</button>
              </div>
            </div>
          <?php 
            } // 3) ELSE, not linked just an image
            else {
          ?>
            <?php print render($content['field_banner_image']); ?>
          <?php } ?>

        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->
    <?php 
      } // ELSE, no header content (#page or #page.default) 
      else { 
    ?>
    <div class="header-intro slim">  
      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>

      <div class="header-intro-content slim clearfix">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro.slim -->
    <?php 
      } 
    ?>

    <div class="row">

      <?php 
        if ( isset($sidebar_right_3) ) { 
          print '<div class="col-xs-9">';
        } 
        elseif ( isset($sidebar_left_3) ) {
          print '<div class="col-xs-9 col-xs-push-3">';
        }
        elseif ( isset($sidebar_right_4) ) {
          print '<div class="col-xs-8">';
        }
        else {
          print '<div class="col-xs-12">';
        }
      ?>
        
        <?php 
        //hide the flagging link from the body content
        hide($content['flag_team_library']);
        hide($content['flag_team_member_library']); 
        print render($content); 
        ?>  

        <?php if ( isset($content['field_accordion_content']) ) { ?>
          <!-- FIELD COLLECTION -->    
          <div class="panel-group" id="fc-accordion">
            <?php $fc_count = 1; ?>
            <?php 
              foreach ($content['field_accordion_content']['#items'] as $entity_uri) : $field_collection_item = entity_load('field_collection_item', $entity_uri);
              foreach ($field_collection_item as $field_collection_object) : 

              $fc_title = $field_collection_object->field_title_accordion['und'][0]['value']; 
              $fc_body = $field_collection_object->field_body_area['und'][0]['safe_value']; 
            ?>

            <div class="panel ">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="collapsed" data-toggle="collapse" data-parent="#fc-accordion" href="#fc-<?php print $fc_count; ?>">
                    <strong class="numeral"><?php print $fc_count; ?></strong> <?php print $fc_title; ?> <i class=""></i>
                  </a>
                </h4>
              </div>
              <div id="fc-<?php print $fc_count; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                  <?php print $fc_body; ?>
                </div>
              </div>
            </div>

              
              <?php $fc_count++; ?>
            <?php endforeach; endforeach; ?>
          </div>
        <?php } ?>

      </div>

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-3 sidebar-right-3 sidebar">
          <?php
          //this is for the flaggin block. This is part of the node content
          if ( ($contentAccess) && isset($content['flag_team_library']) ) { 
            print '<div class="block block-block" id="library-flag-block">';
            print "<h2>" . t('Library') . "</h2>";
            print "<div class='content'><ul>";
            print "<li>" . render($content['flag_team_member_library']) . "</li>";
            print "<li>" . l(t('View My Library ›'), 'my-library') . "</li>";
            print "<li>" . render($content['flag_team_library']) . "</li>";
            //print "<li>" . l(t('View Team Library'), 'team-library') . "</li>";
            print "</ul></div>";
            print "</div>";
          }
          ?>
          <?php print render($sidebar_right_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>

<?php } ?>