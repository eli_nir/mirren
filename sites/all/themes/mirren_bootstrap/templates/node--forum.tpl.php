<?php
// Theme Default Node Template
?>

<?php if ($teaser) { ?>
  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
      <?php if (!$page) { ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php } ?>
    <?php print render($title_suffix); ?>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
    <?php print render($content['links']); ?>
    <?php print render($content['comments']); ?>
  </article>
<?php }else{ ?>
  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <div class="header-intro">  
      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>
      <div class="header-intro-content clearfix">
        <?php
        //this is the for the Forum Header Block Region / Block
        print render($forum_header_block);
        //----------------------------------------------------
        ?>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->

    
    <div class="row">
      <div class="col-xs-12 main-target">
      <h2><?php print $title; ?></h2>
      <div id="forum-post">
        <?php
        //load author user info
        //https://api.drupal.org/api/drupal/modules!user!user.module/function/user_load/7
        $author_user = user_load($uid);
        //get user pic uri 
        $author_picture_uri = $author_user->picture->uri;
        //theme the user pic with an image style
        $author_picture = theme(
          'image_style',
          array(
          'path' => $author_picture_uri,
          'style_name' => 'thumbnail'
          )
        );
        if(isset($author_picture_uri)){
          //print user pic in link
          print "<div id='author-image'>";
          print l($author_picture, 'user/' . $uid, array('html' => TRUE));
          print "</div>";
        }
        //put user first and last name together
        $author_full_name = $author_user->field_first_name['und'][0]['safe_value'] . "&nbsp;" . $author_user->field_last_name['und'][0]['safe_value'];
        //print out author full name in link and position
        print "<h3 id='author-name-title'>";
        print l($author_full_name, 'user/' . $uid, array('html' => TRUE));


        //going to check if the Team Node Reference Field contains data
        if(isset($author_user->field_team_user['und'][0]['nid'])){
          //load the Team Node Reference Data 
          $node_team = node_load($author_user->field_team_user['und'][0]['nid']);
          print ",&nbsp;";
          print $node_team->title;
        }
        print "</h3>";
        //-----------
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        print render($content);
        ?>
      </div>
      <?php //print render($content['links']); ?>
      <?php print render($content['comments']); ?>
  
    </div>  
  </article>
<?php } ?>