<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php 
# Bootstrap Collapse needs an identifier to pair each toggle with its target
# not a foreach loop, so the iterative method (i++) does not count
# views count gets rows, not groups
# alternative method, convert title string to group identifier
// remove non-alphanumeric characters from the title
$row_id = preg_replace('~[^\p{L}\p{N}]++~u', '', $title);
// convert the string to all lowercase
$row_id = strtolower($row_id);
// trim to 20 characters
$row_id = substr($row_id, 0, 20);
?>
<div class="panel">

	<div class="panel-heading">
		<h4 class="panel-title">			
			<a class="collapsed" data-toggle="collapse" data-parent="#chapter-accordion" data-target="#<?php print $row_id; ?>">
			  <?php print $title; ?> <i class=""></i>
			</a>
		</h4>
	</div>

	<div id="<?php print $row_id; ?>" class="panel-collapse collapse">
		<div class="panel-body">

			<?php foreach ($rows as $id => $row): ?>
			  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
			    <?php print $row; ?>
			  </div>
			<?php endforeach; ?>

		</div>
	</div>
</div> 


