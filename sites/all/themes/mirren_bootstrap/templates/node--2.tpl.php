<?php
 /*
  * Barebones Node Template - "Getting a Hold of Us" aka Contact Us
  * is embedded in the sidebar of About Us (node/3)
  */
?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php print render($title_suffix); ?>

  <div class="row">
    <div class="col-xs-12">
      <?php print render($content); ?>
      <?php if ( isset($node_body_content) ) { ?>
          <?php print render($node_body_content); ?>
      <?php } ?>
    </div>
  </div>
  
</article>
