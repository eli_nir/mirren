<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<h2 data-target="#recently-viewed-list" data-toggle="collapse" class="collapsed"><?php print $view->human_name; ?> <i></i></h2>

<div id="recently-viewed-list" class="collapse">
	<ul>
	<?php foreach ($rows as $id => $row): ?>
	  <li<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
	    <?php print $row; ?>
	  </li>
	<?php endforeach; ?>
	</ul>
</div>