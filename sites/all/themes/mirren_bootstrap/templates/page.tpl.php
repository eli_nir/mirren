<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<div id="page" class="<?php if ( isset($header_status) ) { print $header_status; } ?>">

  <div id="secondary_menu">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <?php print render($page['header']); ?>
        </div>
      </div>
    </div>
  </div>

  <div id="main_menu">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <?php print render($page['navigation']); ?>
        </div>
      </div>
    </div>
  </div>

  <?php print render($page['page_header_block']); ?>

  <div id="main" class="clearfix container">

    <div class="row">

        <div class="col-xs-12">
          <?php print $messages; ?>

          <?php if ($tabs) { ?><div class="tabs"><?php print render($tabs); ?></div><?php } ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links) { ?><ul class="action-links"><?php print render($action_links); ?></ul><?php } ?>

          <?php print render($page['content']); ?>

          <?php print $feed_icons; ?>
        </div>

    </div><!-- /.row -->

  </div><!-- /#main -->

  <!-- SECTION 2 -->
  <?php if ($page['featured']) { ?>
    <div id="featured"><?php print render($page['featured']); ?></div>
  <?php } ?>

  <!-- SECTION 3 -->
  <?php if ( ($page['three_col_left']) || ($page['three_col_middle']) || ($page['three_col_right']) ) { ?>
  <div id="section3">
    <div class="container">
      <div class="row">
        <div class="home-third col-xs-4 clearfix">
          <?php print render($page['three_col_left']); ?>
        </div>
        <div class="home-third col-xs-4 clearfix">
          <?php print render($page['three_col_middle']); ?>
        </div>
        <div class="home-third col-xs-4 clearfix">
          <?php print render($page['three_col_right']); ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>

  <?php if ($page['advertising_banner']) { ?>
    <div class="advertising-banner container">
      <div class="row">
        <div class="col-xs-12">
          <?php print render($page['advertising_banner']); ?>
        </div>
      </div>
    </div>
  <?php } ?>

  <?php global $user; ?>
  <?php if ($user->uid) { ?>
  <?php } else { ?>
    <div id="section4">
      <div class="container">
        <div class="row">
          <div class="col-xs-5 try_text">
            <?php print render($page['anon_user_message']); ?>
          </div>
          <div class="col-xs-7 try-action clearfix">
            <?php print render($page['anon_user_cta']); ?>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container -->
    </div><!-- /#section4 -->
  <?php } ?>


  <footer>
    <div id="footer"><div class="section container">

      
      <?php print render($page['footer']); ?>

    <!-- <script>
      /* new UISearch( document.getElementById( 'sb-search' ) ); */
    </script> -->

    </div></div> <!-- /.section, /#footer -->
  </footer>
  
  
<div class="newsletter-wrapper">
  <div class="newsletter-signup">
        <form class="form" role="form" action="/mirren-mailing-list" method="POST">
          <div class="form-group">
            <label class="sr-only" for="exampleInputEmail2">Email address</label>
            <input name="footer-email" type="email" id="exampleInputEmail2" placeholder="Receive Updates on Mirren Events &amp; News" />
          </div>
          <button type="submit" class="btn-mirren btn-mirren-default">Sign Up</button>
        </form>
      </div>


  </div>
  <div style="display:none;">
    <a class="hidden mirren-gate">Log In</a>
    <!-- Content Triggered by ".mirren-gate" class click -->
    <div id="mirren-gate-modal">
      <?php print '<h2>' . t('Join Mirren to view this content') . '</h2>'; ?>
      <?php print '<p>' . t('Mirren Membership provides exclusive access to a cutting-edge community and the best practices that are working to convert more business.') . '</p>'; ?>
      <?php print l('Log In', 'user', array('query' => drupal_get_destination(), 'attributes' => array('class' => array('btn-mirren', 'btn-jumbo')) ) ); ?>
      <?php print l('Learn About Membership', 'membership', array('attributes' => array('class' => array('btn-mirren')) ) ); ?>
      <button class="btn-mirren btn-jumbo colorbox-close">Cancel</button>
    </div>
  </div>
</div>


 <!-- /#page -->
