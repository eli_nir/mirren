<?php 
// Home Feature Content Type
// Teaser markup is used in Home Features Views Block
// Default display is copy of teaser for admin preview of text wrap, layout, etc.
?>

<?php if ($teaser) { ?>
  <article class="home-feature node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
      <img class="img-responsive" src="<?php print render(file_create_url($node->field_image_home_feature['und'][0]['uri'])); ?>" />
    <?php print render($title_suffix); ?>
    <div class="hf-content"> 
      <div class="hf-count"><?php print render($content['field_sort_order']); ?></div>
      <div class="hf-text">
        <h2><?php print $title; ?></h2>
        <?php print render($content['field_teaser_text_home_feature']); ?>
        <a class="btn-mirren" 
           title="<?php print ($node->field_link_home_feature['und'][0]['title']); ?>"
           href="<?php print ($node->field_link_home_feature['und'][0]['url']); ?>"
           <?php if (isset($node->field_link_home_feature['und'][0]['attributes']['target']) == '_blank') { ?>target="_blank"<?php } ?>>
           <?php print ($node->field_link_home_feature['und'][0]['title']); ?> ›
        </a>
      </div>
    </div>
  </article> 

<?php } else { ?>

<div class="row">
  <div class="col-xs-4">
    <article class="home-feature node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
      <?php print render($title_prefix); ?>
        <img class="img-responsive" src="<?php print render(file_create_url($node->field_image_home_feature['und'][0]['uri'])); ?>" />
      <?php print render($title_suffix); ?>
      <div class="hf-content"> 
        <div class="hf-count"><?php print render($content['field_sort_order']); ?></div>
        <div class="hf-text">
          <h2><?php print $title; ?></h2>
          <?php print render($content['field_teaser_text_home_feature']); ?>
          <a class="btn-mirren" 
             title="<?php print ($node->field_link_home_feature['und'][0]['title']); ?>"
             href="<?php print ($node->field_link_home_feature['und'][0]['url']); ?>"
             <?php if (isset($node->field_link_home_feature['und'][0]['attributes']['target']) == '_blank') { ?>target="_blank"<?php } ?>>
             <?php print ($node->field_link_home_feature['und'][0]['title']); ?> ›
          </a>
        </div>
      </div>
      <br/><br/>
    </article> 
  </div>
</div>

<?php } ?>