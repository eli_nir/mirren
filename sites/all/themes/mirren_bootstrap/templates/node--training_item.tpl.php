<?php
// Copy Theme Default Node Template
?>

<?php // IF teaser display
  if ($teaser) {
?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <?php print render($title_prefix); ?>
      <?php if (!$page) { ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php } ?>
    <?php print render($title_suffix); ?>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>

    <?php print render($content['links']); ?>

    <?php print render($content['comments']); ?>

  </article>

<?php } // ELSE default display (full node)
  else {
?>

  <?php
    /* Check for "video" type in training_item content type */
    if ( isset($node->field_type_training_category['und'][0]['value']) ) {
      $training_type = strtolower($node->field_type_training_category['und'][0]['value']);
    }
  ?>

    <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> <?php if (isset($training_type)) { print $training_type; } ?> clearfix"<?php print $attributes; ?>>

      <?php // Which heading size to use?
        // IF type is "video"
        if ( $training_type == 'video' ) {
      ?>
        <div class="header-intro">

            <div class="header-intro-content">

              <div class="header-intro-text">
                <div class="video-header-left">
                <div class="training-breadcrumb">
                	<?php print theme('breadcrumb', array('breadcrumb'=>drupal_get_breadcrumb()));?></div>
                  <?php print render($title_prefix); ?>
                    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
                  <?php print render($title_suffix); ?>
                  <?php print render($content['field_subtitle_training']); ?>
                </div>
                <div class="video-header-right">
                  <?php print render($content['field_duration']); ?>
                </div>
              </div><!-- /.header-intro-text -->

              <?php // MEMBER CONTENT
                if ($contentAccess) { 
              ?>
                <div class="feature-video-embed">
                  <iframe scrolling="no" width="864" height="486" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                </div>
              <?php
                } else {
              ?>
                <div class="mirren-gate-wrapper">
                  <a class="mirren-gate">Play <span></span></a>
                </div>
              <?php
                }
              ?> 

              <!-- PAGINATION TO-DO: LINK URLs  (added to PDF & ARTICLE types) -->
              <div class="video-navigation row">

                <?php print views_embed_view('training_item_pager', 'block_1'); ?>

                <div class="hidden" id="video_message">
                  <p>If you are experiencing any problems playing a video, often a simple browser refresh or restart will resolve the issue. You can
also try clearing the cache in your browser. If you continue to have any issues, please immediately contact <a href="?width=670&amp;height=340&amp;inline=true#member-services-modal" class="colorbox-inline">Member Services</a>.</p>
                  <a class="close" href="#">&times;</a>
                </div>
                <div style="display:none;">
                  <div id="member-services-modal">
                    <?php
                      $block = block_load('block', '20');
                      $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
                      print $output;
                    ?>
                    <button class="btn-mirren colorbox-close">Close</button>
                  </div>
                </div>
              </div>

            </div>


        </div><!-- /.header-intro -->
      <?php } // ELSE type is "article" or "pdf"
        else {
      ?>
        <?php
          // IF there is header content (#page.header-true)
           if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
        ?>
          <div class="header-intro">

            <?php
              // Header Submenu Region
              if ( isset($header_submenu) ) {
            ?>
              <div class="intro-nav">
                <?php print render($header_submenu); ?>
              </div>
            <?php
              }
            ?>
            <div class="header-intro-content clearfix">
              <div class="col-xs-7">
                <div class="header-intro-text">
                <div class="training-breadcrumb">
                	<?php print theme('breadcrumb', array('breadcrumb'=>drupal_get_breadcrumb()));?></div>
                  <?php print render($title_prefix); ?>
                    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
                  <?php print render($title_suffix); ?>
                  <?php print render($content['field_feature_intro']); ?>
                   <?php print views_embed_view('training_item_pager', 'block_1'); ?>

                </div><!-- /.header-intro-text -->
                <div class="header-intro-nav">
                          </div><!-- /.header-intro-nav -->
              </div>
              <div class="col-xs-5">

                <?php // 1) IF feature_image_link is used, wrap the image
                  if ( isset($node->field_feature_image_link['und'][0]['url']) ) { 
                ?>
                  <a 
                    href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
                    role="button"
                    <?php // IF the target attribute is set for "new window"
                      if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) {
                        print 'target="_blank"';
                      } 
                    ?>
                  >
                    <?php print render($content['field_banner_image']); ?>
                  </a>
                <?php // 2) ELSE IF the Video ID field is used
                  } elseif ( isset($node->field_video_id_training_item['und'][0]['value']) ) {
                ?>  
                  <a 
                    href="?width=960&amp;height=600&amp;inline=true#wistia-vid" 
                    role="button"
                    class="colorbox-inline"
                  >
                    <?php print render($content['field_banner_image']); ?>
                  </a>
                  <div style="display:none;">
                    <div id="wistia-vid">
                      <iframe scrolling="no" width="960" height="540" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id_training_item['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                      <button class="btn-mirren colorbox-close">Close</button>
                    </div>
                  </div>
                <?php 
                  } // 3) ELSE, not linked just an image
                  else {
                ?>
                  <?php print render($content['field_banner_image']); ?>
                <?php } ?>

              </div>
            </div><!-- /.header-intro-content -->
          </div><!-- /.header-intro -->
        <?php
          } // ELSE, no header content (#page or #page.default)
          else {
        ?>
          <div class="header-intro slim">  
            <?php 
              // Header Submenu Region
              if ( isset($header_submenu) ) { 
            ?>    
              <div class="intro-nav">
                <?php print render($header_submenu); ?>
              </div>
            <?php 
              }
            ?>

            <div class="header-intro-content slim clearfix">
              <div class="col-xs-7">
                <div class="header-intro-text">
                  <?php print render($title_prefix); ?>
                    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
                  <?php print render($title_suffix); ?>
                  <?php print render($content['field_feature_intro']); ?>
                </div><!-- /.header-intro-text -->
                <div class="header-intro-nav">
                  <?php print views_embed_view('training_item_pager', 'block_1'); ?>
                </div><!-- /.header-intro-nav -->
              </div>
            </div><!-- /.header-intro-content -->
          </div><!-- /.header-intro.slim -->
        <?php
          }
        ?>
      <?php
        }
      ?>


        <div class="row">

          <?php
            if ( isset($sidebar_right_3) ) {
              print '<div class="col-xs-9">';
            }
            elseif ( isset($sidebar_left_3) ) {
              print '<div class="col-xs-9 col-xs-push-3">';
            }
            elseif ( isset($sidebar_right_4) ) {
              print '<div class="col-xs-8">';
            }
            else {
              print '<div class="col-xs-12">';
            }
          ?>

          <?php // MEMBER CONTENT
            if ($contentAccess) { 
          ?>
            
            <?php
            //  MEMBER - ALL TYPES - Series Overview
            //  =====================================
            ?>

            <div class="group-series-overview group-body-group">
              <?php 
                $term = taxonomy_term_load($node->field_series_training['und'][0]['tid']);
                //dpm($term);
                
                if ( !empty($term->description) ) {
                print '<h2>' . t('Series Overview') . '</h2>';
                print '<div id="field-series-overview">';
                /* Removed Read More/Read Less for videos. Now all can see description and series overview
				print ($term->field_teaser['und'][0]['value']);
                print '<a class="read-more-link logged-in collapsed" data-toggle="collapse" data-target="#field-series-overview">Read More</a>';
                print '<div id="field-series-overview" class="collapse">';*/
                print $term->description;
                print '</div>';
                /* print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-series-overview">Read Less</a>'; */
                }
              ?>
            </div>

            <?php
              if ( $training_type == 'video' ) {
            ?>
              <?php
              //  MEMBER - VIDEO TYPE - Video Overview
              //  =====================================
              ?>
              <div class="group-video-overview group-body-group">
                <?php 
                  print '<h2>' . t('Video Overview') . '</h2>';
                  print '<div id="field-video-overview">';
                  
                  /* Removed Read More/Read Less for videos. Now all can see description and series overview
				  print ($node->field_non_member_teaser_video['und'][0]['value']);
                  print '<a class="read-more-link logged-in collapsed" data-toggle="collapse" data-target="#field-video-overview">Read More</a>';
                  print '<div id="field-video-overview" class="collapse">';*/
                  print ($node->body['und'][0]['value']);
                  print '</div>';
                  /* print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-video-overview">Read Less</a>'; */
                ?>
              </div>
            <?php
              }
            ?> 

            <?php
              if ( $training_type == 'pdf' ) {
            ?>
              <?php
              //  MEMBER - PDF Type - Document Overview
              //  ======================================
              ?>
              <div class="group-document-overview group-body-group">
                <?php 
                  print '<h2>' . t('Document Overview') . '</h2>';
                  print ($node->field_non_member_teaser_pdf['und'][0]['value']);
                  print '<a class="read-more-link logged-in collapsed" data-toggle="collapse" data-target="#field-document-overview">Read More</a>';
                  print '<div id="field-document-overview" class="collapse">';
                  print ($node->field_document_overview['und'][0]['value']);
                  print '</div>';
                  print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-document-overview">Read Less</a>';
                ?>
              </div>
            <?php // closes IF 'PDF'
              }
            ?>

            <?php
              if ( $training_type == 'article' ) {
            ?>
              <?php
              //  MEMBER - ARTICLE Type - Read Article
              //  ======================================
              ?>
              <div class="group-read-article group-body-group">
                <?php 
                  print '<h2>' . t('Read the Article') . '</h2>';
                  print ($node->field_introduction_article['und'][0]['value']);
                  print '<a class="read-more-link logged-in collapsed" data-toggle="collapse" data-target="#field-document-overview">Read More</a>';
                  print '<div id="field-document-overview" class="collapse">';
                  print ($node->field_read_the_article['und'][0]['value']);
                  print '</div>';
                  print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-document-overview">Read Less</a>';
                ?>
              </div>
            <?php
              }
            ?> 

            <?php
            //  MEMBER - ALL TYPES - Download File Links
            //  =========================================
            ?>
            <div class="field-name-field-pdf-download">
              <?php foreach ($field_pdf_download as $tvalue) {
                //dpm($tvalue);
                $pdf_filename = render($tvalue['filename']);
                $pdf_uri      = render($tvalue['uri']);
                if ( !empty($tvalue['description']) ) {
                  $pdf_description = render($tvalue['description']);
                }
                else {
                  $pdf_description = 'Download the PDF ›'; 
                }
              ?>
                <span class="file">
                  <a 
                    href="<?php print render(file_create_url($pdf_uri)); ?>"
                    title="Download <?php print $pdf_filename; ?>"
                  >
                    <?php print $pdf_description; ?>
                  </a>
                  
                </span>
              <?php // closes foreach
                }
              ?>
            </div>

          <?php
            //hide the flagging link from the body content
            hide($content['flag_team_library']);
            hide($content['flag_team_member_library']);
            hide($content['field_pdf_download']);
            print render($content);
          ?>

          <?php
            } // NON-MEMBER DISPLAY
            else {
          ?>
            <?php
            //  ANONYMOUS - ALL TYPES - Series Overview
            //  =====================================
            ?>
            <div class="group-series-overview group-body-group">
              <?php 
                $term = taxonomy_term_load($node->field_series_training['und'][0]['tid']);
                //dpm($term);
                
                if ( !empty($term->description) ) {
                print '<h2>' . t('Series Overview') . '</h2>';
                print '<div id="field-series-overview">';
                /* Removed Read More/Read Less for videos. Now all can see description and series overview
				print ($term->field_teaser['und'][0]['value']);
                print '<a class="read-more-link logged-in collapsed" data-toggle="collapse" data-target="#field-series-overview">Read More</a>';
                print '<div id="field-series-overview" class="collapse">';*/
                print $term->description;
                print '</div>';
                /* print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-series-overview">Read Less</a>'; */
                }
              ?>
            </div>

            <?php
              if ( $training_type == 'video' ) {
            ?>
              <?php
              //  ANONYMOUS - VIDEO TYPE - Video Overview
              //  =========================================
              ?>
              <div class="group-video-overview group-body-group">
                <?php 
                  print '<h2>' . t('Video Overview') . '</h2>';
                  print '<div id="field-video-overview">';
                  /* Removed Read More/Read Less for videos. Now all can see description and series overview   
				  print ($node->field_non_member_teaser_video['und'][0]['value']);
                  print '<a class="read-more-link logged-in collapsed" data-toggle="collapse" data-target="#field-video-overview">Read More</a>';
                  print '<div id="field-video-overview" class="collapse">';*/
                  print ($node->body['und'][0]['value']);
                  print '</div>';
                  /* print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-video-overview">Read Less</a>'; */
                ?>

              </div>
            <?php
              }
            ?>
          
            <?php
              if ( $training_type == 'pdf' ) {
            ?>
              <?php
              //  ANONYMOUS - PDF TYPE - Document Overview
              //  =========================================
              ?>
              <div class="group-document-overview group-body-group">
                <?php 
                  print '<h2>' . t('Document Overview') . '</h2>';
                  print ($node->field_non_member_teaser_pdf['und'][0]['value']);
                  print '<a class="mirren-gate read-more-link collapsed">Read More</a>';
                ?>
              </div>
            <?php
              }
            ?>

            <?php
              if ( $training_type == 'article' ) {
            ?>
              <?php
              //  ANONYMOUS - ARTICLE Type - Read Article
              //  ======================================
              ?>
              <div class="group-read-article group-body-group">
                <?php 
                  print '<h2>' . t('Read the Article') . '</h2>';
                  print ($node->field_introduction_article['und'][0]['value']);
                  print '<a class="mirren-gate read-more-link collapsed">Read More</a>';
                ?>
              </div>
            <?php
              }
            ?>

            <?php
            //  ANONYMOUS - ALL TYPES - Gray "Buttons" represent files
            //  =======================================================
            ?>
            <div class="field-name-field-pdf-download">
              <?php foreach ($field_pdf_download as $tvalue) {
                //dpm($tvalue);
                if ( !empty($tvalue['description']) ) {
                  $pdf_description = render($tvalue['description']);
                }
                else {
                  $pdf_description = 'Download the PDF ›'; 
                }
              ?>
                <span class="file">
                  <span class="non-member">
                    <?php print $pdf_description; ?>
                  </span>
                </span>
              <?php // closes foreach
                }
              ?>
            </div>


          <?php // closes else non-member
            }
          ?>

          </div>

          <?php if ( isset($sidebar_right_3) ) { ?>
            <div class="col-xs-3 sidebar-right-3 sidebar">
              <?php
              //this is for the flaggin block. This is part of the node content
              if ( ($contentAccess) && isset($content['flag_team_library']) ) {
                print '<div class="block block-block" id="library-flag-block">';
                print "<h2>" . t('Library') . "</h2>";
                print "<div class='content'><ul>";
                print "<li>" . render($content['flag_team_member_library']) . "</li>";
                print "<li>" . l(t('View My Library ›'), 'my-library') . "</li>";
                print "<li>" . render($content['flag_team_library']) . "</li>";
                //print "<li>" . l(t('View Team Library'), 'team-library') . "</li>";
                print "</ul></div>";
                print "</div>";
              }
              ?>
              <?php print render($sidebar_right_3); ?>
            </div>
          <?php }
            elseif ( isset($sidebar_left_3) ) {
          ?>
            <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
              <?php print render($sidebar_left_3); ?>
            </div>
          <?php }
            elseif ( isset($sidebar_right_4) ) {
          ?>
            <div class="col-xs-4 sidebar-right-4 sidebar">
              <?php print render($sidebar_right_4); ?>
            </div>
          <?php }
            else { ?>
            <!-- 1-col -->
          <?php
            }
          ?>

        </div>

      </article>




<?php } ?>