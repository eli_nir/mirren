<?php

/**
 * @file
 * Default theme implementation to format the simplenews newsletter body.
 *
 * Copy this file in your theme directory to create a custom themed body.
 * Rename it to override it. Available templates:
 *   simplenews-newsletter-body--[tid].tpl.php
 *   simplenews-newsletter-body--[view mode].tpl.php
 *   simplenews-newsletter-body--[tid]--[view mode].tpl.php
 * See README.txt for more details.
 *
 * Available variables:
 * - $build: Array as expected by render()
 * - $build['#node']: The $node object
 * - $title: Node title
 * - $language: Language code
 * - $view_mode: Active view mode
 * - $simplenews_theme: Contains the path to the configured mail theme.
 * - $simplenews_subscriber: The subscriber for which the newsletter is built.
 *   Note that depending on the used caching strategy, the generated body might
 *   be used for multiple subscribers. If you created personalized newsletters
 *   and can't use tokens for that, make sure to disable caching or write a
 *   custom caching strategy implemention.
 *
 * @see template_preprocess_simplenews_newsletter_body()
 */
?>

<p><img src="http://mirren.com/sites/all/themes/mirren_bootstrap/assets/images/mirren-leads-alerts-banner.gif"></p>
 
<p>Hi [simplenews-subscriber:user:field-first-name],</p>
 
<p>Here's the latest leads we've uncovered today...</p>

<div style="margin-left:20px; display:block; position:relative;">
<?php $view = views_get_view('lead_view');
      print $view->render('leads_daily');
?></div>

We've got the full scoop right here:<br><a href="http://www.BizDevLeads.com/Today">http://www.BizDevLeads.com/Today</a>

<p>As we get any more information on these opportunities, we'll keep you updated. </p>

Best,<br> 
Chanel 
 
Chanel McFadzean <br>
Daily Lead Research Manager <br>
Mirren Business Development <br>
 
<p>To manage your Lead Alert settings, click <a href="[simplenews-subscriber:manage-url]">here.</a> </p>
