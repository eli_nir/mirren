<?php
// Lead Content Type Node Template
// Teaser is custom, default is copy of node.tpl
?>

<?php if ($teaser) { ?>

<article class="lead-teaser node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <div class="row">
    <div class="col-xs-11">
      <?php 
        if ( isset($node->field_category_lead['und'][0]['value']) ) {
          $lead_category = ($node->field_category_lead['und'][0]['value']);
          $lead_category = str_replace(" ", "-", $lead_category);
          $lead_category = strtolower($lead_category);
        }
      ?>

      <?php if ($contentAccess) : ?>
          <a class="lead-category-icon-wrap" href="<?php print $node_url; ?>">
          <img src="<?php print image_style_url('lead_teaser', $node->field_banner_image['und'][0]['uri']); ?>" alt="<?php print $node->field_banner_image['und'][0]['alt']; ?>" />
        </a>
      <?php else: ?>
        <a class="lead-category-icon-wrap" href="<?php print $node_url; ?>">
          <span class="lead-category-icon <?php if ( isset($lead_category) ) { print $lead_category; } ?>"></span>
        </a>
      <?php endif; ?> 

      <div class="lead-details">
           
      
      
      
      
        <?php print render($title_prefix); ?>  
        <h2<?php print $title_attributes; ?>>
          <a href="<?php print $node_url; ?>">
            <?php // IF logged in, show default title
              if ($contentAccess) { 
                print $title;
              } 
              else {
                print $node->field_title_non_member['und'][0]['value'];
              }    
            ?> 
          </a>
        </h2>
        <?php print render($title_suffix); ?>
        <?php if (isset($node->field_feature_intro['und'][0]['value']) ) {
            print '<p>' . ($node->field_feature_intro['und'][0]['value']) . '</p>'; 
          } 
        ?>
      </div>
    </div>
    
    
    
    
    <div class="col-xs-1">
      <div class="lead-date-col">
        <span class="lead-month-day"><?php print format_date($node->field_date['und'][0]['value'], "custom", "M d"); ?></span>
        <span class="lead-time"><?php print format_date($node->field_date['und'][0]['value'], "custom", "h:i A"); ?></span>
      </div>
    </div>
  </div>

  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <?php 
      // IF there is header content (#page.header-true)
       if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
    ?> 
    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>
      <div class="header-intro-content clearfix">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>>
                <?php // IF logged in, show default title
                  if ($contentAccess) { 
                    print $title;
                  } 
                  else {
                    print $node->field_title_non_member['und'][0]['value'];
                  }    
                ?>
              </h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-xs-5">

        <?php if ($contentAccess) : ?>
          <?php // 1) IF feature_image_link is used, wrap the image
            if ( isset($node->field_feature_image_link['und'][0]['url']) ) { 
          ?>
            <a 
              href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
              role="button"
              <?php // IF the target attribute is set for "new window"
                if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) {
                  print 'target="_blank"';
                } 
              ?>
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
          <?php // 2) ELSE IF the Video ID field is used
            } elseif ( isset($node->field_video_id['und'][0]['value']) ) {
          ?>  
            <a 
              href="?width=960&amp;height=600&amp;inline=true#wistia-vid" 
              role="button"
              class="colorbox-inline"
            >
              <?php print render($content['field_banner_image']); ?>
            </a>
            <div style="display:none;">
              <div id="wistia-vid">
                <iframe scrolling="no" width="960" height="540" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                <button class="btn-mirren colorbox-close">Close</button>
              </div>
            </div>
          <?php 
            } // 3) ELSE, not linked just an image
            else {
          ?>
            <?php print render($content['field_banner_image']); ?>
          <?php } ?>
        <?php endif; ?>

        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->
    <?php 
      } // ELSE, no header content (#page or #page.default) 
      else { 
    ?>
      <div class="header-intro slim">  
        <?php 
          // Header Submenu Region
          if ( isset($header_submenu) ) { 
        ?>    
          <div class="intro-nav">
            <?php print render($header_submenu); ?>
          </div>
        <?php 
          }
        ?>

        <div class="header-intro-content slim clearfix">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div><!-- /.header-intro-content -->
      </div><!-- /.header-intro.slim -->
    <?php 
      } 
    ?>

    <div class="row">

      <?php 
        if ( isset($sidebar_right_3) ) { 
          print '<div class="col-xs-9">';
        } 
        elseif ( isset($sidebar_left_3) ) {
          print '<div class="col-xs-9 col-xs-push-3">';
        }
        elseif ( isset($sidebar_right_4) ) {
          print '<div class="col-xs-8">';
        }
        else {
          print '<div class="col-xs-12">';
        }
      ?>

      <?php
        //hide the flagging link from the body content
        hide($content['flag_team_library']);
        hide($content['flag_team_member_library']); 
      ?>

      <?php // MEMBER CONTENT
        if ($contentAccess) { 
      ?>

        <!-- THE SCOOP -->
        <?php print '<h2>' . t('The Scoop') . '</h2>'; ?>
        <div class="field-name-body">
          <?php print $node->body['und'][0]['safe_value']; ?>
        </div>

        <?php if ( !empty($node->field_file_download_lead) ) { ?>
          <div class="field-name-field-file-download-lead">
            <?php foreach ($field_file_download_lead as $tvalue) {
              //dpm($tvalue);
              $lead_filename = render($tvalue['filename']);
              $lead_uri      = render($tvalue['uri']);
              if ( !empty($tvalue['description']) ) {
                $lead_description = render($tvalue['description']);
              }
              else {
                $lead_display_type = ($node->field_category_lead['und'][0]['value']);
                if ( $lead_display_type == 'RFP' ) {
                  $lead_description = 'Download RFP';
                }
                elseif ( $lead_display_type == 'RFQ' ) {
                  $lead_description = 'Download RFQ'; 
                }
                else {
                 $lead_description = 'Download File'; 
                }
              }
            ?>
              <span class="file">
                <a 
                  href="<?php print render(file_create_url($lead_uri)); ?>"
                  title="Download <?php print $lead_filename; ?>"
                >
                  <?php print $lead_description; ?>
                </a>
                
              </span>
            <?php // closes foreach
              }
            ?>
          </div>          
        <?php } ?>

        <!-- THE COMPANY -->
        <?php print '<h2>' . t('The Company') . '</h2>'; ?>
        <div class="field-name-field-company">
          <?php print $node->field_company['und'][0]['value']; ?>
        </div>
        <div class="field-name-field-company-summary">
          <?php print ($node->field_company_summary['und'][0]['safe_value']); ?>
        </div>

        <!-- CONTACT INFO -->  
        <?php
          //hide the flagging link from the body content
          hide($content['flag_team_library']);
          hide($content['flag_team_member_library']); 
          hide($content['field_file_download_lead']); 
          print render($content); 
        ?>
        
      <?php // NON-MEMBER
        } else {
      ?>
        <?php print '<h2>' . t('The Scoop') . '</h2>'; ?>
        
        
        <p><?php print l("Log In","user", array('query' => drupal_get_destination()));?> to see the full scoop on this lead.</p>
        <!-- DOWNLOAD BUTTON -->
        <div class="field-name-field-file-download-lead">
          <span class="file">
            <span class="non-member">
              <?php 
                if ( !empty( $node->field_file_download_lead['und'][0]['description'] ) ) {
                  print ( $node->field_file_download_lead['und'][0]['description'] );
                }
                else {
                  $lead_display_type = ($node->field_category_lead['und'][0]['value']);
                  if ( $lead_display_type == 'RFP' ) {
                    $lead_display_type = 'RFP';
                  }
                  elseif ( $lead_display_type == 'RFQ' ) {
                    $lead_display_type = 'RFQ'; 
                  }
                  else {
                   $lead_display_type = 'File'; 
                  }
                  print 'Download' . ' ' . $lead_display_type ;
                }
              ?>
            </span>
          </span>
        </div>
        
          <h2>The Company</h2>
          <p><?php print l("Log In","user", array('query' => drupal_get_destination()));?> to learn about this company.</p>
          <h2>Contact Info</h2>
          <p><?php print l("Log In","user", array('query' => drupal_get_destination()));?> to view the contact info.</p>
        
      <?php 
        } 
      ?>
        
      </div>

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-3 sidebar-right-3 sidebar">
          <?php
          //this is for the flaggin block. This is part of the node content
          if ( ($contentAccess) && isset($content['flag_team_library']) ) { 
            print '<div class="block block-block" id="library-flag-block">';
            print "<h2>" . t('Library') . "</h2>";
            print "<div class='content'><ul>";
            print "<li>" . render($content['flag_team_member_library']) . "</li>";
            print "<li>" . l(t('View My Library ›'), 'my-library') . "</li>";
            print "<li>" . render($content['flag_team_library']) . "</li>";
            //print "<li>" . l(t('View Team Library'), 'team-library') . "</li>";
            print "</ul></div>";
            print "</div>";
          }
          ?>
          <?php print render($sidebar_right_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>
  
  
<!-- Script to temporarily launch all links inside of a DIV into a new window. -->  
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
     (function ($) {
		
		$('.field-name-field-related-info a').attr('target', '_blank')
		
		
		
				})(jQuery);
</script>  
  
  

<?php } ?>