<?php
// Theme Default Node Template
?>

<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
    
    <?php 
      // IF there is header content (#page.header-true)
       if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
    ?> 

    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>

      <div class="header-intro-content clearfix">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php print render($title_prefix); ?>
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
            <?php print render($content['field_feature_intro']); ?>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-xs-5">

          <?php // IF there is a link URL wrap the image with an anchor tag, 1 of 2 ways:
            if ( isset($node->field_feature_image_link['und'][0]['url']) ) { ?>
              <?php // 1) IF colorbox is TRUE, build anchor tag with class="colorbox-video" and ignore "target"
                if ( !empty($node->field_enable_colorbox['und'][0]['value']) ) { ?>
                <a 
                  href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
                  role="button"
                  class="colorbox-video"
                >
              <?php } // 2) ELSE, build anchor tag and check "target" 
                else { ?>
                <a 
                  href="<?php print ($node->field_feature_image_link['und'][0]['url']); ?>" 
                  role="button"
                  <?php // IF the target attribute is set for "new window"
                    if ( isset($node->field_feature_image_link['und'][0]['attributes']['target']) ) { ?>
                    target="_blank"
                  <?php } ?>
                >
              <?php } ?>
                <?php print render($content['field_banner_image']); ?>
              </a>
          <?php } // ELSE, no link, just image
            else { ?>
            <?php print render($content['field_banner_image']); ?>
          <?php } ?>

        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->

    <?php 
      } // ELSE, no header content (#page or #page.default) 
      else { 
    ?>
      <div class="header-intro slim">  
        <?php 
          // Header Submenu Region
          if ( isset($header_submenu) ) { 
        ?>    
          <div class="intro-nav">
            <?php print render($header_submenu); ?>
          </div>
        <?php 
          }
        ?>

        <div class="header-intro-content slim clearfix">
          <div class="col-xs-7">
            <div class="header-intro-text">
              <?php print render($title_prefix); ?>
                <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
              <?php print render($title_suffix); ?>
              <?php print render($content['field_feature_intro']); ?>
            </div><!-- /.header-intro-text -->
          </div>
        </div><!-- /.header-intro-content -->
      </div><!-- /.header-intro.slim -->
    <?php 
      } 
    ?>


    <?php 
      // Full Width Banner Space
      if ( isset($full_width_banner) ) { 
    ?>    
      <div class="row">
        <div id="full_width_banner" class="col-xs-12">
          <?php print render($full_width_banner); ?>
        </div>
      </div>
    <?php 
      }
    ?>

    <div class="row">

      <?php 
        if ( isset($sidebar_right_3) ) { 
          print '<div class="col-xs-9 main-target">';
        } 
        elseif ( isset($sidebar_left_3) ) {
          print '<div class="col-xs-9 col-xs-push-3 main-target">';
        }
        elseif ( isset($sidebar_right_4) ) {
          print '<div class="col-xs-8 main-target">';
        }
        else {
          print '<div class="col-xs-12 main-target">';
        }
      ?>
        <div class="field field-name-body field-type-text-with-summary field-label-hidden">
         <p><img alt="" src="/sites/default/files/images/404-briefcase_0.png" style="width: 303px; height: 266px; float: right; margin: 5px;" />Sorry, the page you requested may have been moved or deleted.</p>

<p>Let's try one of the following remedies to get you back on track:</p>

<p>Revisit the <a href="#">previous page</a>.<br />
Go to&nbsp;<a href="https://www.mirren.com">Home</a>, <a href="http://mirren.com/membership">Membership</a>, <a href="https://www.mirren.com/leads">Daily Leads</a>, <a href="https://www.mirren.com/On-Demand-Learning">On-Demand Learning</a>, or <a href="http://www.mirren.com/live-events">Live Events</a></p>

<p>Or make use of the search:</p>

<?php
          //invoke search block
          $block = module_invoke('search', 'block_view', 'form');
          print "<div id='search-block'>";
          print render($block['content']);
          print "</div>";
          ?>

<p>If you need assistance, please contact <a href="mailto:member.services@mirren.com">Members Services</a>.</p>        </div>
        <?php if ( isset($node_body_content) ) { ?>
            <?php print render($node_body_content); ?>
        <?php } ?>

        <?php // Embed Views Pages
          if (($node->nid) == '25732') { 
            print views_embed_view('lead_view',"page"); 
          }
          elseif (($node->nid) == '25738') { 
            print views_embed_view('account_win',"page"); 
          }
          elseif (($node->nid) == '25764') { 
            //adding view page for the forum - /admin/structure/views/view/forum_landing/edit
            print views_embed_view('forum_landing',"page"); 
          }
          elseif (($node->nid) == '25840') { 
            //adding view page for /service-providers-all - /admin/structure/views/view/all_service_search/edit/page_1
            print views_embed_view('all_service_search',"page_1"); 
          }
          elseif (($node->nid) == '25841') { 
            //adding view page for /service-providers-all- /admin/structure/views/view/all_service_search/edit/page
            print views_embed_view('all_service_search',"page"); 
          }
          else {

          }   
        ?>

        <?php if ( isset($content['field_accordion_content']) ) { ?>
          <!-- FIELD COLLECTION -->    
          <div class="panel-group" id="fc-accordion">
            <?php $fc_count = 1; ?>
            <?php 
              foreach ($content['field_accordion_content']['#items'] as $entity_uri) : $field_collection_item = entity_load('field_collection_item', $entity_uri);
              foreach ($field_collection_item as $field_collection_object) : 

              $fc_title = $field_collection_object->field_title_accordion['und'][0]['value']; 
              $fc_body = $field_collection_object->field_body_area['und'][0]['safe_value']; 
            ?>

            <div class="panel ">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="collapsed" data-toggle="collapse" data-parent="#fc-accordion" href="#fc-<?php print $fc_count; ?>">
                    <strong class="numeral"><?php print $fc_count; ?></strong> <?php print $fc_title; ?> <i class=""></i>
                  </a>
                </h4>
              </div>
              <div id="fc-<?php print $fc_count; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                  <?php print $fc_body; ?>
                </div>
              </div>
            </div>

              
              <?php $fc_count++; ?>
            <?php endforeach; endforeach; ?>
          </div>
        <?php } ?>

      </div>

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-3 sidebar-right-3 sidebar">
          <?php print render($sidebar_right_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>

<?php } ?>