<?php

/**
 * @file
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $created: Formatted date and time for when the comment was created.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->created variable.
 * - $changed: Formatted date and time for when the comment was last changed.
 *   Preprocess functions can reformat it by calling format_date() with the
 *   desired parameters on the $comment->changed variable.
 * - $new: New comment marker.
 * - $permalink: Comment permalink.
 * - $submitted: Submission information created from $author and $created during
 *   template_preprocess_comment().
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $title: Linked title.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - comment: The current template type, i.e., "theming hook".
 *   - comment-by-anonymous: Comment by an unregistered user.
 *   - comment-by-node-author: Comment by the author of the parent node.
 *   - comment-preview: When previewing a new or edited comment.
 *   The following applies only to viewers who are registered users:
 *   - comment-unpublished: An unpublished comment visible only to administrators.
 *   - comment-by-viewer: Comment by the user currently viewing the page.
 *   - comment-new: New comment since last the visit.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * These two variables are provided for context:
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_comment()
 * @see template_process()
 * @see theme_comment()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php
  //this is to check if the user is logged in or not. For each field there a uid value.
  //check to see if varable is even set
  if($comment->uid){
    //if user is not anonymous

      //user is not anonymous
      //load author user info
      //https://api.drupal.org/api/drupal/modules!user!user.module/function/user_load/7
      $author_user = user_load($comment->uid);
      //make sure there is an image
      if(isset($author_user->picture->uri)){
        //get user pic uri
        $author_picture_uri = $author_user->picture->uri;
        //theme the user pic with an image style
        $author_picture = theme(
          'image_style',
          array(
          'path' => $author_picture_uri,
          'style_name' => 'image_56_56'
          )
        );

        //print user pic in link
        print "<div id='commenter-picture'>";
        print l($author_picture, 'user/' . $comment->uid, array('html' => TRUE));
        print "</div>";
      }else{
        //choose an random image
        print "<div id='commenter-picture'>";
        print l("<img src='/sites/all/themes/mirren_bootstrap/assets/images/avatar_selection/member_default_0.jpg' width='56' height='56' />", 'user/' . $content['field_your_company']['#object']->uid, array('html' => TRUE));
        print "</div>";
      }

  }else{
      //user is anonymous
      //generate random number
 
/*
$numbers = range(0, 19);
      shuffle($numbers);
*/

      
      
      

$max = 44;
$numbers = range(0, $max);

// Retrieve already used numbers, if any.
$already_used = unserialize(variable_get('mirren_bootstrap_numbers_already_used', array()));

// Subtract already used numbers from $numbers.
$numbers = array_values(array_diff($numbers, $already_used));

shuffle($numbers);

// add current number to $already_used.
$already_used[] = $number = array_pop($numbers);

// Take care that the stored array of numbers is emptied if contains all the numbers.
$new_already_used = (count($already_used) <= $max) ? $already_used : array();
variable_set('mirren_bootstrap_numbers_already_used', serialize($new_already_used));
      
     
            //--------------------
      
	  print "<div id='commenter-picture'>";
      print "<img src='/sites/all/themes/mirren_bootstrap/assets/images/avatar_selection/member_default_" . $number . ".jpg' width='56' height='56' />";
      print "</div>";

    }
  ?>


  <?php

  print "<h4>" . $author . "</h4>";
  print "<div id='comment-date'>";
  print($created);
  print "</div>";



  // We hide the comments and links now so that we can render them later.
  hide($content['links']);
  hide($content['field_your_company']);
  hide($content['field_your_location']);
  print render($content);
  //drupal_set_message("<pre>" . print_r($content, TRUE) . "</pre>");
  //strip html from comment body to use for Tweet This
  $comment_body = strip_tags(render($content['comment_body']));
  //get url
  $current_url = 'http%3A%2F%2F' .$_SERVER['HTTP_HOST'] . str_replace("/", "%2F", $_SERVER['REQUEST_URI']);
  ?>
  <div class="comment-footer-link">
    <?php print render($content['links']); ?>
    <div class="custom-tweet-button">
      <a onclick="showPopup(this.href);return(false);" href="https://twitter.com/share?url=<?php print $current_url;?>&amp;text=<?php print $comment_body;?>">Tweet this</a>
    </div>
  </div>
</div>
