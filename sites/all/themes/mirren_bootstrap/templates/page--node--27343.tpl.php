<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?><!-- jQuery library (served from Google) -->

<html>
<head>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
<script src="/sites/all/themes/mirren_bootstrap/assets/js/jquery.bxslider.min.js" type="text/javascript"></script>
<link href="/sites/all/libraries/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css">



    <title></title>
</head>

<body>
    <div id="page" class="<?php if ( isset($header_status) ) { print $header_status; } ?>">
        <div id="secondary_menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php print render($page['header']); ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="main_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php print render($page['navigation']); ?><!-- Collapsed Nav -->

                        <nav class="navbar navbar-default collapsed-nav" role="navigation">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    
                                    <span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
                                    
                                    
                                    
                                   </button> 
                                    
                                    <a class="navbar-brand" href="http://www.mirren.com/">Mirren</a>
                                </div><!-- Collect the nav links, forms, and other content for toggling -->

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a href="/leads">Daily Leads</a></li>

                                        <li><a href="/training-online">On-Demand Learning</a></li>

                                        <li><a href="/training-in-person">In Person Training</a></li>

                                        <li><a href="/live-events">Live Events</a></li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">More</a>

                                            <ul class="dropdown-menu">
                                                <li><a href="/membership">Membership</a></li>

                                                <li><a href="/search-consultants">Search Consultants</a></li>

                                                <li><a href="/service-providers">Service Providers</a></li>

                                                <li><a href="/discussion-forums">Forums</a></li>

                                                <li><a href="/about-us">About / Contact</a></li>

                                                <li><a href="/blog">Blog</a></li>

                                                <li><a href="/subscribe">Mailing List</a></li>

                                                <li>
                                                    <!-- START OF LOGIN/LOGOUT BLOCK -->
                                                    <?php global $user;
                                                                                    if ($user->uid != 0) {
                                                                                        // code for the logout button ?>

                                                    <ul class="login-block">
                                                        <li class="first leaf"><a href="/user" title="">My Profile</a></li>

                                                        <li class="leaf"><a href="/my-agency-hub" title="">My Agency Hub</a></li>

                                                        <li class="leaf"><a href="/my-library" title="">My Library</a></li>

                                                        <li class="last leaf"><a href="/user/logout" title="">Logout</a></li>
                                                    </ul>
                                                    
                                                    <?php
                                                                                        }
                                                                                    else {
                                                                                    // code for the login button
                                                                                    echo "<a class=\"user-link\" href=\"?q=user\">Login</a>";
                                                                                        }
                                                                                ?>
                                                                                
                                                                                <!-- END OF LOGIN/LOGOUT BLOCK -->
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
            
        </div>
    

        <?php print render($page['page_header_block']); ?>

        <div id="main" class="clearfix container">
            <div class="row">
                <div class="col-md-12">
                    <?php print $messages; ?><?php if ($tabs) { ?>

                    <div class="tabs">
                        <?php print render($tabs); ?>
                    </div><?php } ?><?php print render($page['help']); ?><?php if ($action_links) { ?>

                    <ul class="action-links">
                        <?php print render($action_links); ?>
                    </ul><?php } ?><?php print render($page['content']); ?><?php print $feed_icons; ?>
                </div>
            </div><!-- /.row -->
        </div><!-- /#main -->
        <!-- SECTION 2 -->
        <!-- SECTION 3 -->
        
        <div id="featured-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="content">
                           <!--  <span>RIGHT NOW IN NEW YORK</span>  -->
                            <!-- <p><a href="http://mirren.com/live-broadcast-help" target="_blank">Webcast Help [?]</a></p> -->
                            <?php if ($page['featured']) { ?>

                            <div id="featured">
                                <?php print render($page['featured']); ?>
                            </div><?php } ?>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="content">
                            <div class="twitter_wrapper">
                                <img src="sites/all/themes/mirren_bootstrap/assets/images/twitter_logo_block.png"><span class="mirrenlive">#mirrenlive</span> 
                                
                                
                                <?php /* if (function_exists('twitter_pull_render')) {  */print twitter_pull_render('#mirrenlive', '', 20); ?>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        

<div id="event-updates">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <h3>More Event Coverage</h3>
            
            <div class="all-galleries">
                           
                           
                           
                                            <div class="gallery">
					
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-100.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-105.jpg">
					<div class="gallery-title">Day 2 Highlights</div></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-101.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-102.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-103.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-104.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-106.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-107.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-108.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-109.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-110.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-111.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-112.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-113.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-114.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-115.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-116.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-117.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-118.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-119.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-120.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-121.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-122.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-123.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-124.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-125.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-126.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-127.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-128.jpg"></a>
					<a class="colorbox" rel="nine" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-2/day-2-129.jpg"></a>
														
					 </div>
        
                           
                           
                                                                           
                        
                         <div class="gallery">
					
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-100.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-105.jpg">
					<div class="gallery-title">Day 1 Highlights</div></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-101.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-102.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-103.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-104.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-106.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-107.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-108.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-109.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-110.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-111.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-112.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-113.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-114.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-115.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-116.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-117.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-118.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-119.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-120.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-121.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-122.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-123.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-124.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-125.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-126.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-127.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-128.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-129.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-130.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-131.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-132.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-133.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-134.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-135.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-136.jpg"></a>
					<a class="colorbox" rel="eight" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/day-1-137.jpg"></a>

					
					 </div>

                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     <div class="gallery">
					
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-105.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-105.jpg">
					<div class="gallery-title">Pre-Conference Pub Night</div></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-101.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-102.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-103.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-104.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-106.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-107.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-108.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-101.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-110.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-111.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-112.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-113.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-114.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-115.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-116.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-117.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-118.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-119.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-120.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-121.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-122.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-123.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-124.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-125.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-126.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-127.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-128.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-129.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-130.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-131.jpg"></a>
					<a class="colorbox" rel="seven" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/pub-night/pub-night-132.jpg"></a>

					
					 </div>

                        
                        
                        
                        
                        
               <div class="gallery">
				   
					<a href="//fast.wistia.net/embed/iframe/9t067enw25?width=600&height=425&iframe=true" class="gallery colorbox-load"><img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-1/lastyear-thumb.jpg" rel="group1">
					
					<span class="gallery-title">Highlights From Last Year</span></a>
										 </div>
               
               
               
               
               
               <div class="gallery">
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-102.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-102.jpg" class="gallery-thumb" height="107px" width="160px">
					<div class="gallery-title">Getting All Set for the Big Day Tomorrow</div></a>
					
					
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-100.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-101.jpg"></a> 
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-102.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-103.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-104.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-105.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-106.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-107.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-108.jpg"></a>
					<a class="colorbox" rel="one" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/mirren-day-0-109.jpg"></a>
										
					 
               </div>
               
               <div class="gallery">

					<a href="//fast.wistia.net/embed/iframe/qdttkizm8g?width=600&height=425&iframe=true" class="gallery colorbox-load"><img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/day-0/docurated-thumb.png" rel="group1">
					<span class="gallery-title">Productivity Hacks to Help You Stay Organized</span></a>
										 </div>
					 
					 <div class="gallery">
					
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-09.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-09.jpg">
					<span class="gallery-title">Last Year’s Event: Day One Photos</span></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-01.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-02.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-03.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-04.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-05.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-06.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-07.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-08.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-01.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-10.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-11.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-12.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-13.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-14.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-15.jpg"></a>
					<a class="colorbox" rel="three" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-1/2013-day-1-16.jpg"></a>
					
					 </div>
					 
					 
					
  <div class="gallery">
				
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-02.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-02.jpg">
					<span class="gallery-title">Last Year’s Event: Day One Photos</span></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-01.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-02.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-03.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-04.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-05.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-06.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-07.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-08.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-01.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-10.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-11.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-12.jpg"></a>
					<a class="colorbox" rel="four" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-2/2013-day-2-13.jpg"></a>
					
					
					 </div>			

					 
					
 <div class="gallery">
				
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-05.jpg">
					<img src="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-05.jpg">
					<span class="gallery-title">Last Year’s Pre-Conference Pub Night</span></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-01.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-02.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-03.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-04.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-05.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-06.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-07.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-08.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-01.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-10.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-11.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-12.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-13.jpg"></a>
					<a class="colorbox" rel="five" href="/sites/all/themes/mirren_bootstrap/assets/images/conference-2014/2013-day-0/2013-day-0-14.jpg"></a>
					
					
					 </div>		
 

					
					
         </div>
      </div>
      </div>
   </div>
</div>
                   


        <div id="call-to-action">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php print render($page['anon_user_cta']); ?>
                        
                        


                      
                        
                        
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        <!--*****Live Sessions Start*****-->

       <!--
 <div id="live-sessions">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h3 class="session-title">Sessions Live Now</h3>

                        <p>The broadcasts open approximately 5 minutes prior to the start of each session. You will need the password provided in the email you received today (unique password for each day). When logging in, be sure to use your agency email address.</p>

                        <p>If you miss a session, recordings become available down below within 1 - 2 hours of each session completing. You will have access to these recordings for 2 full weeks (until May 28th).</p>
                    </div>

                    <div class="col-sm-9">
                        <?php
                           if ($content['field_conf_recording_link']['#items']['0']['value']=='upcoming') {
                              
                        } else {  
                         
                                    }?><?php echo views_embed_view('conference_sessions', 'live_sessions');?>
                    </div>
                </div>
            </div>
        </div>
--><!--*****Live Sessions End*****-->
        
        

        
        
        
        
        <!--*****Upcoming Recordings - Start*****-->

        <div id="upcoming-sessions">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h3 class="session-title">Tuesday, May 13</h3>

                        
                    </div>

                    <div class="col-sm-9">
                   <?php echo views_embed_view('conference_sessions', 'upcoming_sessions');?>
                    </div>
                </div>
            </div>
        </div><!--*****Upcoming Recordings - End*****-->
        <!--*****Session Recordings - Start*****-->

        <div id="session-recordings">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <h3 class="session-title">Wednesday, May 14</h3>
							
                        
                    </div>

                    <div class="col-sm-9">
                  <?php echo views_embed_view('conference_sessions', 'completed_sessions');?>
                    </div>
                </div>
            </div>
        </div><!--*****Session Recordings - End*****-->
        <!--*****Call to Action - Start*****-->

        <div id="call-to-action-two">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php print render($page['advertising_banner']);?>
                    </div>
                </div>
            </div>
        </div><!--*****Call to Action - End*****-->
        <!--*****Broadcast Form - Start*****-->

       <!--
 <div id="broadcast-form">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1><span>1. Choose Your Three Leads</span></h1>

                      <div class="lead-selection-wrapper">
                      
                        <div id="leads-choice-view" class="views-exposed-widgets clearfix in">
                            <?php echo views_embed_view('lead_view', 'page_1');?>
                        </div>
                      </div>  
                        
                    </div>
                </div>
            </div>


            <div class="hr-rule"></div>
        </div>
-->
        <div id="broadcast-form">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                    <h1>Activate Your Trial Membership</h1>
                        <div id="name-choice-view">
                            <?php print render($page['conference_form']);?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hr-rule"></div>
        </div>
        
        <!--*****Broadcast Form - End*****-->
        <!--*****Footer - Start*****-->

        <div id="broadcast-footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Conference Sponsors</h3>
                    </div>
                </div>
            </div><?php print render($page['footer']);?><!--*****Footer - End*****-->
            <!-- /.section, /#footer -->
        </div>

        <div style="display:none;">
            <a class="hidden mirren-gate">Log In</a> <!-- Content Triggered by ".mirren-gate" class click -->

            <div id="mirren-gate-modal">
                <?php /* print '<h2>' . t('Join Mirren to view this content') . '</h2>'; */ ?><?php print '<p>' . t('You are now being redirected to view this live session. You will need the password provided in the email you received today (unique password for each day). When logging in, be sure to use your agency email address.<p>Please note, that some sessions may start a few minutes late as we are working hard to move 400 attendees around the venue.') . '</p>'; ?><?php /* print l('Log In', 'user', array('query' => drupal_get_destination(), 'attributes' => array('class' => array('btn-mirren', 'btn-jumbo')) ) ); */ ?><?php print l('Watch Now ›', 'membership', array('attributes' => array('class' => array('btn-mirren')) ) ); ?><!-- <button class="btn-mirren btn-jumbo colorbox-close">Cancel</button> -->
            </div>
        </div>
    </div>
    
    
    
    

    <div class="hidden-popup">
        <div id="inline_content">
            <div id="conference-fee-table">
                <img src="/sites/all/themes/mirren_bootstrap/assets/images/fee_table.png">

                <div class="close_button">
                    <button class="btn btn-primary" onclick="jQuery.colorbox.close();" type="button">CLOSE</button>
                </div>
            </div>
        </div>
    </div><!-- /#page -->
     
    
</body>
</html>
