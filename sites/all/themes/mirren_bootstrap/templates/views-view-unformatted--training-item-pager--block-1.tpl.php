<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

$result = ($view->result) ? current($view->result) : FALSE;


// Find next training item
$last_nid = NULL;
$next_nid = NULL;
foreach ($result->field_field_training_item as $item) {
  if ($last_nid == $result->nid) {
    $next_nid = $item['raw']['nid'];
    break;
  }
  $last_nid = $item['raw']['nid'];
}

// Find next chapter training item
$next_nid_chapter = NULL;
if (!$next_nid) {
  $chapter_nid = $result->field_training_item_node_nid;
  $chapters = $result->_field_data['field_chapters_node_nid']['entity']->field_chapters['und'];
  foreach ($chapters as $pos => $chapter) {
    if ($chapter['nid'] == $chapter_nid && isset($chapters[$pos + 1])) {
      $node = node_load($chapters[$pos + 1]['nid']);
      $next_nid_chapter = $node->field_training_item['und'][0]['nid'];
    }
  }
}

?>

<div class="link-left col-xs-4">
  <?php if ($result && $result->field_chapters_node_nid) : ?>
    <?php print l('&lsaquo;  Back to program chapters', "node/$result->field_chapters_node_nid", array('html' => TRUE, 'fragment' => $result->field_training_item_node_nid)); ?>
  <?php endif; ?>
</div>


<div class="link-center col-xs-4">
  <span class="link-trouble-viewing">Trouble viewing this video?</span>
</div>

<div class="link-right col-xs-4">
  <?php
  if ($result && $next_nid) {
    print l('Continue to the next training item &rsaquo;', "node/$next_nid", array('html' => TRUE));
  }
  elseif ($next_nid_chapter) {
    print l('Continue to the next program chapter &rsaquo;', "node/$next_nid_chapter", array('html' => TRUE));
  }
  ?>
</div>