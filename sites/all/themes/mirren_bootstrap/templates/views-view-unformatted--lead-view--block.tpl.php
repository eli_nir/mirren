<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)) { ?>
  <h3><?php print $title; ?></h3>
<?php } ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="row teaser-each ' . $classes_array[$id] .'"';  } ?>>
    <!-- 1/4 -->
    <div class="col-xs-3 teaser-icon">            
	    <div class="lead-icon clearfix">
	      <div class="col-xs-12">
	        <div class="row">
	          <div class="col-xs-12">
	            <span class="full-stripe"></span>
	          </div>
	          <div class="col-xs-12">
	            <span class="full-stripe"></span>
	          </div>
	          <div class="col-xs-12">
	            <span class="full-stripe"></span>
	          </div>
	        </div>

	        <div class="row">
	          <div class="lead-icon-left col-xs-6">
	            <span class="half-stripe"></span>
	            <span class="half-stripe"></span>
	            <span class="half-stripe"></span>
	            <span class="half-stripe"></span>
	          </div>
	          <div class="lead-icon-right col-xs-6">
	            <span class="half-stripe"></span>
	            <span class="half-stripe"></span>
	            <span class="half-stripe"></span>
	            <span class="half-stripe"></span>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>
	<!-- 3/4 -->
	<div class="col-xs-9 teaser-detail">
		<?php print $row; ?>
	</div>
  </div>
<?php endforeach; ?>