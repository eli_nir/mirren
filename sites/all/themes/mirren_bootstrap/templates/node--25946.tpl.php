<?php
/*
  *
  * This is the My Agency Hub Page
  *
*/
//This is getting the current logged in user 
global $user;
$user_id = $user->uid;
//load the user 
$user_data = user_load($user->uid);
//--------------------------------
//if there is a team node reference
if(isset($user_data->field_team_user['und'][0]['nid'])){
  //load the Team Node
  $team_data = node_load($user_data->field_team_user['und'][0]['nid']);
  //get title
  $team_title = $team_data->title;
  //get team logo
  if(isset($team_data->field_logo_team['und'][0]['uri'])){
    $team_logo_uri = $team_data->field_logo_team['und'][0]['uri'];
  }
  //get website url
  if(isset($team_data->field_website['und'][0]['url'])){
    $team_website_url = $team_data->field_website['und'][0]['url'];
    //check for http://
    if (substr($team_website_url, 0, 7) != 'http://'){
      $team_website_url = 'http://' . $team_website_url;
    } 
  }  
  //get facebook url
  if(isset($team_data->field_facebook['und'][0]['url'])){
    $team_facebook_url = $team_data->field_website['und'][0]['url'];
    //check for http://
    if (substr($team_facebook_url, 0, 7) != 'http://'){
      $team_facebook_url = 'http://' . $team_facebook_url;
    } 
  }  
  //get blog url
  if(isset($team_data->field_blog['und'][0]['url'])){
    $team_blog_url = $team_data->field_blog['und'][0]['url'];
    //check for http://
    if (substr($team_blog_url, 0, 7) != 'http://'){
      $team_blog_url = 'http://' . $team_blog_url;
    } 
  }  
  //get linkedin url
  if(isset($team_data->field_linkedin['und'][0]['url'])){
    $team_linkedin_url = $team_data->field_linkedin['und'][0]['url'];
    //check for http://
    if (substr($team_linkedin_url, 0, 7) != 'http://'){
      $team_linkedin_url = 'http://' . $team_linkedin_url;
    } 
  }  
  //get twitter url
  if(isset($team_data->field_twitter_link['und'][0]['url'])){
    $team_twitter_url = $team_data->field_twitter_link['und'][0]['url'];
    //check for http://
    if (substr($team_twitter_url, 0, 7) != 'http://'){
      $team_twitter_url = 'http://' . $team_twitter_url;
    } 
  } 
//-----
}?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="header-intro">  
    <?php 
    // Header Submenu Region
    if (isset($header_submenu)) { 
    ?>    
      <div class="intro-nav">
      <?php print render($header_submenu); ?>
      </div>
    <?php } ?>
    <div class="header-intro-content clearfix">
      <div class="row">
        <div class="col-xs-7">
          <div class="header-intro-text">
            <?php
            //if there is a team node reference
            if ( isset($team_title) ) {
              print "<h1>" . $team_title . "&nbsp;&nbsp;<span id='team-edit-link'>" . "<a href='/node/" . $user_data->field_team_user['und'][0]['nid'] . "/edit'>Edit ›</a>". "</h1>";
              //if website url
              if( isset($team_website_url) ){
                print "<div class='field-name-field-website'>";
                print l($team_website_url, $team_website_url, array('attributes'=>array('target'=>'_blank')));
                print "</div>";
              }
              //if there is any addresses from the field collection
              if(isset($team_data->field_address['und'])){
                //address field collection loading
                //http://drupal.stackexchange.com/a/35884
                //http://api.drupalecommerce.org/api/misc/field_collection!field_collection.module/function/field_collection_item_load_multiple/7.x
                //build ID array
                $ids = array();
                foreach ($team_data->field_address['und'] as $key => $value) {
                  $ids[] = $value['value'];
                }
                $address_field_collection = field_collection_item_load_multiple($ids);
                print "<div id='node-address'>";
                //go through field collection array and get to the data
                foreach ($ids as $pos => $key) {
                  //build address as per the field collection
                  print "<div class='team-address-container'>";
                  //address 1 check
                  if(isset($address_field_collection[$key]->field_address_1['und'][0]['safe_value'])){
                    print "<div class='field-name-field-address-1'>";
                    print $address_field_collection[$key]->field_address_1['und'][0]['safe_value'];
                    print "</div>";
                  }
                  //address 2 check
                  if(isset($address_field_collection[$key]->field_address_2['und'][0]['safe_value'])){
                    print "<div class='field-name-field-address-2'>";
                    print $address_field_collection[$key]->field_address_2['und'][0]['safe_value'];
                    print "</div>";
                  }
                  //field_city check
                  if(isset($address_field_collection[$key]->field_city['und'][0]['safe_value'])){
                    print "<div class='field-name-field-city'>";
                    print $address_field_collection[$key]->field_city['und'][0]['safe_value'];
                    print ",&nbsp;</div>";
                  }
                  //field_state_province check
                  if(isset($address_field_collection[$key]->field_state_province['und'][0]['value'])){
                    print "<div class='field-name-field-state-province'>";
                    print $address_field_collection[$key]->field_state_province['und'][0]['value'];
                    print "&nbsp;</div>";
                  }
                  //field_zip_postal_code check
                  if(isset($address_field_collection[$key]->field_zip_postal_code['und'][0]['safe_value'])){
                    print "<div class='field-name-field-zip-postal-code'>";
                    print $address_field_collection[$key]->field_zip_postal_code['und'][0]['safe_value'];
                    print "</div>";
                  }
                  //field_country check
                  if(isset($address_field_collection[$key]->field_country['und'][0]['value'])){
                    print "<div class='field-name-field-country'>";
                    print $address_field_collection[$key]->field_country['und'][0]['value'];
                    print "</div>";
                  }
                  print "</div>";
                }
                print "</div>";
              }
              //----------------------------------------
            }else{?>
              <h1><?php print $title_attributes; ?><?php print $title; ?></h1>
            <?php }?>
            <div id="team-social-media">
              <?php
              //if there is a facebook link
              if(isset($team_facebook_url)){
                print "<div class='field-name-field-facebook'>";
                print l('Facebook', $team_facebook_url, array('attributes'=>array('target'=>'_blank')));
                print "</div>";
              }
              //if there is a twitter link
              if(isset($team_twitter_url)){
                print "<div class='field-name-field-twitter-link'>";
                print l('Twitter', $team_twitter_url , array('attributes'=>array('target'=>'_blank')));
                print "</div>";
              }
              //if there is a linkedin link
              if(isset($team_linkedin_url)){
                print "<div class='field-name-field-linkedin'>";
                print l('Linkedin', $team_linkedin_url , array('attributes'=>array('target'=>'_blank')));
                print "</div>";
              }
              //if there is a linkedin link
              if(isset($team_blog_url)){
                print "<div class='field-name-field-blog'>";
                print l('Blog', $team_blog_url, array('attributes'=>array('target'=>'_blank')));
                print "</div>";
              }
              ?>
            </div>
          </div><!-- /.header-intro-text -->
        </div>
        <div class="col-xs-5">
          
          <?php
          //if there is a logo
          if($team_logo_uri){
            //get image path
            $team_logo_path = file_create_url($team_logo_uri);
            print "<div class='field-name-field-logo-team'>";
            print "<img src='" . $team_logo_path . "' />";
            print "</div>";
            //going to leave in the image style code in case of a change
            /*$author_picture = theme(
              'image_style',
              array(
              'path' => $author_picture_uri,
              'style_name' => 'thumbnail'
              )
            );*/
          }//----------------------------
          ?>
        </div>
      </div><!-- /.row -->
    </div><!-- /.header-intro-content -->
  </div><!-- /.header-intro -->
  <div class="row">
    <div class="col-xs-8">
      <?php // MEMBER CONTENT
        if ($contentAccess) { 
      ?>  
      <?php
      if(isset($team_data->body['und'][0]['safe_value'])){
        print "<h2>Company Overview</h2>";
        print "<div class='team-body'>";
        print $team_data->body['und'][0]['safe_value'];
        print "</div>";
      }
      ?>
      <div id="agency-user-profile">
        <h2>My Profile&nbsp;&nbsp;<span><a href="user/<?php print $user_data->uid; ?>/edit">Edit ›</a></span></h2>
        <?php
        //if there is a user pic
        if(isset($user_data->picture->uri)){
          $author_picture = theme(
            'image_style',
            array(
            'path' => $user_data->picture->uri,
            'style_name' => 'image_140_140'
            )
          );
          print "<div class='profile-image'>";
          print $author_picture;
          print "</div>";
        }
        
        //if there is names
        if(isset($user_data->field_first_name['und'][0]['safe_value'])){
          print "<div class='user-name'>";
          print $user_data->field_first_name['und'][0]['safe_value'];
          //if there is a last name filled out
          if(isset($user_data->field_last_name['und'][0]['safe_value'])){
            print "&nbsp;" . $user_data->field_last_name['und'][0]['safe_value'];
          }
          print "</div>";
        }
        //if there is a position
        if(isset($user_data->field_position_user['und'][0]['safe_value'])){
          print "<div class='user-position'>";
          print $user_data->field_position_user['und'][0]['safe_value'];
          print "</div>";
        }     
        //member since
        print "<div class='user-since'>";
        print "Member since ";
        print date('Y', $user_data->created);
        print "</div>";     
        //create the proper urls 
        //get facebook url
        if(isset($user_data->field_facebook_user['und'][0]['url'])){
          $user_facebook_url = $user_data->field_facebook_user['und'][0]['url'];
          //check for http://
          if (substr($user_facebook_url, 0, 7) != 'http://'){
            $user_facebook_url = 'http://' . $user_facebook_url;
          } 
        } 
        //get twitter url
        if(isset($user_data->field_twitter_user['und'][0]['url'])){
          $user_twitter_url = $user_data->field_twitter_user['und'][0]['url'];
          //check for http://
          if (substr($user_twitter_url, 0, 7) != 'http://'){
            $user_twitter_url = 'http://' . $user_twitter_url;
          } 
        }  
        //get linkedin url
        if(isset($user_data->field_linkedin_user['und'][0]['url'])){
          $user_linkedin_url = $user_data->field_linkedin_user['und'][0]['url'];
          //check for http://
          if (substr($user_linkedin_url, 0, 7) != 'http://'){
            $user_linkedin_url = 'http://' . $user_linkedin_url;
          } 
        } 
        //get website url
        if(isset($user_data->field_personal_website['und'][0]['url'])){
          $user_website_url = $user_data->field_personal_website['und'][0]['url'];
          //check for http://
          if (substr($user_website_url, 0, 7) != 'http://'){
            $user_website_url = 'http://' . $user_website_url;
          } 
        }  
        print "<div id='my-agency-user-social-links'>";
        //if there is a facebook link
        if(isset($user_facebook_url)){
          print "<div class='field-name-field-facebook'>";
          print l('Facebook', $user_facebook_url, array('attributes'=>array('target'=>'_blank')));
          print "</div>";
        }
        //if there is a twitter link
        if(isset($user_twitter_url)){
          print "<div class='field-name-field-twitter-link'>";
          print l('Twitter', $user_twitter_url , array('attributes'=>array('target'=>'_blank')));
          print "</div>";
        }
        //if there is a linkedin link
        if(isset($user_linkedin_url)){
          print "<div class='field-name-field-linkedin'>";
          print l('Linkedin', $user_linkedin_url , array('attributes'=>array('target'=>'_blank')));
          print "</div>";
        }
        //if there is a linkedin link
        if(isset($user_website_url)){
          print "<div class='field-name-field-blog'>";
          print l('Blog', $user_website_url, array('attributes'=>array('target'=>'_blank')));
          print "</div>";
        }
        print "</div>";
        //if there is a user bio
        if(isset($user_data->field_bio_user['und'][0]['safe_value'])){
          print "<div class='user-bio-body'>";
          print $user_data->field_bio_user['und'][0]['safe_value'];
          print "</div>";
        }?>
      <?php
        }
      ?>
      </div>
      <?php print render($content); ?>
    </div>
    <div class="col-xs-4 sidebar-right-3 sidebar">
      <?php print render($sidebar_right_3); ?>
    </div>  
  </div>
</article>
<?php
// MIRREN-505 (02/14)
// modal content change, stashing member code work:
//invite colorbox fun
//if there is a member code
/*if(isset($team_data->field_agency_code['und'][0]['safe_value'])){
  //build membercode html
  $member_code_html = "<br />With your Membership Code: " . $team_data->field_agency_code['und'][0]['safe_value'] ;
}else{
  //no member code
  $member_code_html = "";
}*/
/*<div style="display:none">
  <div id="my-agency-hub-colorbox">
    <h4>Adding New Team Members to Mirren</h4>
    <p>Membership includes unlimited seats from your office location (or all of your offices if you have a multi-office / national membership).</p>
    <p>To add more team members, simply email <a href="mailto:member.services@mirren.com">Member Services</a> with the following information for each team member:</p>
    <ul>
      <li>Name </li> 
      <li>Title </li>
      <li>Email </li>
      <li>City </li> 
      <li>State </li>
    </ul>
    <p>Be sure to email us from the same email you use with your Mirren Membership.</p>
    <p>We will set up their membership and contact them with their login information within 2 - 3 business days.</p>
    <button class="btn-mirren colorbox-close">Close</button>
  </div>
</div>*/


?>
<div style="display:none">
  <div id="my-agency-hub-colorbox">
    <h4>Adding More Team Members to Mirren</h4>
    <p>Membership includes unlimited seats from your office location (or all of your offices if you have a multi-office / national membership).</p>
    <p>To add more team members, simply email <a href="mailto:member.service@mirren.org">Member Services</a> with the following information for each team member:</p>
    <ul>
      <li>Name</li>
      <li>Title</li>
      <li>Email</li>
      <li>City</li>
      <li>State</li>
    </ul>    
    <p>Be sure to email us from the same email you use with your Mirren Membership.</p>
    <p>We will set up their membership and contact them with their login information within 2 - 3 business days</p>
    <button class="btn-mirren colorbox-close">Close</button>
  </div>
</div>