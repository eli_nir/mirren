<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

?>





<div id="wistia-video">
       <div class="container">
           <div class="row">
                 <div class="col-sm-8">



<?php // MEMBER CONTENT

if (user_is_logged_in()) {?>
              
<div class="video-container">
<div id="wistia-video">
<iframe src="//fast.wistia.net/embed/iframe/<?php print $content['field_wistia_video']['#items'][0]['value']; ?>" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="590" height="360"></iframe></div>
</div>
                 </div>

<div class="col-sm-4">
                 
                 	<div class="video-information">

<h2><?php print $content['field_wistia_title']['#items'][0]['value'];?></h2>
<p class="wistia-subtitle"><?php print $content['field_wistia_subtitle']['#items'][0]['value'];?></p>
<p class="wistia-description"><?php print $content['field_wistia_text']['#items'][0]['value'];?></p>
                 	</div>
</div>
                
      <?php
                } else {
              ?>
               
               
               <div class="video-container">
               <div class="mirren-gate-wrapper">
               <?php print render($content['field_non_member_preview']); ?>
                  <a class="mirren-gate">Play <span></span></a>
                </div>
               </div>
              
               
                 </div>
               
               <div class="col-sm-4">
                 
                 	<div class="video-information">
               
               
               
               
               
                
<h2><?php print $content['field_wistia_title']['#items'][0]['value'];?></h2>
<p class="wistia-subtitle"><?php print $content['field_wistia_subtitle']['#items'][0]['value'];?></p>
<p class="wistia-description"><?php print $content['field_wistia_text']['#items'][0]['value'];?></p>
                 	</div>
             
              <?php
                }
              ?> 
              
 </div>
            
          </div>
	 </div>
</div>

              
    