<?php
// Copy Theme Default Node Template
?>

<?php // IF teaser display
  if ($teaser) { 
?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
    <?php print render($title_prefix); ?>
      <?php if (!$page) { ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php } ?>
    <?php print render($title_suffix); ?>
    
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>

    <?php print render($content['links']); ?>

    <?php print render($content['comments']); ?>
    
  </article>

<?php } // ELSE default display (full node)
  else { 
?>

    <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> video clearfix"<?php print $attributes; ?>>
      
      
        <div class="header-intro">  
          
            <div class="header-intro-content">

              <div class="header-intro-text">
                <div class="video-header-left">
                  <?php print render($title_prefix); ?>
                    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
                  <?php print render($title_suffix); ?>
                  <?php // print render($content['field_conference_speaker']); ?>
                  <?php 
                    // embed Speaker Name, Co. List views block
                    print views_embed_view('conference_speaker', $display_id = 'block_32');
                  ?>
                </div>
                <div class="video-header-right">
                  <?php print render($content['field_duration']); ?>
                </div>
              </div><!-- /.header-intro-text -->

              <?php // MEMBER CONTENT
                if ($contentAccess) { 
              ?>
                <div class="feature-video-embed">
                  <iframe scrolling="no" width="864" height="486" frameborder="0" name="wistia_embed" class="wistia_embed" allowtransparency="true" src="//fast.wistia.net/embed/iframe/<?php print ($node->field_video_id['und'][0]['value']); ?>?videoFoam=true&plugin%5Bresumable%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fresumable%2Fplugin.js&plugin%5Bresumable%5D%5Basync%5D=false"></iframe>
                </div>
              <?php
                } else {
              ?>
                <div class="mirren-gate-wrapper">
                  <a class="mirren-gate">Play <span></span></a>
                </div>
              <?php
                }
              ?> 

              <div class="video-navigation row">
                <div class="link-left col-xs-4">
                  <?php print l(t('‹ Back to All Guest Speakers'), 'node/25767'); ?>
                </div>
                <div class="link-center col-xs-4">
                  <span class="link-trouble-viewing">Trouble viewing this video?</span>
                </div>
                <div class="link-right col-xs-4">
                  &nbsp;
                </div>
                <div class="hidden" id="video_message">
                  <p>If you are experiencing any problems playing a video, often a simple browser refresh or restart will resolve the issue. You can 
also try clearing the cache in your browser. If you continue to have any issues, please immediately contact <a href="?width=670&amp;height=340&amp;inline=true#member-services-modal" class="colorbox-inline">Member Services</a>.</p>
                  <a class="close" href="#">&times;</a>
                </div>
                <div style="display:none;">
                  <div id="member-services-modal">
                    <?php
                      $block = block_load('block', '20');
                      $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
                      print $output;
                    ?>
                    <button class="btn-mirren colorbox-close">Close</button>
                  </div>
                </div>
              </div>

            </div>

          
        </div><!-- /.header-intro -->
      


        <div class="row">

          <?php 
            if ( isset($sidebar_right_3) ) { 
              print '<div class="col-xs-9">';
            } 
            elseif ( isset($sidebar_left_3) ) {
              print '<div class="col-xs-9 col-xs-push-3">';
            }
            elseif ( isset($sidebar_right_4) ) {
              print '<div class="col-xs-8">';
            }
            else {
              print '<div class="col-xs-12">';
            }
          ?>
          

            <!--  here -->
            <?php // MEMBER CONTENT
              if ($contentAccess) { 
            ?>

              <?php
              //  MEMBER - Video Overview
              //  =====================================
              ?>
              <div class="group-video-overview group-body-group">
                <?php 
                  print '<h2>' . t('Video Overview') . '</h2>';
                  /* Removed Read More/Read Less for videos. Now all can see description and series overview
				  print ($node->field_non_member_teaser_video['und'][0]['value']);
                  print '<a class="read-more-link collapsed" data-toggle="collapse" data-target="#field-video-overview">Read More</a>';*/
                  print '<div id="field-video-overview">';
                  print ($node->body['und'][0]['value']);
                  print '</div>';
                  /* print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-video-overview">Read Less</a>'; */
                ?>
              </div>

              <?php
              //  MEMBER - Download File Links
              //  =========================================
              ?>
              <div class="field-name-field-file-download-lead">
                <?php foreach ($field_file_download_lead as $tvalue) {
                  //dpm($tvalue);
                  $pdf_filename = render($tvalue['filename']);
                  $pdf_uri      = render($tvalue['uri']);
                  if ( !empty($tvalue['description']) ) {
                    $pdf_description = render($tvalue['description']);
                  }
                  else {
                    $pdf_description = 'Download File'; 
                  }
                ?>
                  <span class="file">
                    <a 
                      href="<?php print render(file_create_url($pdf_uri)); ?>"
                      title="Download <?php print $pdf_filename; ?>" target="_blank"
                    >
                      <?php print $pdf_description; ?>
                    </a>
                    
                  </span>
                <?php // closes foreach
                  }
                ?>
              </div>

              <?php 
                //hide the flagging link from the body content
                hide($content['flag_team_library']);
                hide($content['flag_team_member_library']); 
                hide($content['field_file_download_lead']); 
                print render($content); 
              ?>

            <?php 
              }  // ANONYMOUS USER
              else {
            ?>
              <?php
              //  ANONYMOUS - Video Overview
              //  =========================================
              ?>
              <div class="group-video-overview group-body-group">
                <?php 
                  print '<h2>' . t('Video Overview') . '</h2>';
                  /* Removed Read More/Read Less for videos. Now all can see description and series overview
				  print ($node->field_non_member_teaser_video['und'][0]['value']);
                  print '<a class="read-more-link collapsed" data-toggle="collapse" data-target="#field-video-overview">Read More</a>';*/
                  print '<div id="field-video-overview">';
                  print ($node->body['und'][0]['value']);
                  print '</div>';
                  /* print '<a class="read-less-link hidden" data-toggle="collapse" data-target="#field-video-overview">Read Less</a>'; */
                ?>

              </div>
              
              <?php
              //  ANONYMOUS - ALL TYPES - Gray "Buttons" represent files
              //  =======================================================
              ?>
              <div class="field-name-field-file-download-lead">
                <?php foreach ($field_file_download_lead as $tvalue) {
                  //dpm($tvalue);
                  if ( !empty($tvalue['description']) ) {
                    $pdf_description = render($tvalue['description']);
                  }
                  else {
                    $pdf_description = 'Download File'; 
                  }
                ?>
                  <span class="file">
                    <span class="non-member">
                      <?php print $pdf_description; ?>
                    </span>
                  </span>
                <?php // closes foreach
                  }
                ?>
              </div>

            <?php 
              }
            ?>

            


          </div>

          <?php if ( isset($sidebar_right_3) ) { ?>
            <div class="col-xs-3 sidebar-right-3 sidebar">
              <?php
              //this is for the flaggin block. This is part of the node content
              if ( ($contentAccess) && isset($content['flag_team_library']) ) {  
                print '<div class="block block-block" id="library-flag-block">';
                print "<h2>" . t('Library') . "</h2>";
                print "<div class='content'><ul>";
                print "<li>" . render($content['flag_team_member_library']) . "</li>";
                print "<li>" . l(t('View My Library ›'), 'my-library') . "</li>";
                print "<li>" . render($content['flag_team_library']) . "</li>";
                //print "<li>" . l(t('View Team Library'), 'team-library') . "</li>";
                print "</ul></div>";
                print "</div>";
              }
              ?>
              <?php print render($sidebar_right_3); ?>
            </div>
          <?php } 
            elseif ( isset($sidebar_left_3) ) { 
          ?>
            <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
              <?php print render($sidebar_left_3); ?>
            </div>
          <?php } 
            elseif ( isset($sidebar_right_4) ) { 
          ?>
            <div class="col-xs-4 sidebar-right-4 sidebar">
              <?php print render($sidebar_right_4); ?>
            </div>
          <?php }
            else { ?>
            <!-- 1-col -->
          <?php 
            }
          ?>
            
        </div>
        
      </article>


<?php } ?>