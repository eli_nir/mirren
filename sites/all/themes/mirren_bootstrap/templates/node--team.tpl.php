<?php
// Theme Default Node Template
?>

<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <div class="header-intro">  
      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>
      <div class="header-intro-content clearfix">
        <div class="row">
          <div class="col-xs-7">
            <div class="header-intro-text">
              <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
              <?php print render($content['field_website']);?>
              <div id="node-address">
                <?php print render($content['field_address']);?> 
              </div>
              <div id="node-social-media">
                <?php print render($content['field_facebook']);?> 
                <?php print render($content['field_twitter_link']);?> 
                <?php print render($content['field_linkedin']);?> 
                <?php print render($content['field_blog']);?> 
              </div>
            </div>
          </div>
          <div class="col-xs-5">
            <?php print render($content['field_logo_team']);?> 
          </div>
        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->
    <div class="row">
      <div class="col-xs-8 main-target">
        <?php //print render($content); ?>
        <h2>Company Overview</h2>
        <?php print render($content['body']);?> 
      </div>

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-4 sidebar-right-3 sidebar">
          <?php print render($sidebar_right_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>

<?php } ?>