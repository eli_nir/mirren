<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>
<div class="header-intro-content user-profile-page clearfix">
  <div class="col-xs-8">
    <div class="header-intro-text clearfix">
			<h1 property="dc:title" datatype="">
				<?php print strip_tags(render($user_profile['field_first_name'])); ?> <?php print strip_tags(render($user_profile['field_last_name'])); ?>
				<span class="edit-profile"><?php print l(t('Edit ›'), "user/{$GLOBALS['user']->uid}/edit");?></span>
			</h1>
			
						
      <?php print render($user_profile['user_picture']); ?>
      <div id="user-position">
      	<?php 
      		if (isset($user_profile['field_position_user'])) {
      			print strip_tags(render($user_profile['field_position_user']));
      		}
      		if ( isset($user_profile['field_position_user']) && isset($user_profile['field_team_user']) ) {
      			print ' at ';
      		}
      		if (isset($user_profile['field_team_user'])) {
      			print render($user_profile['field_team_user']);
      		}
		?>
      </div>
       <!--
       
       Removed Code for "Member Since:"
      
<div class="member-since">
				<?php 
				//load the current user data because it is not in the profile
				$user = user_load(arg(1));	
				print "Member since: ";
				print date('D, F j, Y g:ia', $user->created);
				?>
      </div>
-->




      <?php print render($user_profile['field_personal_website']); ?>
		</div>
	</div>
	<div class="col-xs-4">
		<div id="user-social-media">
	    <?php print render($user_profile['field_facebook_user']); ?>
	    <?php print render($user_profile['field_twitter_user']); ?>
	    <?php print render($user_profile['field_linkedin_user']); ?>
	    <?php print render($user_profile['field_personal_website']); ?>
	  </div>
	  <div class="leads-updates">
	
	<a href="/newsletter/subscriptions/">Edit Leads Email Updates ›</a>
	
	
</div>

	</div>
</div>
<div class="row">
	<div class="col-xs-8 user-profile-content">
		<h2>Personal Bio</h2>
    <?php print render($user_profile['field_bio_user']); ?>
	</div>
	<div class="col-xs-4 sidebar-right-3 sidebar">
		<?php 
		//if there is a team sidebar 
		$block = module_invoke('views', 'block_view', 'team_member_block_users_-block');
		if(isset($block['content'])){
			print "<div id='team-members-block'>";
			print "<h2>";
			print render($block['subject']);
			print "</h2>";
			print render($block['content']);
			print "</div>";
		}?>
	</div>    
</div>


