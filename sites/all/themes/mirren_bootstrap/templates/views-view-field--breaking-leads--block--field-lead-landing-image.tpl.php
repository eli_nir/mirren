<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 * http://mirren.boriszaydlin.com/admin/structure/views/nojs/config-item/breaking_leads/block/field/field_title_non_member
 */
	global $user;
	
	$url = $row->_field_data['nid']['entity']->field_lead_landing_image_crop_nm['und'][0]['uri'];

	if(!empty($url) && ($user->uid == 0)){

		$image_anonim = theme(
			'image_style',
			array(
				'path' => $url,
				'style_name' => '80x80',
			)
		);

		$output = $image_anonim;
	}

	if ( $user->uid ) {
		if ( strstr($row->_field_data['nid']['entity']->field_lead_landing_image_lib['und'][0]['uri'], 'Rectangle') ) {
			$newURI = str_replace('Rectangle', 'Square', $row->_field_data['nid']['entity']->field_lead_landing_image_lib['und'][0]['uri']);
			if (file_exists($newURI)) {
				$output = theme_image_style(array('path' => $newURI, 'style_name' => '80x80', 'width' => '', 'height' => ''));
			}
		}
	}

	if ( $user->uid == 0 ) {
		if ( strstr($row->_field_data['nid']['entity']->field_lead_landing_image_lib_nm['und'][0]['uri'], 'Rectangle') ) {
			$newURI = str_replace('Rectangle', 'Square', $row->_field_data['nid']['entity']->field_lead_landing_image_lib_nm['und'][0]['uri']);
			if (file_exists($newURI)) {
				$output = theme_image_style(array('path' => $newURI, 'style_name' => '80x80', 'width' => '', 'height' => ''));
			}
		}
	}

?>
<?php print $output; ?>