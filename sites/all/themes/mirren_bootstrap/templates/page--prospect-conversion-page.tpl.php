<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<html>
<head>
<title></title>
</head>

<body>

    <div id="page" class="<?php if ( isset($header_status) ) { print $header_status; } ?>">
        <div id="secondary_menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php print render($page['header']); ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="main_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php print render($page['navigation']); ?><!-- Collapsed Nav -->

                        <nav class="navbar navbar-default collapsed-nav" role="navigation">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"></button> <a class="navbar-brand" href="http://www.mirren.com/">Mirren</a>
                                </div><!-- Collect the nav links, forms, and other content for toggling -->

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a href="/leads">Daily Leads</a></li>

                                        <li><a href="/training-online">On-Demand Learning</a></li>

                                        <li><a href="/training-in-person">In Person Training</a></li>

                                        <li><a href="/live-events">Live Events</a></li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">More</a>

                                            <ul class="dropdown-menu">
                                                <li><a href="/membership">Membership</a></li>

                                                <li><a href="/search-consultants">Search Consultants</a></li>

                                                <li><a href="/service-providers">Service Providers</a></li>

                                                <li><a href="/discussion-forums">Forums</a></li>

                                                <li><a href="/about-us">About / Contact</a></li>

                                                <li><a href="/blog">Blog</a></li>

                                                <li><a href="/subscribe">Mailing List</a></li>

                                                <li>
                                                    <!-- START OF LOGIN/LOGOUT BLOCK -->
                                                    <?php global $user;
                                                                                                            if ($user->uid != 0) {
                                                                                                           // code for the logout button ?>

                                                    <ul class="login-block">
                                                        <li class="first leaf"><a href="/user" title="">My Profile</a></li>

                                                        <li class="leaf"><a href="/my-agency-hub" title="">My Agency Hub</a></li>

                                                        <li class="leaf"><a href="/my-library" title="">My Library</a></li>

                                                        <li class="last leaf"><a href="/user/logout" title="">Logout</a></li>
                                                    </ul><?php
                                                                                                                    }
                                                                                                                 else {
                                                                                                                  // code for the login button
                                                                                                               echo "<a class=\"user-link\" href=\"?q=user\">Login</a>";
                                                                                                                 }
                                                                                                              ?><!-- END OF LOGIN/LOGOUT BLOCK -->
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        
        <?php print render($page['page_header_block']); ?>
        
        
        
         <?php print $messages; ?>
         
         <?php if ($tabs) { ?>

                    <div class="tabs">
                    <?php print render($tabs); ?>
                    </div>
                    <?php } ?>
                    <?php print render($page['help']); ?><?php if ($action_links) { ?>

                    <ul class="action-links">
                        <?php print render($action_links); ?>
                    </ul><?php } ?>
                    

        
        
        
        
         <?php print render($page['content']); ?> 

         
        <div id="main" class="clearfix container">
        
                    <div class="row">
                <div class="col-md-12">
                                       
                    
                                       
                    
                    
                </div>
            
            </div><!-- /.row -->
        </div><!-- /#main -->
        <!-- SECTION 2 -->
        <!-- SECTION 3 -->
</div>

  





    
       
 <!--*****Footer - Start*****-->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php print render($page['footer']);?>
                </div>
            </div>
        </div><!--*****Footer - End*****-->
        <!-- /.section, /#footer -->

        
            </footer>
            
            
            
<div class="newsletter-wrapper">
  <div class="newsletter-signup">
        <form class="form" role="form" action="/mirren-mailing-list" method="POST">
          <div class="form-group">
            <label class="sr-only" for="exampleInputEmail2">Email address</label>
            <input name="footer-email" type="email" id="exampleInputEmail2" placeholder="Receive Updates on Mirren Events &amp; News" />
          </div>
          <button type="submit" class="btn-mirren btn-mirren-default">Sign Up</button>
        </form>
      </div>


  </div>
  <div style="display:none;">
    <a class="hidden mirren-gate">Log In</a>
    <!-- Content Triggered by ".mirren-gate" class click -->
    <div id="mirren-gate-modal">
      <?php print '<h2>' . t('Join Mirren to view this content') . '</h2>'; ?>
      <?php print '<p>' . t('Mirren Membership provides exclusive access to a cutting-edge community and the best practices that are working to convert more business.') . '</p>'; ?>
      <?php print l('Log In', 'user', array('query' => drupal_get_destination(), 'attributes' => array('class' => array('btn-mirren', 'btn-jumbo')) ) ); ?>
      <?php print l('Learn About Membership', 'membership', array('attributes' => array('class' => array('btn-mirren')) ) ); ?>
      <button class="btn-mirren btn-jumbo colorbox-close">Cancel</button>
    </div>
  </div>
</div>
            
            
            
            
            
            
            
            
            <!-- /#page -->
    </body>
</html>
