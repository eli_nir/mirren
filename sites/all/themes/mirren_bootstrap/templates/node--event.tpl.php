<?php
// Event Content Type Node Template
// teaser code not used (showing views fields display)
// full node display - custom theming. Always a header; but no header feature img/text.
?>

<?php if ($teaser) { ?>

<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <?php print render($title_prefix); ?>
    <?php if (!$page) { ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php } ?>
  <?php print render($title_suffix); ?>
  
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
  
</article>

<?php } else { ?>

  <article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>    
 
    <div class="header-intro">  

      <?php 
        // Header Submenu Region
        if ( isset($header_submenu) ) { 
      ?>    
        <div class="intro-nav">
          <?php print render($header_submenu); ?>
        </div>
      <?php 
        }
      ?>

      <div class="header-intro-content clearfix">
        <div class="row">
          <div class="col-xs-8">
            <div class="header-intro-text">
              <?php print render($title_prefix); ?>
                <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
              <?php print render($title_suffix); ?>
              <?php  print render($content['field_event_type']); ?>
            </div><!-- /.header-intro-text -->
          </div>
          <div class="col-xs-3 col-xs-push-1">
            <div class="header-event-details">

				<!-- This code adjusts the time according to the time zone. These variables are not used when outputting the date and time for right now. See below if you want to reinstate. -->

              
				
				<?php 
                // http://drupal.stackexchange.com/questions/14661/format-date-seems-to-ignore-the-time-zone
                // Get the timezone offset
                /* $offset = date('Z', strtotime($field_date_event[0]['value'])); */
                $offset = date('Z', strtotime($field_date_event[0]['value']));
                // Apply the timezone offset
                $start_date_string = strtotime(date("Y-m-d g:ia", strtotime($field_date_event[0]['value'])) . " " . $offset . " seconds");
                $end_date_string = strtotime(date("Y-m-d g:ia", strtotime($field_date_event[0]['value2'])) . " " . $offset . " seconds");
                $right_now_string = strtotime(date("Y-m-d g:ia"));
                if ( $start_date_string > $right_now_string ) { 
                  $future_event = true;
                }
              ?>


              <div class="date-icon <?php print ($node->field_event_type['und'][0]['value']); ?>">
                <span class="month-full-year">
                  <?php print format_date($start_date_string, "custom", "M Y"); ?>
                </span>
                <span class="full-day">
                  <?php print format_date($start_date_string, "custom", "d"); ?>
                </span><i></i>
              </div>
              
              <?php 
                if ( $future_event ) {
              ?>
                <div class="event-time-formatted">
                 
					 	
					 	                  <?php 
                    print format_date($start_date_string, "custom", "g:i"); 
                    print ' - ';
                    /* print format_date($end_date_string, "custom", "g:i A T");  Remove Timezone  */ 
                    print format_date($end_date_string, "custom", "g:i A");
                    print_r(" ET")
                  ?>
                  
                
                  
                  
                  
                  
                
                  
                </div>  
                <div class="reg-wrap">
                  <a class="btn-mirren register-link" 
                     title="<?php print ($node->field_registration_link['und'][0]['title']); ?>"
                     href="<?php print ($node->field_registration_link['und'][0]['url']); ?>"
                     <?php if (isset($node->field_registration_link['und'][0]['attributes']['target']) == '_blank') { ?>target="_blank"<?php } ?>>
                     <?php print ($node->field_registration_link['und'][0]['title']); ?> ›
                  </a>
                </div>
              <?php
                }
              ?>
            </div>
          </div>
        </div>
      </div><!-- /.header-intro-content -->
    </div><!-- /.header-intro -->
    

    <?php 
      // Full Width Banner Space
      if ( isset($full_width_banner) ) { 
    ?>    
      <div class="row">
        <div id="full_width_banner" class="col-xs-12">
          <?php print render($full_width_banner); ?>
        </div>
      </div>
    <?php 
      }
    ?>

    <div class="row">

      <?php 
        if ( isset($sidebar_right_3) ) { 
          print '<div class="col-xs-9 main-target">';
        } 
        elseif ( isset($sidebar_left_3) ) {
          print '<div class="col-xs-9 col-xs-push-3 main-target">';
        }
        elseif ( isset($sidebar_right_4) ) {
          print '<div class="col-xs-8 main-target">';
        }
        else {
          print '<div class="col-xs-12 main-target">';
        }
      ?>

        <?php 
          //hide the flagging link from the body content
          hide($content['flag_team_library']);
          hide($content['flag_team_member_library']); 
          hide($content['field_date_event']); 
          print render($content); 
        ?>

        <?php if ( isset($content['field_cost_table']) ) { ?>
          <!-- FIELD COLLECTION -->    
          <div class="event-cost-table" id="">
            <div class="cost-table-row ct-header">
              <div class="ct-header ct-type">&nbsp;</div>
              <div class="ct-header ct-mem">Member</div>
              <div class="ct-header ct-non-mem">Non-Member</div>
            </div>

            <?php 
              $ct_count = '1';
              foreach ($content['field_cost_table']['#items'] as $entity_uri) : $field_collection_item = entity_load('field_collection_item', $entity_uri);
              foreach ($field_collection_item as $field_collection_object) : 
              $ct_type = $field_collection_object->field_cost_type['und'][0]['value']; 
              $ct_member_price = $field_collection_object->field_member_price['und'][0]['value'];
              $ct_non_member_price = $field_collection_object->field_non_member_price['und'][0]['value'];  
              $ct_info = $field_collection_object->field_additional_info_cost_table['und'][0]['value']; 
            ?>

            <div class="cost-table-row ct-row-<?php print $ct_count; ?>">
              <div class="ct-type">
                <div class="orange-dot mirren-tooltip" data-toggle="tooltip" title="<?php print $ct_info; ?>"><span>?</span></div><?php print $ct_type; ?>
              </div>
              <div class="ct-mem"><?php print '$' . $ct_member_price; ?></div>
              <div class="ct-non-mem"><?php print '$' . $ct_non_member_price; ?></div>
        
            </div>
              
             <?php $ct_count++; ?>
            <?php endforeach; endforeach; ?>
          </div>

        <?php } ?>

        <?php 
          if ( $future_event ) {
        ?>
          <a class="btn-mirren register-link bottom" 
             title="<?php print ($node->field_registration_link['und'][0]['title']); ?>"
             href="<?php print ($node->field_registration_link['und'][0]['url']); ?>"
             <?php if (isset($node->field_registration_link['und'][0]['attributes']['target']) == '_blank') { ?>target="_blank"<?php } ?>>
             <?php print ($node->field_registration_link['und'][0]['title']); ?> ›
          </a> <br/>
        <?php 
          }
        ?>

        <?php
           $block = block_load('views', 'events-block_1');
           $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
           print $output;
        ?>
        
        

      </div>

      <?php if ( isset($sidebar_right_3) ) { ?>
        <div class="col-xs-3 sidebar-right-3 sidebar">
          <?php print render($sidebar_right_3); ?>
          <?php
          //this is for the flaggin block. This is part of the node content
          if ( ($contentAccess) && isset($content['flag_team_library']) ) {  
            print '<div class="block block-block" id="library-flag-block">';
            print "<h2>" . t('Library') . "</h2>";
            print "<div class='content'><ul>";
            print "<li>" . render($content['flag_team_member_library']) . "</li>";
            print "<li>" . l(t('View My Library ›'), 'my-library') . "</li>";
            print "<li>" . render($content['flag_team_library']) . "</li>";
            //print "<li>" . l(t('View Team Library'), 'team-library') . "</li>";
            print "</ul></div>";
            print "</div>";
          }
          ?>
          
        </div>
      <?php } 
        elseif ( isset($sidebar_left_3) ) { 
      ?>
        <div class="col-xs-3 col-xs-pull-9 sidebar-left-3 sidebar">
          <?php print render($sidebar_left_3); ?>
        </div>
      <?php } 
        elseif ( isset($sidebar_right_4) ) { 
      ?>
        <div class="col-xs-4 sidebar-right-4 sidebar">
          <?php print render($sidebar_right_4); ?>
        </div>
      <?php }
        else { ?>
        <!-- 1-col -->
      <?php 
        }
      ?>
        
    </div>
    
  </article>

<?php } ?>