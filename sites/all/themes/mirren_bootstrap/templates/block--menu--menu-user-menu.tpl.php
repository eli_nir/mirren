<?php
//custom
?>
<div id="user-logged-in-menu">
  <?php
  global $user;
  //drupal_set_message("<pre>" . print_r($user, TRUE) . "</pre>");
  //load user info
  //https://api.drupal.org/api/drupal/modules!user!user.module/function/user_load/7
  $user_profile = user_load($user->uid);
  //get user pic and apply styles to it
  //make sure there is an image
  if(isset($user_profile->picture->uri)){
    //get user pic uri 
    $user_picture_uri = $user_profile->picture->uri;
    //theme the user pic with an image style
    $user_picture = theme(
      'image_style',
      array(
      'path' => $user_picture_uri,
      'style_name' => 'image_22_22'
      )
    );
    //print user pic in link
    print "<div id='user-picture'>";
    print l($user_picture, 'user/' . $user->uid, array('html' => TRUE));
    print "</div>";
  }else{
    //choose an random image
    print "<div id='user-picture'>";
    print l("<img src='/sites/all/themes/mirren_bootstrap/assets/images/avatar_selection/member_default_0.png' width='22' height='22' />", 'user/' . $user->uid, array('html' => TRUE));
    print "</div>";
  }
  //-----------------------------------
  ?>
  <div id="upper-logged-in">
   
   
    <?php
    //if there is names
    if(isset($user_profile->field_first_name['und'][0]['safe_value'])){
      $user_name = $user_profile->field_first_name['und'][0]['safe_value'];
      //if there is a last name filled out
      if(isset($user_profile->field_last_name['und'][0]['safe_value'])){
        $user_name .= " " . $user_profile->field_last_name['und'][0]['safe_value'];
      }
    }else{
      //no names load user name
      $user_name = $user->name;
    }
    //check number of characters in user name of full name truncate after 11 characters
    if(strlen($user_name) > 10){
      $user_name = substr($user_name , 0, 10);
      $user_name .= "...";
    }?>
    
    <span class="collapsed" data-toggle="collapse" data-parent="#user-logged-in-menu" href="#lower-logged-in">
    <div class="name-container">
    <?php print $user_name; ?>
    <!-- <img src="/sites/all/themes/mirren_bootstrap/assets/images/user-menu-up.gif"> -->
    </div>
    </span>
    
    
   
    
  </div>
  
  
  <div class="collapse" id="lower-logged-in">
	  <ul class="menu"><li class="first leaf"><a href="/user" title="">My Profile</a></li>
	  	<li class="leaf"><a href="/my-agency-hub" title="">My Agency Hub</a></li>
	  	<li class="leaf"><a href="/my-library" title="">My Library</a></li>
	  	<li class="last leaf"><a href="/user/logout" title="">Logout</a></li>
	  </ul>
    
 </div>
</div>
    