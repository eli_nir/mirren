<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 * http://mirren.boriszaydlin.com/admin/structure/views/nojs/config-item/breaking_leads/block/field/field_title_non_member
 */
    global $user;

    if(!empty($row->_field_data['nid']['entity']->field_title_non_member['und'][0]['value']) && ($user->uid == 0)){
        $title_anonim = $row->_field_data['nid']['entity']->field_title_non_member['und'][0]['value'];
        $output = l($title_anonim, drupal_lookup_path('alias',"node/".$row->nid));
    }
?>
<?php print $output; ?>