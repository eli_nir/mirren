<?php

/**
 * Override or insert variables into the html templates.
 */

function mirren_bootstrap_preprocess_html(&$variables, $hook) {

  // Add paths
  $variables['base_path'] = base_path();
  $variables['path_to_theme'] = drupal_get_path('theme', 'mirren_bootstrap');

  // node access -> Training Item Content Type -> category/type field value
  $node = menu_get_object();
  if ($node && isset($node->nid)) {
    $node = node_load($node->nid);
    node_build_content($node);
    // Define paths to the custom fields in content
    $variables['training_type'] = strtolower( render($node->field_type_training_category['und'][0]['value']) );
  }

  drupal_add_library('system', 'jquery.cookie');

  /* Roles Based Restricted Content - var used in body class  */
  global $user;
  $check = array_intersect(array('member', 'trial', 'master', 'mirren administrator', 'store administrator', 'administrator', 'converted trial', 'special access', 'page administrator'), array_values($user->roles));
  if (empty($check) ? FALSE : TRUE) {
    $variables['roleClearance'] = 'content-access';
  } else {
    $variables['roleClearance'] = 'content-blocked';
  }

}

/**
 * Implements hook_process_html().
 */

function mirren_bootstrap_process_html(&$variables) {

  // Attributes for html element.
  $variables['html_attributes_array'] = array(
    'lang' => $variables['language']->language,
    'dir' => $variables['language']->dir,
  );

  $variables['body_attributes_array'] = array();

  // Flatten out html_attributes.
  $variables['html_attributes'] = drupal_attributes($variables['html_attributes_array']);

}

/**
 * Implement hook_html_head_alter().
 */

function mirren_bootstrap_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  if (isset($head['system_meta_content_type']['#attributes']['content'])) {
    $head['system_meta_content_type']['#attributes'] = array('charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']));
  }
}

/**
  * Implements hook_process_html_tag
  * - From http://sonspring.com/journal/html5-in-drupal-7#_pruning
  */
function mirren_bootstrap_process_html_tag(&$variables) {

    //Since we're HTML5...remove needless XHTML
    $tags = &$variables['element'];

    // Remove type="..." and CDATA prefix/suffix.
    unset($tags['#attributes']['type'], $tags['#value_prefix'], $tags['#value_suffix']);

    // Remove media="all" but leave others unaffected.
    if (isset($tags['#attributes']['media']) && $tags['#attributes']['media'] === 'all') {
      unset($tags['#attributes']['media']);
    }

}

/**
 * Override or insert variables into the page template.
 */

function mirren_bootstrap_preprocess_page(&$variables, $hook) {

  // Add paths
  $variables['base_path'] = base_path();
  $variables['path_to_theme'] = drupal_get_path('theme', 'mirren_bootstrap');

  //Auto replace Copyright Year
  $variables['page']['footer']['block_1']['#markup'] = str_replace('*', date('Y'), $variables['page']['footer']['block_1']['#markup']);
  $variables['page']['footer']['block_1']['#markup'] = str_replace('%privacy_policy_link%', l('Privacy Policy', 'node/14006'), $variables['page']['footer']['block_1']['#markup']);

  // node access
  $node = menu_get_object();
  if ($node && isset($node->nid)) {
    $node = node_load($node->nid);
    node_build_content($node);
    // look for Header Image or Header Intro Text
    if ( isset($node->field_feature_intro['und'][0]['value']) || isset($node->field_banner_image['und'][0]['uri']) ) {
      $variables['header_status'] = 'header-true';
    }
    elseif ( $node->type == 'event' ) {
     $variables['header_status'] = 'header-true';
    }
    else {
     $variables['header_status'] = 'default';
    }

  }


if (isset($variables['node']->type)) { $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type; }


}

/**
 * Override or insert variables into the node template.
 */
function mirren_bootstrap_preprocess_node(&$variables) {
  // This enables message-printing in node.tpl files
  //$variables['messages'] = theme('status_messages');
  // Custom region variables for use in node.tpl files
  if ($block_region_name = block_get_blocks_by_region('sidebar_right_3')) {
    $variables['sidebar_right_3'] = $block_region_name;
  }
  if ($block_region_name = block_get_blocks_by_region('sidebar_left_3')) {
    $variables['sidebar_left_3'] = $block_region_name;
  }
  if ($block_region_name = block_get_blocks_by_region('sidebar_right_4')) {
    $variables['sidebar_right_4'] = $block_region_name;
  }
  if ($block_region_name = block_get_blocks_by_region('full_width_banner')) {
    $variables['full_width_banner'] = $block_region_name;
  }
  if ($block_region_name = block_get_blocks_by_region('header_submenu')) {
    $variables['header_submenu'] = $block_region_name;
  }
  if ($block_region_name = block_get_blocks_by_region('node_body_content')) {
    $variables['node_body_content'] = $block_region_name;
  }
  if ($block_region_name = block_get_blocks_by_region('forum_header_block')) {
    $variables['forum_header_block'] = $block_region_name;
  }
  //--------------------------------------------------
  // NODE TEMPLATE CONTENT ACCESS BY ROLE - set $contentAccess variable
  global $user;
  $check = array_intersect(array('member', 'trial', 'master', 'mirren administrator', 'store administrator', 'administrator', 'converted trial', 'special access', 'page administrator'), array_values($user->roles));
  if (empty($check) ? FALSE : TRUE) {
    $variables['contentAccess'] = TRUE;
  } else {
    $variables['contentAccess'] = FALSE;
  }
}


/**
 * Pager Theming
 *
 */
function mirren_bootstrap_preprocess_pager(&$variables, $hook) {  
  if ($variables['quantity'] > 5) $variables['quantity'] = 5;
  $variables['tags'][0] = '<<';
  $variables['tags'][1] = '‹';
  $variables['tags'][3] = '›';
  $variables['tags'][4] = '>>';
}

/**
 * Block Theming
 *
 */
function mirren_bootstrap_preprocess_block(&$variables) {
  $block_id = $variables['block']->module . '-' . $variables['block']->delta;
  if ( $block_id == 'search-form') {
    $variables['classes_array'][] = drupal_html_class('collapse');
  }
}

/* Bootstrap Style Breadcrumb Trail */
function mirren_bootstrap_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if ( !empty ($breadcrumb) ) {
    $crumbs = '<ol class="breadcrumb">';
    foreach ( $breadcrumb as $value ) {
      $crumbs .= '<li>'.$value.'</li>';
    }
    $crumbs .= '</ol>';
    return $crumbs;
  }
}



/* Main Menu */
function mirren_bootstrap_menu_tree__main_menu($variables) {
  return '<ul class="inline-text">' . $variables['tree'] . '</ul>';
}

function mirren_bootstrap_menu_link__main_menu(array $variables) {
  //dpm($variables);

  $element = $variables['element'];
  $sub_menu = '';

  // Add mlid as class
  $element['#attributes']['class'][] = 'menu-' . $element['#original_link']['mlid'];

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  if (($element['#title'] === 'Search')) {
    $element['#localized_options']['html'] = TRUE;
    $linktext = '<span class="glyphicon glyphicon-search"></span>';
    $element['#localized_options']['attributes']['data-toggle'][] = 'collapse';
    $element['#localized_options']['attributes']['data-target'][] = '#block-search-form';
  }
  else {
    $linktext = $element['#title'];
  }

  $output = l($linktext, $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/* Secondary Menu */
function mirren_bootstrap_menu_tree__menu_secondary_menu($variables) {

  global $user;
  if ($user->uid) {
    $user_link = l("Log Out ›","user/logout");
  }
  else {
    $user_link = l("Log In","user", array('query' => drupal_get_destination()));
  }

  return '<ul class="inline-text">' . $variables['tree'] . '<li class="user-link leaf">' . $user_link . '</li>' . '</ul>';
}

/*
 * https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_form_alter/7
 * https://api.drupal.org/comment/25228#comment-25228
 * Altering the Forms from the Template
*/
function mirren_bootstrap_form_alter(&$form, &$form_state, $form_id){
/* Descriptive Text Under Password Recovery Form */
 if($form_id == 'user_pass') {
   $form['actions']['submit']['#value'] = t('reset password');
   $form['name']['#description'] = t('Enter your membership email and you will receive instructions to reset your password. <br> If necessary, check your spam folder.');
 }

  if($form_id == 'user_login') {
    //enable description text
    $form['name']['#description'] = t('');
    $form['pass']['#description'] = t('');
  }

  //drupal_set_message('<pre>' . print_r($form_id, TRUE) . "</pre>");
  switch($form_id) {
    case 'user_login': // the value we stole from the rendered form
    case 'user_login_block':
      $form['customtext'] = array(
        '#type' => 'item',
        '#markup' => "<p>" . l(t('Forget Your Password ›'), 'user/password') . t(' (Takes just 30 Secs to reset)') . "</p>",
        '#weight' => 10, // Adjust so that you can place it whereever
      );

      if (isset($form['remember_me'])) {
        $form['remember_me']['#title'] = 'Remember My Email';
      }

        // Modal Login
        if ($form_id == 'user_login_block') {

          // Input Description Removed. To add, just fill in the text
          $form['name']['#description'] = t('Enter your e-mail address.');
          $form['pass']['#description'] = t('Enter the password that accompanies your e-mail.');

          // hide 'create new' & 'forgot password' links
          unset($form['links']);

          // style submit button
          $form['actions']['submit']['#attributes']['class'][] = 'btn-mirren';
        }
      break;

      // My Library
      case 'views_form_my_library_logged_in_user__block_1':
        // No resutls
        if (empty($form['draggableviews'])) {
          unset ($form['actions']['submit']);
        }
        else {
          $form['actions']['submit']['#value'] = t('Save');
          $form['actions']['cancel'] = array(
              '#type'  => 'submit',
              '#value' => t('Cancel'),
              '#weight' => -10,
          );

        }

        break;

      //if the contact webform - we need to add an email (if posted) from the footer form
      case 'webform_client_form_22273':
        //webform form_alter
        //http://stackoverflow.com/questions/5105608/drupal-form-alter-in-webform-forms
        //if there has been a post from the footer email form
        if(isset($_POST['footer-email'])){
          //form alter and add this to the value of the Webform email field.
          $form['submitted']['e_mail_address']['newsletter_email_address']['#default_value'] = $_POST['footer-email'];
        }
      break;
    }
  }


/**
 * Modify Forum Comment form
 */
function mirren_bootstrap_form_comment_node_forum_form_alter(&$form, &$form_state) {
  $form['comment_body']['#weight'] = -10;
  $form['comment_body']['und'][0]['value']['#attributes']['data-default'] = $form['comment_body']['und'][0]['value']['#default_value'];
  $form['actions']['submit']['#value'] = t('Post ›');

  $form['#validate'][] = 'mirren_bootstrap_form_comment_node_forum_form_validate';
  $form['#submit'][] = 'mirren_bootstrap_form_comment_node_forum_form_submit';

  if (user_is_logged_in()) {
    // Hide fields
    $form['author']['_author'] =
    $form['field_your_company']['#access'] =
    $form['field_your_location']['#access'] = FALSE;
  }
  // Anon user
  else {
    $form['membertoggle'] = array(
      '#type' => 'radios',
      '#options' => array('user' => 'Member', 'anon' => 'Non-Member'),
      '#weight' => -9,
      '#suffix' => '<div class="anon-comment-fields">',
    );
    $form['actions']['#prefix'] = '</div> <!-- /comment-post-fields-->';
  }
}

/**
 * Validate handler for forum comments
 */
function mirren_bootstrap_form_comment_node_forum_form_validate($form, &$form_state) {
  if ($form['comment_body']['und'][0]['value']['#default_value'] == $form_state['values']['comment_body']['und'][0]['value']) {
    form_set_error('comment_body', 'Comments are required.');
  }
}

/**
 * Submit handler for forum comments
 */
function mirren_bootstrap_form_comment_node_forum_form_submit($form, &$form_state) {
  global $user;

  // Remove saved comment (handled by JS)
  setrawcookie('deleteTmpComment', 1, 0, '/');

  if (!$user->uid) {
    $form_state['redirect'][1]['query']['showAnonThankYou'] = 1;
  }
}


function mirren_bootstrap_mail_alter(&$message) {
  if ($message['id'] == 'user_register_no_approval_required') {
    $message['send'] = FALSE;
  }
}

function mirren_bootstrap_preprocess_field(&$variables, $hook) {
  if(($variables['element']['#field_type'] == 'text_long' || $variables['element']['#field_type'] == 'text_with_summary') && $variables['element']['#items'][0]['format'] == null) {
    $variables['items'][0]['#markup'] = nl2br($variables['items'][0]['#markup']);
  }
}

/*
 * Implements template_preprocess_comment().
 */
function mirren_bootstrap_preprocess_comment(&$vars) {
  $comment = $vars['comment'];
  if ($comment->uid != 0) {
    $comment_user = user_load($comment->uid);
    $user_full_name = '';
    if (!empty($comment_user->field_first_name[LANGUAGE_NONE])) {
      $user_full_name .= $comment_user->field_first_name[LANGUAGE_NONE][0]['value'];
    }
    if (!empty($comment_user->field_last_name[LANGUAGE_NONE])) {
      $user_full_name .= ' ' . $comment_user->field_last_name[LANGUAGE_NONE][0]['value'];
    }
    if (!empty($user_full_name)) {
      $vars['author'] = l($user_full_name, 'user/' . $comment_user->uid);
    }
  }
}

